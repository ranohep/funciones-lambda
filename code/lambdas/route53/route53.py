import boto3
import sys
import time


route53_client = boto3.client('route53')

sts_cient = boto3.client('sts')

cwl_client = boto3.client('logs')
route53_paginator = route53_client.get_paginator('list_hosted_zones')

response_r53_paginator = route53_paginator.paginate(PaginationConfig={'StartingToken': None})



def lambda_handler(event, context):

    get_acc_no = sts_cient.get_caller_identity()['Account']
    get_acc_role_arn = sts_cient.get_caller_identity()['Arn']

    find_str = "-SERVICE"
    get_lifecycle = get_acc_role_arn[get_acc_role_arn.find('/')+1:]
    find_lifecycle = get_lifecycle.find(find_str)
    lifecycle = get_lifecycle[:find_lifecycle]
    # print(lifecycle) # DEVL

    caller_refernce = ''.join([
            lifecycle.lower(),
            '-',
            get_acc_no,
            '-',
            '001'

        ])
    if lifecycle == 'TEST-CMBU' or lifecycle == 'TEST-CU' or lifecycle == 'SDBX-SHRD' or lifecycle == 'SDBX-CENG' or lifecycle == 'SDBX-SENG' or lifecycle == 'DEVL-CENG' or lifecycle == 'TEST-CSBU' or lifecycle == 'DEVL-CSBU' or  lifecycle == 'DEVL-CU' or lifecycle == 'TEST-MFBU' or lifecycle == 'DEVL-CMBU' or lifecycle == 'DEVL-MFBU' or lifecycle == 'TEST-SFBU' or lifecycle == 'DEVL-DU' or lifecycle == 'TEST-DU' or lifecycle == 'ACPT-ESS' or lifecycle == 'ACPT-SFBU' or lifecycle == 'ACPT-MFBU' or lifecycle == 'ACPT-CU' or lifecycle == 'ACPT-DU' or lifecycle == 'ACPT-CMBU' or  lifecycle == 'ACPT-CSBU' or lifecycle == 'TEST-ESS' or lifecycle == 'DEVL-SFBU' or lifecycle == 'DEVL-SENG' or lifecycle == 'ACPT-LOG': 

        hostedzone_domain_name = ''.join([
            lifecycle.lower(),
            '.intgfanniemae.com.'
        ])

        list_hosteszones = list_hostedzones_on_r53(hostedzone_domain_name)
        # print('Hostedzone domain list - {}'.format(list_hosteszones))

        if hostedzone_domain_name in  list_hosteszones:
            print('Domain exist -{}'.format(hostedzone_domain_name))

        else:
            # print('Domain doesnt exists -{}'.format(hostedzone_domain_name))
            print('Domain creation is in progress')
            create_r53_hostedzone = route53_client.create_hosted_zone(
                Name=hostedzone_domain_name,
                CallerReference=caller_refernce,
                HostedZoneConfig={
                    'Comment': 'External DNS',
                    'PrivateZone': False
                }
            )
            # print(create_r53_hostedzone['HostedZone'])
            print(create_r53_hostedzone['DelegationSet']['NameServers'])

            Hosted_zone_id = create_r53_hostedzone['HostedZone']['Id']

            time.sleep(5)

            cwl_log_group_for_query_logging = ''.join([
            '/aws/route53/',
            lifecycle.lower(),
            '.intgfanniemae.com'
            ])

            cloudwatchlogsloggrouparn = ''.join([
                'arn:aws:logs:us-east-1:',
                get_acc_no,
                ':',
                'log-group'
                ':',
                cwl_log_group_for_query_logging

            ])

            # print(cloudwatchlogsloggrouparn)

            # creation of log group

            cwl_response = cwl_client.create_log_group(
                logGroupName=cwl_log_group_for_query_logging
            )

            if cwl_response['ResponseMetadata']['HTTPStatusCode'] == 200:
                print('cloudwatch loggroup created for  r53 query logging- {}'.format(cwl_log_group_for_query_logging))

            time.sleep(5)

            create_r53_query_loggging_response = route53_client.create_query_logging_config(
                HostedZoneId=Hosted_zone_id,
                CloudWatchLogsLogGroupArn=cloudwatchlogsloggrouparn
            )

            if create_r53_query_loggging_response['ResponseMetadata']['HTTPStatusCode'] == 200:
                print('R53 Query logging enabled -{}'.format(hostedzone_domain_name))

    elif lifecycle == 'PROD-LOG' or lifecycle == 'PROD-CU' or lifecycle == 'PROD-MFBU' or lifecycle == 'PROD-ESSX' or lifecycle == 'PROD-ESSP': 


        hostedzone_domain_name = ''.join([
            lifecycle.lower(),
            '.fanniemae.com.'
        ])

        list_hosteszones = list_hostedzones_on_r53(hostedzone_domain_name)
        # print('Hostedzone domain list - {}'.format(list_hosteszones))

        if hostedzone_domain_name in  list_hosteszones:
            print('Domain exist -{}'.format(hostedzone_domain_name))

        else:
            # print('Domain doesnt exists -{}'.format(hostedzone_domain_name))
            print('Domain creation is in progress')
            create_r53_hostedzone = route53_client.create_hosted_zone(
                Name=hostedzone_domain_name,
                CallerReference=caller_refernce,
                HostedZoneConfig={
                    'Comment': 'External DNS',
                    'PrivateZone': False
                }
            )
            # print(create_r53_hostedzone['HostedZone'])
            print(create_r53_hostedzone['DelegationSet']['NameServers'])

            Hosted_zone_id = create_r53_hostedzone['HostedZone']['Id']

            time.sleep(5)

            cwl_log_group_for_query_logging = ''.join([
                '/aws/route53/',
                lifecycle.lower(),
                '.fanniemae.com'
            ])

            cloudwatchlogsloggrouparn = ''.join([
                'arn:aws:logs:us-east-1:',
                get_acc_no,
                ':',
                'log-group'
                ':',
                cwl_log_group_for_query_logging

            ])

            print(cloudwatchlogsloggrouparn)

            # creation of log group

            cwl_response = cwl_client.create_log_group(
                logGroupName=cwl_log_group_for_query_logging
            )

            if cwl_response['ResponseMetadata']['HTTPStatusCode'] == 200:
                print('cloudwatch loggroup created for  r53 query logging- {}'.format(cwl_log_group_for_query_logging))

            time.sleep(5)

            create_r53_query_loggging_response = route53_client.create_query_logging_config(
                HostedZoneId=Hosted_zone_id,
                CloudWatchLogsLogGroupArn=cloudwatchlogsloggrouparn
            )

            if create_r53_query_loggging_response['ResponseMetadata']['HTTPStatusCode'] == 200:
                print('R53 Query logging enabled -{}'.format(hostedzone_domain_name))



def list_hostedzones_on_r53(hostedzone_domain_name):

    hostedzone_domain_names_list = []

    try:

        print(hostedzone_domain_name)

        for r53hosted_zone_info in response_r53_paginator:
            # print('======')
            # print('R53 Hosted zone info - {}'.format(r53hosted_zone_info))
            for get_hostedzone_info in r53hosted_zone_info['HostedZones']:
                # print('HostedZone Name - {}'.format(get_hostedzone_info['Name']))
                # if get_hostedzone_info['Name'] == 'devl-ess.intgfanniemae.com.':
                hostedzone_domain_names = get_hostedzone_info['Name']
                hostedzone_domain_names_list.append(hostedzone_domain_names)
            return hostedzone_domain_names_list

    except:
        print('Hosted Zone name is not found - {}'.format(get_hostedzone_info['Name']))

# print()

if __name__ == '__main__':
    main()
