import os
import glob
import platform
import shutil
import subprocess
import zipfile
from doit.tools import create_folder
from contextlib import contextmanager
DOIT_CONFIG = {
    'default_tasks': [ 'zip' ]
}

REQ_FILE = 'dist/requirements.txt'
DIST_DIR = 'dist'

SOURCES = tuple([
    'handler.py',
])

TESTS   = tuple(glob.glob(
    os.path.join('tests', '**', 'test_*.py'), recursive=True)
)

def copy(src, dst):
    dst_dir = os.path.split(dst)[0]
    os.makedirs(dst_dir, exist_ok=True)
    shutil.copy(src, dst)


def task_setup_pipenv():
    """
    Setup the pipenv venv with all of the development tools
    """
    return {
        'actions': ['pipenv install -d'],
        'file_dep': [ 'Pipfile', 'Pipfile.lock'],
    }

def task_setup_pip_user():
    """
    Setup the user environment - for windows until able to setup pipenv
    """
    return {
        'actions': ['pip install -r requirements-devl.txt --user'],
        'file_dep': [ 'requirements.txt', 'requirements-devl.txt']
    }

def task_test():
    """Test Application"""
    return {
        'actions': [
            'python -m pytest' if platform.system() == 'Windows' else 'pipenv run pytest',
        ],
        'task_dep': [
            'setup_pip_user' if platform.system() == 'Windows' else 'setup_pipenv'
        ],
        'file_dep': SOURCES + TESTS,
        'verbosity': 2,
    }

def task_mypy():
    """MyPy on Source"""
    #cmd = "mypy --strict " + " ".join(SOURCES)
    cmd = "mypy " + " ".join(SOURCES)
    return {
        'actions': (cmd, ),
        'file_dep': SOURCES,
    }

def task_lint():
    """Run linter on source"""
    cmd = "pylint " + " ".join(SOURCES)
    return {
        'actions': (cmd, ),
        'file_dep': SOURCES,
    }

def task_requirements():
    """Build requirements.txt file from pipenv's Pipfile"""

    def clean_dist(dir):
        if os.path.isdir(dir):
            shutil.rmtree(dir)

    @contextmanager
    def read_requirements():
        if os.name == 'nt':
            f = open('requirements.txt', 'rb')
            yield f
            f.close()
        else:
            proc = subprocess.Popen(
                ['pipenv', 'lock', '-r'],
                stdout=subprocess.PIPE
            )
            yield proc.stdout

    def install_requirements(req_file):
        create_folder(os.path.split(req_file)[0])
        with open(req_file, 'wb') as req, read_requirements() as f:
            for line in f:
                if line.startswith(b'-e '):
                    req.write(line[3:])
                else:
                    req.write(line)
        subprocess.run(['pip', 'install', '-r', req_file, '-t', DIST_DIR])

    return {
        'actions': [
            (clean_dist,           [DIST_DIR]),
            (install_requirements, [REQ_FILE]),
        ],
        'file_dep': ['Pipfile', 'Pipfile.lock'],
        'targets': [REQ_FILE]
    }


def task_copy_source():
    """Copy Python Source to DIST_DIR for packaging"""

    for file in SOURCES:
        dst = os.path.join(DIST_DIR, file)
        yield {
            'name': file,
            'actions': [
                (copy, [file, dst])
            ],
            'task_dep': ['requirements'],
            'file_dep': [file],
            'targets': [dst]
        }

def task_zip():
    """
    Package into a AWS lambda zip file in DIST_DIR
    """
    ZIP_FILE = os.path.join(
        DIST_DIR,
        os.path.basename(os.path.dirname(__file__)) + '.zip'
    )

    def zip_dist(file):
        with zipfile.ZipFile(file, 'w') as zf:
            for dfile in glob.glob(os.path.join('dist', '**'), recursive=True):
                if dfile == file or dfile.endswith('.zip'):
                    continue
                zf.write(dfile, dfile[len('dist' + os.pathsep):])

    return {
        'actions':  [
            (zip_dist, [ZIP_FILE]),
        ],
        'task_dep': [],
        'targets':  [ZIP_FILE, 'samTemplate.yaml'],
        'file_dep': [os.path.join(DIST_DIR, file) for file in SOURCES] +
                    [REQ_FILE]
    }
