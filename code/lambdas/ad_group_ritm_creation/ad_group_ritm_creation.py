#!/usr/bin/env python

import copy
import boto3
import logging
import json
import re
import os
import base64
import requests
#from .message import Message, RequestType
from requests.auth import HTTPBasicAuth
from botocore.exceptions import ClientError
from urllib.parse import urlencode
from urllib.request import Request, urlopen
from ldap3 import Server, Connection, SUBTREE

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


logger = logging.getLogger('ADGroupCreator')
primary_region='us-east-1'
post_headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': None}
token_url =  "https://fanniemaetest.service-now.com/oauth_token.do"
ritm_url = "https://fanniemaetest.service-now.com/api/now/import/u_create_ritm_import"
ad_header = 'OnwqazIhOVhsSFRNCg=='
client_id = ''
client_secret = ''
owner_id = ''
owner_secret = ''
ad_secret = ''
api_proxy_url = {'https': 'http://zsproxy.fanniemae.com:9480',
    'http': 'http://zsproxy.fanniemae.com:9480'
}
lookup_values={}

def handler(event, context):
    """ Lambda entry point """

    account_id = event['Account']
    region = event['Region']
    account_name = event['account_name']
    # ssm_client = boto3.client('ssm')
    # lifecycle_ssm_param = '/fnma/account/lifecycle'
    # response = ssm_client.get_parameter(Name=lifecycle_ssm_param)
    lifecycle = event['lifecycle']
    print(lifecycle)
    environment = lifecycle.split('-')[0] 

    submit_ad_groups_create_ritm(account_id, account_name, region, lifecycle,environment)

def get_auth_token(token_url, client_id, client_secret, owner_id, owner_pwd):
    logger.info('[-] Requesting token ...')

    # Grant type is 'password' i.e ROPC
    data = {'grant_type': 'password', 'username': owner_id, 'password': owner_pwd}
    response = requests.post(token_url, proxies=api_proxy_url, data=data, verify=False, allow_redirects=False, auth=(client_id, client_secret))
    if response.status_code == 200:
        logger.info(f'\tMyServices Authentication Response [HTTP {0}]'.format(response.status_code))
        return json.loads(response.text)
    elif response.status_code >= 500:
        logger.error(f'\t[{response.status_code}] Server Error: {response.content}')
    elif response.status_code == 404:
        logger.error(f'\t[{response.status_code}] URL not found: [{token_url}]')
    elif response.status_code == 401:
        logger.error(f'\t[{response.status_code}] Authentication Failed. Response: {response.content}')
    elif response.status_code >= 400:
        logger.error(f'\t[{response.status_code}] Bad Request. Content: {response.content}')
    elif response.status_code >= 300:
        logger.error(f'\t[{response.status_code}] Unexpected redirect.')
    else:
        logger.error(f'\tUnexpected Error: [HTTP response.status_code]: Content: {response.content}')
    return None

def create_ad_group(sc_request):
    logger.info(f"[-] create_ad_group. sc_request:\n{json.dumps(sc_request, indent=2, default=str)}")
    sc_variables = sc_request['u_service_catalog_variables']
    sc_variables['data_app_server'] = sc_variables['data_app_server'].replace('-', ' ').replace('_', ' ').replace('(', ' ').replace(')', ' ').replace('[', ' ').replace(']', ' ').replace('/', ' ')
    sc_request_key_value = {
        'u_service_catalog_variables': json.dumps(sc_variables),
        'u_service_catalog_id': sc_request['u_service_catalog_id']
    }
    logger.info(f"\tsc_request_key_val:\n{json.dumps(sc_request_key_value, indent=2, default=str)}")
    sc_request_str = json.dumps(sc_request_key_value)
    logger.info(f"\tsc_request_str:\n{sc_request_str}")
    # api_proxy_url = os.environ['API_PROXY_URL']
    # logger.info(f"\api_proxy_url:\n{api_proxy_url}")
    #token_url = os.environ['TOKEN_URL']
    token = get_auth_token(token_url, client_id, client_secret, owner_id, owner_secret)
    if token is None:
        raise "Failed to get Token"
    #create_ritm_url = os.environ['CREATE_RITM_URL']   
    create_ritm_url=ritm_url
    logger.info(f"\tcreate_ritm_url:\n{create_ritm_url}")
    requests_session = requests.session()
    requests_session.verify = False
    post_headers.update({'Authorization': f"Bearer {token['access_token']}"})
    logger.info(f"\tHEADER:\n{json.dumps(post_headers, indent=2, default=str)}")
    response = requests.post(url=create_ritm_url, proxies=api_proxy_url, headers=post_headers, data=sc_request_str, verify=False)
    logger.info(response)
    response_content = json.loads(response.content.decode('utf-8'))
    logger.info('[-] [HTTP {0}]: Content: {1}'.format(response.status_code, response_content))

    if response.status_code == 200:
        logger.info(json.dumps(response_content, indent=2, default=str))
        return response.status_code, response_content
    elif response.status_code == 201:
        logger.debug(response_content)
        if 'result' in response_content:
            status = response_content['result'][0]['status']
            if status == 'inserted':
                ritm_details = json.loads(response_content['result'][0]['ritm_details'])
                logger.info(json.dumps(ritm_details, indent=2, default=str))
                return status, ritm_details
            elif status == 'error':
                logger.error(json.dumps(response_content, indent=2, default=str))
                return status, response_content

    response.status_code == 400
    {'error': {'message': 'Exception while reading request', 'detail': 'Cannot decode: java.io.StringReader@33a4a4'}, 'status': 'failure'}
    logger.error(json.dumps(response_content, indent=2, default=str))
    return response.status_code, response_content


def does_ad_group_exist(group_name):
    """ Check AD Group using LDAP connection """
    logger.info(f"\Checking AD Group Existence for :\n{group_name}")
    # Connect to server
    server = Server(host='fanniemae.com', port=636, use_ssl=True, get_info='NO_INFO')
    base = 'CN=AWS AD_FAST_USR,OU=Applications,OU=Accounts,OU=DC,DC=fanniemae,DC=com'
    connection = Connection(server, base, ad_secret, auto_bind=True)

    search_base = 'OU=Groups,DC=fanniemae,DC=com'
    found = False
    search_filter = '(cn=' + group_name + ')'
    LOGGER.info('Starting search for %s', search_filter)
    connection.search(search_base, search_filter, attributes=['CN'])
    for entry in connection.entries:
        response = json.loads(entry.entry_to_json())
        ad_group = response['attributes']['cn'][0]
        if ad_group == group_name:
            found = True
            break
    # Close and unbind connection with server
    connection.unbind()

    # Return list of AD groups
    return found

def get_secret():
    global client_id,client_secret,owner_id,owner_secret,ad_secret
    owner_header = os.environ['OWNER_HEADER']
    logger.info(f"\owner_header:\n{owner_header}")
    client_header = os.environ['CLIENT_HEADER'] 
    logger.info(f"\client_header:\n{client_header}")
    ad_header = os.environ['AD_HEADER']
    logger.info(f"\tad_header:\n{ad_header}")
    client_header_decoded = str(base64.b64decode(client_header)).split('b\'')[1]
    client_id = client_header_decoded.split(":")[0]
    client_secret = (client_header_decoded.split(":")[1]).split('\\n')[0]
    owner_header_decoded = base64.b64decode(owner_header).decode()
    owner_id = owner_header_decoded.split(":")[0]
    owner_secret = (owner_header_decoded.split(":")[1])
    ad_header_decoded = str(base64.b64decode(ad_header)).split('b\'')[1]
    ad_secret = ad_header_decoded.split("\\n")[0]    

def get_user_id(user_email):
    """ Find User Name using LDAP connection """
    # Connect to server
    server = Server(host='fanniemae.com', port=636, use_ssl=True, get_info='NO_INFO')
    base = 'CN=AWS AD_FAST_USR,OU=Applications,OU=Accounts,OU=DC,DC=fanniemae,DC=com'
    connection = Connection(server, base, ad_secret, auto_bind=True)

    search_base = 'DC=fanniemae,DC=com'
    found = False
    search_filter = '(&(objectClass=user)(mail=' + user_email + '))'
    attrs = ["*"]
    LOGGER.info('Starting search for %s', search_filter)
    connection.search(search_base,search_filter, search_scope=SUBTREE, attributes=['sAMAccountName'])
    user_data = ''
    for entry in connection.entries:
        response = json.loads(entry.entry_to_json())
        print(response)
        user_data = response['attributes']['sAMAccountName'][0]
        if not user_data:
            break
    # Close and unbind connection with server
    connection.unbind()

    # Return User ID
    return user_data

def submit_ad_groups_create_ritm(aws_account, account_name, aws_region, lifecycle, env_name):
    ad_group_file = 'ad_group_data/ad-grp-ritm-spec.json'
    get_secret()
    ritm_specs = {}
    with open(ad_group_file, 'r') as fp:
        ritm_specs = json.load(fp)    

    lifecycleupper = lifecycle.upper()
    environment = lifecycle.split('-')[0]
    print("Lifecycle",lifecycle)
    env_name = translate_environment(env_name)
    #lookup_values={'lifecycle_type':env_name}
    for aws_role, role_ritm_spec in ritm_specs.items():
        logger.info(f"\t[-] Processing {aws_role} for {lifecycle} of {aws_region}...")
        if primary_region != aws_region and role_ritm_spec['Scope'] == 'global':  # Secondary Region
            logger.info(f"\t\t{aws_role} is {role_ritm_spec['Scope']} for {llifecycle} of {aws_region}, No AD Group needed. Continuing.")
            continue
        if 'Condition' in role_ritm_spec:
            #print("Second Look Up value",lookup_values)
            if evaluate_condition(aws_role, role_ritm_spec['Condition'], environment) is False:
                logger.info(f"\tRole Persona {aws_role}; AD Request condition doesn't match with {role_ritm_spec['Condition']}. Ignoring.")
                continue

        ritm_spec_updated = copy.deepcopy(role_ritm_spec)
        role_sc_var = ritm_spec_updated['ServiceCatalogRequestSpec']['u_service_catalog_variables']
        if '[' in role_sc_var['contact_name']:
            role_sc_var['contact_name'] = role_sc_var['contact_name'][role_sc_var['contact_name'].index('[') + 1:role_sc_var['contact_name'].rindex(']')]
        if '[' in role_sc_var['requested_for']:
            role_sc_var['requested_for'] = role_sc_var['requested_for'][role_sc_var['requested_for'].index('[') + 1:role_sc_var['requested_for'].rindex(']')]
        if '[' in role_sc_var['ad_group_owner']:
            role_sc_var['ad_group_owner'] = role_sc_var['ad_group_owner'][role_sc_var['ad_group_owner'].index('[') + 1:role_sc_var['ad_group_owner'].rindex(']')]

        role_sc_var['ad_security_group_name_add'] = role_sc_var['ad_security_group_name_add'].format(lifecycle=lifecycleupper, aws_account=aws_account)
        role_sc_var['data_app_server'] = role_sc_var['data_app_server'].format(env_name=env_name, aws_account_name=account_name)
        role_sc_var['environment'] = role_sc_var['environment'].format(env_name=env_name)

        # Check if the Group exist before submitting the request
        # if does_ad_group_exist(role_sc_var['ad_security_group_name_add']):
        #     logger.info(f"Group {role_sc_var['ad_security_group_name_add']} already Exists. No action needed.")
        #     return None
        # else:
        sc_request = ritm_spec_updated['ServiceCatalogRequestSpec']
        logger.info(f"\t\tAD Group Service Catalog Requesting for\n{json.dumps(sc_request, indent=2, default=str)}")
            
            # status, result = create_ad_group(sc_request)
            # if status == 'inserted':
            #     logger.info(f"\t\tAD Group Service Catalog Request submitted for {aws_role}. RITM Details: {result}")
            # else:
            #     logger.info(f"FAILED - AD Group for {aws_role} RITM request failed. status: {status}, content: {result}")
                # raise "Error in AD Group creation"

def translate_environment(env):
    valid_vals = {
        "development": "Development",
        "acceptance": "Acceptance",
        "acptcont": "Acceptance",
        "test": "Test",
        "prod": "Production",
        "contingency": "Contingency",
        "clve": "Integration",
        "sdbx":"Sandbox"
    }
    return valid_vals[env]
    
def evaluate_condition(title, condition,environment):
    logger.info(f'Evaluating Role Provisioning Condition for {title} :: {condition}')
    for criteria_type, criteria in condition.items():
        logger.debug(f'{criteria_type}\t{criteria}')
        for attrib, campare_to in criteria.items():
            criteria_outcome = True
            logger.debug(f'{title}:\t{attrib}\t{campare_to}')
           
            # if attrib not in environment:
            #     logger.error(f"\t{title}:\tCondition variable {attrib} is not in lookup_values.")
            #     raise ('FAILED', f"\tCondition variable {attrib} is not in lookup_values.")
            if criteria_type == 'StringEquals':
                criteria_outcome = environment == campare_to
                logger.debug(f"\t{title}:\t{criteria_type} {attrib} :: {environment} :::: {campare_to}")
            elif criteria_type == 'StringNotEquals':
                criteria_outcome = environment != campare_to
                logger.debug(f"\t{title}:\t{criteria_type} {attrib} :: {environment} :::: {campare_to}")
            elif criteria_type == 'StringLike':
                criteria_outcome = environment in campare_to
                logger.debug(f"\t{title}:\t{criteria_type} {attrib} :: {environment} :::: {campare_to}")
            elif criteria_type == 'StringNotLike':
                criteria_outcome = environment not in campare_to
                logger.debug(f"\t{title}:\t{criteria_type} {attrib} :: {environment} :::: {campare_to}")
            if criteria_outcome is False:
                return False
    return True