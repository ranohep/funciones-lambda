#!/usr/bin/env python3

import sys
from pathlib import Path
import os
sys.path.append(
    str(Path(__file__).resolve().parent.parent)
)
print(sys.path)
from iam import assume_session
import boto3


def target_account_pipeline(id):
    sfn = boto3.client('stepfunctions')


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: <targetaccount #> <stackset>')
        raise SystemExit

    session = assume_session(
        'Debug',
        'asap-stack-sets',
        (sys.argv[1:2], 'asap-cicd')
    )

    ssm = session.client('ssm')
    print(ssm.get_parameter(Name='/global/Lifecycle'))
