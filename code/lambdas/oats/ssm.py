#!/usr/bin/env python3

import logging
import json
from collections import defaultdict

from iam import assume_session
import boto3

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

intg_accounts = defaultdict(
    lambda: "255242629704",
    {
        "devl": "189693026861", # DEVL
        "dcnt": "255242629704", # PROD-ESSP
        "test": "342797660180", # TEST
        "acpt": "254915557507", # ACPT
        "acnt": "255242629704", # PROD-ESSP
        "cont": "106139703084", # CONT
        "clve": "422946095386", # CLVE
        "prod": "106139703084", # PROD
        "burn": "255242629704", # PROD-ESSP
        "sdbx": "255242629704", # PROD-ESSP
    }
)

def get_target_parameter(account_id, region, name, session=None):
    if session is None:
        session = assume_session(
            'Onboarding', 'asap-stack-sets', (account_id, 'asap-cicd')
        )

    ssm = session.client('ssm', region_name=region)

    value = ssm.get_parameter(Name=name)["Parameter"]["Value"]
    return value

def set_target_parameter(account_id, region,
        name, value, session=None, **kwargs):
    if session is None:
        session = assume_session(
            'Onboarding', 'asap-stack-sets', (account_id, 'asap-cicd')
        )

    ssm = session.client('ssm', region_name=region)

    response = ssm.put_parameter(
            Name=name,
            Value=value,
            **kwargs
    )
    return response["Version"]



def get_target_lifecycle(account_id, region):
    lifecycle = get_target_parameter(account_id, region,
                                     "/fnma/account/lifecycle")
    return lifecycle


def get_target_lifecycle_simple(account_id, region):
    lifecycle = get_target_parameter(
        account_id, region, "/fnma/account/lifecyclesimple"
    )
    return lifecycle


def get_target_environment(account_id, region):
    environment = get_target_parameter(account_id, region,
                                       '/global/Environment')
    return environment


def get_target_account_name(account_id, region):
    account_name = get_target_parameter(account_id, region,
                                        '/global/AccountShortName')
    return account_name

def get_target_default_vpcid(account_id, region):
    vpc_id = get_target_parameter(account_id, region,
                                  '/provisioning/core/vpc/default/VpcId')
    return vpc_id


def get_is_target_account_contingency(account_id, region):
    "Is target account contg, based on the AccountName != Lifecycle"
    account_name = get_target_account_name(account_id, region)
    lifecycle = get_target_lifecycle(account_id, region)

    return account_name != lifecycle


def get_target_lambda_subnets(account_id, region):
    return get_target_parameter(
        account_id,
        region,
        "/provisioning/core/vpc/default/subnet/compute/connected/SubnetIds",
    )


def get_target_account_parameter_handler(event, context):
    account = event['Account']
    region = event['Region']
    name = event['ParameterName']

    return get_target_parameter(account, region, name)


def get_intg_account_info_handler(event, context):
    log.debug('get_target_lifecycle_intg_info_account_handler event: %s',
              json.dumps(event, default=str))
    account = event['Account']
    region = event['Region']
    lifecycletype_path = event['LifecycleTypePath']
#    intgaccount_prefix = event['IntgAccountForTypePathPrefix']
    intgname_path = event['IntgHostedZoneNamePath']
    intgnameserver_path = event['IntgHostedZoneNameServersPath']

    lifecycle_type = get_target_parameter(account, region, lifecycletype_path)
    intg_domainname = get_target_parameter(account, region, intgname_path)
    intg_nameservers = get_target_parameter(account, region,
                                            intgnameserver_path)

    intg_account = intg_accounts[lifecycle_type]
    intg_region = region

    return {
        'LifecycleType': lifecycle_type,
        'IntgHostedZoneName': intg_domainname,
        'IntgHostedZoneNameServers': intg_nameservers,
        'IntgAccountId': intg_account,
        'IntgRegion': intg_region
    }


if __name__ == '__main__':
    import unittest.mock as mock
    context = mock.MagicMock()

    event = {
        "Account": "068270649629",
        "Region": "us-east-1",
        "ParameterName": "/provisioning/core/topic/AWSResourceLifecycleEvents",
    }

    # print(get_target_account_parameter_handler(event, context))

    event = {
        "Account": "381057197743",
        "Region": "us-east-1",
        "LifecycleTypePath": "/global/LifecycleType",
        "IntgHostedZoneNamePath": "/provisioining/services/route53/IntgHostedZone/Name",
        "IntgHostedZoneNameServersPath": "/provisioining/services/route53/IntgHostedZone/NameServers"
    }

    print(get_intg_account_info_handler(event, context))
