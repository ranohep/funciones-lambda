import boto3
import json
# from iam import assume_session
session_name = 'cwl_Logdestination'

def assume_session(session_name, *account_role_tuples, **kwargs):
    sts = boto3.client('sts')
    cred_params = {}
    print('Source and target role input -{}'.format(account_role_tuples))
    for account_role in account_role_tuples:
        if isinstance(account_role, str) or len(account_role) == 1:
            account = sts.get_caller_identity()['Account']
            role = account_role
            print('Source role info',account,role)
        else:
            account, role = account_role
            print('Target role info',account,role)

        cred_params = dict(
            RoleArn=f'arn:aws:iam::{account}:role/{role}', RoleSessionName=session_name
        )

        print('Source and Target creds parameters for assume role',cred_params)

        response = sts.assume_role(**cred_params)
        print('assumed role response',response)
        cred = response["Credentials"]
        cred_params = dict(
            aws_access_key_id=cred["AccessKeyId"],
            aws_secret_access_key=cred["SecretAccessKey"],
            aws_session_token=cred["SessionToken"],
        )
        sts = boto3.client('sts', **cred_params)

    session = boto3.session.Session(**kwargs, **cred_params)
    return session

# def check_logdestination_on_account(target_account, region, lifecycle, log_destination):
def lambda_handler(event, context):

    #{
    #     "Comment": "Test Secret Manager SNS Access Task",
    #     "Account": "5221-6835-6138",
    #     "Region": "us-east-1"
    # }

    

    print('Events - {}'.format(event)) # Events - {'Accno': '739363106608', 'Region': 'us-east-1'}

    # for each_acc in event:

    target_account = event['Account']
    target_region = event['Region']

    # session = assume_session("asap-stack-sets", (target_account, "asap-cicd"))
    session = assume_session('Onboard',
                             "asap-stack-sets", (target_account, "asap-cicd"),
                             region_name=target_region)

    print('showing session assumed details - {}'.format(session))

    ssm_client = session.client('ssm', region_name=target_region)

    param_lifecycle = ssm_client.get_parameter(Name="/provisioning/default/common/Lifecycle")['Parameter']['Value']

    print('Target Account Number - {}'.format(target_account)) # Account Number -739363106608
    print('Target Region - {}'.format(target_region)) # Region - us-east-1
    print('Target Lifecycle - {}'.format(param_lifecycle))
    environment = param_lifecycle.split('-')[0]
    print('Environment - {}'.format(environment))

    src_acc_no = target_account

    if environment in ['devl','test','acpt']:

        destinationname = ''.join([
        'spitsi-',
        param_lifecycle,
        '-',
        src_acc_no,
        '-destination-01'
        ])

        destinationname = [destinationname]
        target_logdestination = 'acpt-log-261371583998'
        logdestination_region = 'us-east-1'

        print('Lower environment destination name - {}'.format(destinationname)) # arn:aws:logs:us-east-1:261371583998:destination:spitsi-devl-ess-781522252005-destination-01
        print('Lower environment target log destination -{}'.format(target_logdestination))
        print('Source account number for log destination policy- {}'.format(src_acc_no))
        logdestination_on_account = check_logdestination_on_account(src_acc_no,destinationname,target_logdestination,logdestination_region)

    
    elif environment in ['acnt','dcnt','tcnt']:

        destinationname = ''.join([
        'spitsi-',
        param_lifecycle,
        '-',
        src_acc_no,
        '-destination-01'
        ])

        destinationname = [destinationname]
        target_logdestination = 'acpt-logc-261371583998'
        logdestination_region = 'us-east-2'

        print('Lower environment destination name - {}'.format(destinationname)) # arn:aws:logs:us-east-1:261371583998:destination:spitsi-devl-ess-781522252005-destination-01
        print('Lower environment target log destination -{}'.format(target_logdestination))
        print('Source account number for log destination policy- {}'.format(src_acc_no))
        logdestination_on_account = check_logdestination_on_account(src_acc_no,destinationname,target_logdestination,logdestination_region)

    

    elif environment in ['clve','prod']:

        spitsi_destinationname = ''.join([
        'spitsi-',
        param_lifecycle,
        '-',
        src_acc_no,
        '-destination-01'
        ])

        # splkes-prod-log-applogs-prod-sfbu-771034845599-destination

        splkes_destinationname = ''.join([
        'splkes-prod-log-applogs-',
        param_lifecycle,
        '-',
        src_acc_no,
        '-destination'
        ])

        destinationname = [ spitsi_destinationname, splkes_destinationname ]
        target_logdestination = 'prod-log-827271557489'
        logdestination_region = 'us-east-1'

        print('Higher environment destination name - {}'.format(destinationname))
        print('Higher environment target log destination -{}'.format(target_logdestination))
        print('Source account number for log destination policy- {}'.format(src_acc_no))
        logdestination_on_account = check_logdestination_on_account(src_acc_no,destinationname,target_logdestination,logdestination_region)

    elif environment in ['cont']:

        spitsi_destinationname = ''.join([
        'spitsi-',
        param_lifecycle,
        '-',
        src_acc_no,
        '-destination-01'
        ])

        splkes_destinationname = ''.join([
        'splkes-cont-log-applogs-',
        param_lifecycle,
        '-',
        src_acc_no,
        '-destination'
        ])

        destinationname = [ spitsi_destinationname, splkes_destinationname ]
        target_logdestination = 'cont-log-827271557489'
        logdestination_region = 'us-east-2'

        print('Higher environment destination name - {}'.format(destinationname))
        print('Higher environment target log destination -{}'.format(target_logdestination))
        print('Source account number for log destination policy- {}'.format(src_acc_no))
        logdestination_on_account = check_logdestination_on_account(src_acc_no,destinationname,target_logdestination,logdestination_region)

    elif environment in ['burn','sdbx']:
        pass

    # logdestination_on_account = check_logdestination_on_account(src_acc_no,destinationname,target_logdestination,logdestination_region)

    
def check_logdestination_on_account(src_acc_no,destinationname,target_logdestination,logdestination_region):

    log_acc_no = target_logdestination.split('-')[2]
    target_logdestination_lf = target_logdestination.split('-')[0:2]
    logdestination_lf = '-'.join(target_logdestination_lf)

    print('Logdestination Lifecycle - {}'.format(logdestination_lf))

    print('Logdestination account number - {}'.format(log_acc_no))

    # session = assume_session("asap-stack-sets", (log_acc_no, "asap-cicd"))

    session = assume_session('Onboard',
                             "asap-stack-sets", (log_acc_no, "asap-cicd"),
                             region_name='us-east-1')

    print('showing session assumed details - {}'.format(session))

    cwl_client = session.client('logs', region_name=logdestination_region)

    log_destination_policy_resouce_arn_list = []

    for destination in destinationname:

        log_destination_policy_resouce_arn = ''.join([
            'arn:aws:logs:',
            logdestination_region,
            ':',
            log_acc_no,
            ':destination:',
            destination

        ])
        log_destination_policy_resouce_arn_list.append(log_destination_policy_resouce_arn)

    print('Log_destination_policy_resouce_arn_list -{}'.format(log_destination_policy_resouce_arn_list))

    log_destination_policy = {
        'Version': '2012-10-17',
        'Statement': [
            {
                'Sid': '',
                'Effect': 'Allow',
                'Principal': {
                    'AWS': 'source_accno'
                },
                'Action': 'logs:PutSubscriptionFilter',
                'Resource': log_destination_policy_resouce_arn_list
            }
        ]
    }

    for item in log_destination_policy['Statement']:
        item['Principal']['AWS'] = item['Principal']['AWS'].replace('source_accno',src_acc_no)
        # item['Resource']= item['Resource'].replace('target_region',logdestination_region).replace('target_account',log_acc_no).replace('destinationname',destinationname)

    # print('Logdestination policy -{}'.format(log_destination_policy))

    log_destination_access_policy = json.dumps(log_destination_policy)

    print('Logdestination policy - {}'.format(log_destination_access_policy))
    
    cwl_destination_paginator = cwl_client.get_paginator('describe_destinations')

    # spitsi-test-ess-285488728505-destination-01

    # try:

    print('destinationname',destinationname)
    
    for destinationname in destinationname:

        destinationtype = destinationname.split('-')[0]

        print('destinationtype',destinationtype)
        # spitsi-prod-bstn-998952144315-destination-01

        cwl_destination_response_iterator = cwl_destination_paginator.paginate(
            DestinationNamePrefix=destinationname,
            PaginationConfig={
                'StartingToken': None
            }
        )

        print('---',cwl_destination_response_iterator)

        for destination_details in cwl_destination_response_iterator:
            print(destination_details['destinations'])
            destination_details_length = len(destination_details['destinations'])

            try:

                if destination_details_length != 0:
                    for get_destination_name in destination_details['destinations']:
                        print(get_destination_name['destinationName'])
                        print('Already Log destination is there on Account - {}'.format(logdestination_lf))
                else:

                    if logdestination_region == 'us-east-1' and log_acc_no == '261371583998' and destinationtype == 'spitsi':

                        # print('Creating Log destination is in progress')
                        stream_arn = ''.join([
                            'arn:aws:kinesis:',
                            logdestination_region,
                            ':',
                            log_acc_no,
                            ':stream/spitsi-',
                            logdestination_lf,
                            '-stream-01'])

                        print(stream_arn)

                        role_arn = ''.join([
                            'arn:aws:iam::',
                            log_acc_no,
                            ':role/CWLtoKinesisRole']
                        )
                        

                        print('Acceptance Kinesis Role - {}'.format(role_arn))

                    elif logdestination_region == 'us-east-2' and log_acc_no == '261371583998' and destinationtype == 'spitsi':

                        # print('Creating Log destination is in progress')
                        stream_arn = ''.join([
                            'arn:aws:kinesis:',
                            logdestination_region,
                            ':',
                            log_acc_no,
                            ':stream/spitsi-',
                            logdestination_lf,
                            '-stream-01'])

                        print(stream_arn)

                        role_arn = ''.join([
                            'arn:aws:iam::',
                            log_acc_no,
                            ':role/Acpt-Logc-CWLtoKinesisRole']
                        )

                        print('Acceptance Contingency Kinesis Role - {}'.format(role_arn))

                    elif logdestination_region == 'us-east-1' and log_acc_no == '827271557489' and destinationtype == 'spitsi':

                        # print('Creating Log destination is in progress')
                        stream_arn = ''.join([
                            'arn:aws:kinesis:',
                            logdestination_region,
                            ':',
                            log_acc_no,
                            ':stream/spitsi-',
                            logdestination_lf,
                           '-stream-01'])

                        print(stream_arn)

                        role_arn = ''.join([
                            'arn:aws:iam::',
                            log_acc_no,
                            ':role/CWLtoKinesisRole']
                        )

                        print('Production Kinesis Role - {}'.format(role_arn))

                    elif logdestination_region == 'us-east-2' and log_acc_no == '827271557489' and destinationtype == 'spitsi':

                       # print('Creating Log destination is in progress')
                        stream_arn = ''.join([
                            'arn:aws:kinesis:',
                            logdestination_region,
                            ':',
                            log_acc_no,
                            ':stream/spitsi-',
                            logdestination_lf,
                            '-stream-01'])

                        print(stream_arn)

                        role_arn = ''.join([
                            'arn:aws:iam::',
                            log_acc_no,
                            ':role/ContCWLtoKinesisRole']
                        )

                        print('Contingency Kinesis Role - {}'.format(role_arn))

                    elif logdestination_region == 'us-east-1' and log_acc_no == '827271557489' and destinationtype == 'splkes':

                        # print('Creating Log destination is in progress')

                        # arn:aws:firehose:us-east-1:827271557489:deliverystream/splkes-prod-log-applogs-for-prod-firehose
                        stream_arn = ''.join([
                            'arn:aws:firehose:',
                            logdestination_region,
                            ':',
                            log_acc_no,
                            ':deliverystream/splkes-',
                            logdestination_lf,
                            '-applogs-for-prod-firehose'])

                        print(stream_arn)
                        
                        # arn:aws:iam::827271557489:role/splkes-prod-log-applogs-prod-cloudwatch-destination-role
                        
                        role_arn = ''.join([
                            'arn:aws:iam::',
                            log_acc_no,
                            ':role/splkes-prod-log-applogs-prod-cloudwatch-destination-role']
                        )

                        print('Production Kinesis Firehose Role - {}'.format(role_arn))

                    elif logdestination_region == 'us-east-2' and log_acc_no == '827271557489' and destinationtype == 'splkes':

                        # print('Creating Log destination is in progress')
                        stream_arn = ''.join([
                            'arn:aws:firehose:',
                            logdestination_region,
                            ':',
                            log_acc_no,
                            ':deliverystream/splkes-',
                            logdestination_lf,
                            '-applogs-for-cont-firehose'])

                        print(stream_arn)

                        role_arn = ''.join([
                            'arn:aws:iam::',
                            log_acc_no,
                            ':role/splkes-cont-log-applogs-cont-cloudwatch-destination-role']
                        )

                        print('Contingency Kinesis firehose Role - {}'.format(role_arn))

                    cwl_destination_creation = cwl_client.put_destination(
                        destinationName=destinationname,
                        targetArn=stream_arn,
                        roleArn=role_arn
                    )

                    # print(cwl_destination_response)

                    if cwl_destination_creation['ResponseMetadata']['HTTPStatusCode'] == 200:
                        print('Logdestination created now - {} - {}'.format(log_acc_no,logdestination_lf))

                    
                    cwl_put_destination_policy = cwl_client.put_destination_policy(
                        destinationName=destinationname,
                        accessPolicy=log_destination_access_policy
                    )

                    # print(cwl_put_destination_policy)
                    if cwl_put_destination_policy['ResponseMetadata']['HTTPStatusCode'] == 200:
                        print('Logdestination Policy added on - {}'.format(destinationname))
   
            except:
                print('Error occured while creating logdestination')


