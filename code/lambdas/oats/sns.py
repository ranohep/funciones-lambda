#!/usr/bin/env python3

import logging
import json
from collections import OrderedDict
import boto3
from iam import assume_session
from ssm import get_target_parameter

logging.basicConfig(level=logging.WARN)
log = logging.getLogger(__file__)
log.setLevel(logging.DEBUG)


class TopicAccessPolicyInvalid(Exception):
    "Access Policy for topic wasn't in form allowing modifications"
    pass


def get_sns_policy(topic_account, region, topic_name, session):

    topic_arn = ':'.join(['arn:aws:sns', region, topic_account, topic_name, ])
    policy = get_sns_policy_by_arn(topic_arn, session=session)

    return topic_arn, policy


def get_sns_policy_by_arn(topic_arn, session):
    sns = session.client('sns')

    response = sns.get_topic_attributes(TopicArn=topic_arn)
    attributes = response['Attributes']
    policy = json.loads(attributes['Policy'], object_pairs_hook=OrderedDict)

    return policy


def set_sns_policy(topic_arn, region, policy, session):
    sns = session.client('sns', region_name=region)

    sns.set_topic_attributes(
        TopicArn=topic_arn,
        AttributeName='Policy',
        AttributeValue=json.dumps(policy)
    )


def update_policy_for_account_role(policy, role_name, new_account):
    """
    Update policy by adding an account role in a statement that has
    other account with same role
    """

    account_princ = ':'.join(
        ['arn:aws:iam', '', new_account, f'role/{role_name}']
    )

    for stmt in policy['Statement']:
        aws_princ = stmt['Principal']['AWS']
        if isinstance(aws_princ, str):
            if aws_princ.split(':')[-1] == f'role/{role_name}':
                stmt['Principal']['AWS'] = [
                    aws_princ,
                    account_princ
                ]
                return True
        else:
            roles = [p.split(':')[-1] for p in aws_princ]
            if f'role/{role_name}' in roles:
                if account_princ in aws_princ:
                    return False
                else:
                    # filter out unique ids for deleted accounts
                    aws_princ = \
                        [p for p in aws_princ if not p.startswith('AROA')]
                    aws_princ.append(account_princ)
                    stmt['Principal']['AWS'] = aws_princ
                    return True
    else:
        raise TopicAccessPolicyInvalid


def update_sns_policy_handler(event, context):
    """
    Find statement that AWS Principal has same role for different account
    and append this new account
    """
    log.debug('update_sns_policy_handler: %s', json.dumps(event))

    account = event['Account']
    region = event['Region']
    topic_account = event['TopicAccount']
    topic_name = event['TopicName']
    topic_region = event['TopicRegion']
    role_name = event['RolePathAndName']

    session = assume_session(
        'OnboardAccount', 'asap-stack-sets', (topic_account, 'asap-cicd'),
        region_name=topic_region
    )

    topic_arn, policy = get_sns_policy(
        topic_account, topic_region, topic_name, session=session
    )

    log.debug('Policy for %s: %s', topic_arn, json.dumps(policy))
    try:
        if update_policy_for_account_role(policy, role_name, account):
            log.debug('Policy change for %s: %s',
                      topic_arn, json.dumps(policy))
            set_sns_policy(topic_arn, region, policy, session)
            log.info('%s/%s added to %s', account, role_name, topic_arn)
        else:
            log.info('%s/%s already access to %s',
                     account, role_name, topic_arn)

    except TopicAccessPolicyInvalid:
        log.error(
            '%s access policy was not able to be modified for %s/%s',
            topic_arn, account, role_name
        )
        raise


def update_sns_policy_from_ssm_parameter_handler(event, context):
    "Update the SNS policy for Secret Manager Resource Topic"
    log.debug('update_secret_manager_sns_policy_handler: %s',
              json.dumps(event))
    account = event['Account']
    region = event['Region']
    role_name = event['RolePathAndName']
    parameter_name = event.get(
        'Parameter',
        '/provisioning/core/topic/AWSResourceLifecycleEvents'
    )

    log.debug('Getting %s from %s @ %s', parameter_name, account, region)
    sns_arn = get_target_parameter(account, region, parameter_name)
    sns_region, sns_account = sns_arn.split(':')[3:5]

    log.info('Updating policy on %s', sns_arn)

    session = assume_session('OnboardAccount', 'asap-stack-sets',
                             (sns_account, 'asap-cicd'),
                             region_name=sns_region)

    policy = get_sns_policy_by_arn(sns_arn, session=session)
    log.debug('SNS policy for %s: %s', sns_arn, json.dumps(policy))
    try:
        if update_policy_for_account_role(policy, role_name, account):
            log.debug('Policy change for %s/%s: %s',
                      account, role_name, json.dumps(policy))
            set_sns_policy(sns_arn, sns_region, policy, session)
            print(json.dumps(policy, indent=4))
            log.info('%s/%s added to %s', account, role_name, sns_arn)
        else:
            log.info('%s/%s already access to %s',
                     account, role_name, sns_arn)
    except TopicAccessPolicyInvalid:
        log.error(
            '%s access policy was not able to be modified for %s/%s',
            sns_arn, account, role_name
        )
        raise


if __name__ == '__main__':
    import unittest.mock as mock

    context = mock.MagicMock()

    event = {
        'Account': '489754306130',
        'Region': 'us-east-1',
        "TopicAccount": "189693026861",
        'TopicRegion': 'us-east-1',
        'TopicName': 'fnma-awstransformation-notification',
        'RolePathAndName': 'fnma/asap/asap-compute'
    }

    print(update_sns_policy_handler(event, context))

    event = {
        # "Account": "068270649629",
        # "Account": "781522252005",
        "Account": "172587047172",
        "Region": "us-east-1",
        'Parameter': 's5ugys-resource-test-topic',
        'RolePathAndName': 'asap-yapr-resource-lifecycle-events-notify'
    }

    # print(update_sns_policy_from_ssm_parameter_handler(event, context))
