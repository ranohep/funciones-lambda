import pytest
import unittest.mock as mock
import windows
from windows import ActiveDirectoryEnvironment

TEST_INFO = {
    "Production": {
        "Name": "prod.aws.fanniemae.com",
        "AccountId": "11111111111"
    },
    "NonProduction": {
        "Name": "testdir3.devl-ceng.aws.fanniemae.com",
        "AccountId": "172587047172"
    }
}

def mock_get_target_environment(account, region):
    return account

windows.get_target_environment = mock_get_target_environment

def test_prod_info():
    ad = ActiveDirectoryEnvironment(TEST_INFO, "Production", "ue1")
    assert ad.name == TEST_INFO["Production"]["Name"]
    assert ad.account_id == TEST_INFO["Production"]["AccountId"]


def test_contg_info():
    ad = ActiveDirectoryEnvironment(TEST_INFO, "Contingency", "ue1")
    assert ad.name == TEST_INFO["Production"]["Name"]
    assert ad.account_id == TEST_INFO["Production"]["AccountId"]

def test_clve_info():
    ad = ActiveDirectoryEnvironment(TEST_INFO, "CLVE", "ue1")
    assert ad.name == TEST_INFO["Production"]["Name"]
    assert ad.account_id == TEST_INFO["Production"]["AccountId"]

def test_other_info():
    ad = ActiveDirectoryEnvironment(TEST_INFO, "Development", "ue1")
    assert ad.name == TEST_INFO["NonProduction"]["Name"]
    assert ad.account_id == TEST_INFO["NonProduction"]["AccountId"]

@mock.patch("windows.assume_session")
def test_windows_with_no_directories(assume_session):
    sdbx_account = "522168356138"
    ad = ActiveDirectoryEnvironment(TEST_INFO, sdbx_account, "us-east-1")

    ds = mock.Mock()
    ds.describe_directories.return_value = {
            "DirectoryDescriptions": []
    }
    assume_session.return_value.client.return_value = ds
    assert ad.owner_id is None

@mock.patch("windows.assume_session")
def test_active_directory_with_no_match_directory(assume_session):
    sdbx_account = "522168356138"
    ad = ActiveDirectoryEnvironment(TEST_INFO, sdbx_account, "us-east-1")

    ds = mock.Mock()
    ds.describe_directories.return_value = {
            "DirectoryDescriptions": [
                {
                    "DirectoryId": "somenumber",
                    "Name": "NOT_A_DIRECTORY_NAME",
                    "Type": "MicrosoftAD"
                }
            ]
    }
    assume_session.return_value.client.return_value = ds
    assert ad.owner_id is None

@mock.patch("windows.assume_session")
def test_active_directory_with_match_directory_wrong_type(assume_session):
    sdbx_account = "522168356138"
    ad = ActiveDirectoryEnvironment(TEST_INFO, sdbx_account, "us-east-1")

    ds = mock.Mock()
    ds.describe_directories.return_value = {
            "DirectoryDescriptions": [
                {
                    "DirectoryId": "somenumber",
                    "Name": TEST_INFO["NonProduction"]["Name"],
                    "Type": "NOT MS AD"
                }
            ]
    }
    assume_session.return_value.client.return_value = ds
    assert ad.owner_id is None

@mock.patch("windows.assume_session")
def test_active_directory_with_match_directory(assume_session):
    sdbx_account = "522168356138"
    ad = ActiveDirectoryEnvironment(TEST_INFO, sdbx_account, "us-east-1")
    directory_id = "d-123456789"

    ds = mock.Mock()
    ds.describe_directories.return_value = {
            "DirectoryDescriptions": [
                {
                    "DirectoryId": directory_id,
                    "Name": TEST_INFO["NonProduction"]["Name"],
                    "Type": "MicrosoftAD"
                }
            ]
    }
    assume_session.return_value.client.return_value = ds
    assert ad.owner_id == directory_id

@mock.patch("windows.set_target_parameter")
@mock.patch("windows.assume_session")
def test_handler(assume_session, set_target_parameter):
    sdbx_ceng = "522168356138"
    directory_id = "d-123456789"
    event = {
        "Account": sdbx_ceng,
        "Region": "us-east-1",
        "ConsumerDirectoryPath": "/s5ugys/ad",
        "ActiveDirectoryInfo": {
            "Production": {
                "Name": "fmadprod.prod-essp.aws.fanniemae.com",
                "AccountId": "255242629704"
            },
            "NonProduction": {
                #"Name": "fmadnonprod.prod-essx.aws.fanniemae.com",
                #"AccountId": "068270649629"
                "Name": "testdir3.devl-ceng.aws.fanniemae.com",
                "AccountId": "172587047172"
            }
        }
    }
    ds = mock.Mock()
    ds.describe_directories.return_value = {
            "DirectoryDescriptions": [
                {
                    "DirectoryId": directory_id,
                    "Name": TEST_INFO["NonProduction"]["Name"],
                    "Type": "MicrosoftAD"
                }
            ]
    }
    new_consumer_id = "consumer_id"
    ds.share_directory.return_value = {"SharedDirectoryId": new_consumer_id}
    assume_session.return_value.client.return_value = ds
    

    context = mock.Mock()
    windows.share_ad_directory_handler(event, context)

    ds.share_directory.assert_called_with(
            DirectoryId=directory_id,
            ShareTarget={"Id": sdbx_ceng, "Type": "ACCOUNT"},
            ShareMethod="HANDSHAKE"
    )

    ds.accept_shared_directory.assert_called_with(
            SharedDirectoryId=new_consumer_id
    )

    set_target_parameter.assert_called_with(
            event["Account"], event["Region"],
            event["ConsumerDirectoryPath"], new_consumer_id,
            Type="String", Overwrite=True)

    


