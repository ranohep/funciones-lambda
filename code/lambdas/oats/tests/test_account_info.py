import pytest
from unittest import mock
import io
import json
import boto3
from botocore.stub import Stubber
import account_info

context = mock.MagicMock()
context.function_name = "test_function"

devl_ceng = "172587047172"
devl_ceng_region = "us-east-1"
devl_ccoe = "648156244979"
devl_ccoe_region = "us-east-1"
bucket_name = "fnma-asap-devl-ceng"
prefix = "asap-oats/maas-data"
execution_id = "test"

repo = "s5ugys-test"
branch = "master"
path = "accounts"

get_account_details_event = {
    "Account": devl_ccoe,
    "Region": devl_ccoe_region,
    "BucketName": bucket_name,
    "Prefix": prefix,
    "ExecutionId": execution_id,
    "AccountGeneration": "v2",
    "SsmParameters": [
        "/global/Lifecycle"
    ]
}

@mock.patch("account_info.assume_session")
def test_get_account_details_handler_session_error(assume_session):
    "Test session error"

    assume_session.side_effect = Exception()
    with pytest.raises(account_info.TargetAccountAssumeSessionError):
        account_info.get_account_details_handler(get_account_details_event,
                context)

@mock.patch("account_info.get_account_parameters")
@mock.patch("account_info.assume_session")
@mock.patch("account_info.boto3.client")
def test_get_account_parameters_for_one_account(client, session,
        account_parameters):
    account_parameters.return_value = {}
    response = account_info.get_account_details_handler(
            get_account_details_event, context)
    account_parameters.assert_called_once()
    assert account_parameters.call_args[0][0] == \
            get_account_details_event["SsmParameters"]
    assert response["Bucket"] == get_account_details_event["BucketName"]
    assert response["Key"].startswith(get_account_details_event["Prefix"])

def test_no_ssm_parameters_in_path():
    "Test SsmParameters has path doesn't exist"
    ssm = mock.Mock()
    ssm.get_parameters_by_path.return_value = {"Parameters": []}
    session = mock.Mock()
    session.client.return_value = ssm
    response = account_info.get_account_parameters(["/EMPTY/"], session)
    assert response == {}

def test_ssm_parameter_not_exist():
    "Test SsmParameter doesn't exist"
    ssm = mock.Mock()
    ssm.get_parameter.side_effect = Exception()
    session = mock.Mock()
    session.client.return_value = ssm

    response = account_info.get_account_parameters(["/EMTPY"], session)
    assert response == {}

def test_ssm_parameter_exists():
    "Test SsmParameter exists"
    key = "/PARAMETER"
    expected_value = {key: "Value"}
    ssm = mock.Mock()
    ssm.get_parameter.return_value = {
        "Parameter": {
            "Value": expected_value[key]
        }
    }
    session = mock.Mock()
    session.client.return_value = ssm

    response = account_info.get_account_parameters(expected_value.keys(),
            session)

    assert response == expected_value

def test_ssm_parameters_exists():
    "Test SsmParameter exists"
    d = "/PARAMETER/"
    key = f"{d}Key"
    expected_value = {key: "Value"}
    ssm = mock.Mock()
    ssm.get_parameters_by_path.return_value = {
            "Parameters": [{"Name": key, "Value": expected_value[key]}]
    }
    session = mock.Mock()
    session.client.return_value = ssm

    response = account_info.get_account_parameters([d], session)

    assert response == expected_value

@mock.patch("account_info.boto3.client")
def test_get_account_details_handler(client):
    
    response = account_info.get_account_details_handler(
            get_account_details_event, context)
    put_object_args = client.return_value.put_object.call_args_list[0][1] 
    assert put_object_args["Bucket"] == get_account_details_event["BucketName"]
    assert put_object_args["Key"] == "{}/{}/{}-{}.json".format(
            get_account_details_event["Prefix"],
            get_account_details_event["ExecutionId"],
            get_account_details_event["Account"],
            get_account_details_event["Region"])


persist_event = {
    "SourceAccountId": devl_ceng,
    "SourceAccountRegion": devl_ceng_region,
    "Repository": repo,
    "RepositoryBranch": branch,
    "RepositoryPath": path,
}

@mock.patch("account_info.boto3.client")
def test_gather_s3_objects_does_not_throw(client):
    s3 = mock.Mock()
    s3.get_object.side_effect = Exception()
    client.return_value = s3

    s3_objects = {
            "Bucket": bucket_name,
            "Key": "{}/{}/{}-{}.json".format(
                path, execution_id, devl_ccoe, devl_ceng_region),
            "VersionId": "some_version"
    }

    r = account_info.gather_s3_objects(s3_objects)
    assert r == []

@mock.patch("account_info.boto3.client")
def test_gather_s3_objects_get_and_delete_count_equals_diff_bucket(client):
    s3 = mock.Mock()
    account_files = (
            {"Account": str(idx), "SsmParameters": {
                "/global/Lifecycle": str(idx)}}
            for idx in range(0,10))
    s3.get_object.side_effect = (
            {"Body": io.StringIO(json.dumps(a))} for a in account_files)
    s3.delete_objects.return_value = {}
    client.return_value = s3

    s3_objects = []
    for key_idx in range(0 ,9):
        s3_objects.append({
            "Bucket": f"bucket_{key_idx}",
            "Key": f"prefix/{key_idx}",
            "VersionId": "some_version"
        })

    r = account_info.gather_s3_objects(s3_objects)
    assert s3.get_object.call_count == s3.delete_objects.call_count

@mock.patch("account_info.boto3.client")
def test_gather_s3_objects_get_and_delete_count_equals_comm_bucket(client):
    s3 = mock.Mock()
    account_files = (
            {"Account": str(idx), "SsmParameters": {
                "/global/Lifecycle": str(idx)}}
            for idx in range(0,10))
    s3.get_object.side_effect = (
            {"Body": io.StringIO(json.dumps(a))} for a in account_files)
    s3.delete_objects.return_value = {}
    client.return_value = s3

    s3_objects = []
    for key_idx in range(0 ,9):
        s3_objects.append({
            "Bucket": f"common_bucket",
            "Key": f"prefix/{key_idx}",
            "VersionId": "some_version"
        })

    r = account_info.gather_s3_objects(s3_objects)
    assert s3.get_object.call_count == len(s3_objects)
    assert s3.delete_objects.call_count == 1

@mock.patch("account_info.assume_session")
def test_persist_account_info_zero_account_details(assume_session):
    repo_account = devl_ceng
    repo_region = "us-east-1"
    repo = "s5ugys-test"
    repo_branch = "master"
    repo_path = "account"

    account_details = []
    r = account_info.persist_account_info_to_codecommit(repo_account,
            repo_region, repo, repo_branch, repo_path, account_details)
    assert r == {}

@mock.patch("account_info.assume_session")
def test_persist_account_info_one_account_details(assume_session):
    repo_account = devl_ceng
    repo_region = "us-east-1"
    repo = "s5ugys-test"
    repo_branch = "master"
    repo_path = "account"

    account_details = [
        {
            "Account": "account",
            "SsmParameters": {"/global/Lifecycle": "Lifecycle"}
        }
    ]
    with mock.patch("account_info.get_branch_commit") as get_branch:
        with mock.patch("account_info.commit_files") as commit_files:
            commit_files.return_value = ("commit", "ts")
            r = account_info.persist_account_info_to_codecommit(repo_account,
                    repo_region, repo, repo_branch, repo_path, account_details)
            get_branch.assert_called()
            commit_files.assert_called()

@mock.patch("account_info.assume_session")
def test_persist_account_info_multiple_account_details(assume_session):
    repo_account = devl_ceng
    repo_region = "us-east-1"
    repo = "s5ugys-test"
    repo_branch = "master"
    repo_path = "account"

    account_details = [
        {
            "Account": "account1",
            "SsmParameters": {"/global/Lifecycle": "Lifecycle1"}
        },
        {
            "Account": "account2",
            "SsmParameters": {"/global/Lifecycle": "Lifecycle2"}
        }
    ]
    with mock.patch("account_info.sync_folders") as sync_folders:
        sync_folders.return_value = ("commit", "ts")
        r = account_info.persist_account_info_to_codecommit(repo_account,
                repo_region, repo, repo_branch, repo_path, account_details)
        sync_folders.assert_called()


