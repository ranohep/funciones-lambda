import pytest
from unittest import mock
import codecommit

repo_name = "s5ugys_test"
repo_branch = "master"


@mock.patch("codecommit.get_codecommit_folder")
@mock.patch("codecommit.get_branch_commit")
def test_xtns_with_empty_folder(get_branch_commit, get_codecommit_folder):
    get_branch_commit.return_value = "commit_id"
    get_codecommit_folder.side_effect = codecommit.FolderDoesNotExistException
    session = mock.Mock()
    files = [(f"path/{p}", f"body/{p}") for p in range(0,10)]

    commit_id, xtns = codecommit.sync_xtns(repo_name, "branch", files, session)
    get_branch_commit.assert_called()
    assert commit_id == get_branch_commit.return_value
    assert {t[0] for t in files} == {t["filePath"] for t in xtns}
    assert all("fileContent" in t for t in xtns)

@mock.patch("codecommit.get_codecommit_folder")
def test_xtns_delete_extra_files(get_codecommit_folder):
    get_codecommit_folder.return_value = (
            "commit_id", [{"absolutePath": "delete/me"}])
    session = mock.Mock()
    files = [(f"path/{p}", f"body/{p}") for p in range(0,10)]

    commit_id, xtns = codecommit.sync_xtns(repo_name, "branch", files, session)
    assert commit_id == get_codecommit_folder.return_value[0]
    delete_paths = {t["filePath"] for t in xtns if "fileContent" not in t}
    assert delete_paths == {t["absolutePath"] 
            for t in get_codecommit_folder.return_value[1]}
    assert {t[0] for t in files} == \
            {t["filePath"] for t in xtns if "fileContent" in t}


def test_sync_folders_head_outdated_pulls_new_commit_id():
    import boto3
    exceptions = boto3.client("codecommit").exceptions
    cc = mock.Mock()
    cc.exceptions = exceptions
    cc.create_commit.side_effect = [
            exceptions.ParentCommitIdOutdatedException({},""),
            {"commitId": "commited"}]
    session = mock.Mock()
    session.client.return_value = cc

    with mock.patch("codecommit.sync_xtns") as sync_xtns:
        sync_xtns.return_value = ("commit", [{}])
        with mock.patch("codecommit.get_branch_commit") as get_branch_commit:
            with mock.patch("codecommit.get_commit_timestamp"):
                codecommit.sync_folders(repo_name, "commit_msg", repo_branch,
                        [], session)
                assert cc.create_commit.call_count == 2
                get_branch_commit.assert_called()

def test_sync_folders_no_change():
    import boto3
    exceptions = boto3.client("codecommit").exceptions
    cc = mock.Mock()
    cc.exceptions = exceptions
    cc.create_commit.side_effect = exceptions.NoChangeException({}, "")
    session = mock.Mock()
    session.client.return_value = cc

    with mock.patch("codecommit.sync_xtns") as sync_xtns:
        sync_xtns.return_value = ("commit_sync_id", [{}])
        with mock.patch("codecommit.get_commit_timestamp"):
            commit_id, _ = codecommit.sync_folders(repo_name, "commit_msg",
                    repo_branch, [], session)

            cc.create_commit.call_count == 1
            assert commit_id == sync_xtns.return_value[0]

def test_sync_folders_shrink_batch():
    import boto3
    exceptions = boto3.client("codecommit").exceptions
    cc = mock.Mock()
    cc.exceptions = exceptions
    cc.create_commit.side_effect = [
            exceptions.MaximumFileEntriesExceededException({}, ""),
            {"commitId": "commit1"}, {"commitId": "commit2"}]
    session = mock.Mock()
    session.client.return_value = cc

    with mock.patch("codecommit.sync_xtns") as sync_xtns:
        sync_xtns.return_value = ("commit_sync_id", [{}, {}])
        with mock.patch("codecommit.get_commit_timestamp"):
            commit_id, _ = codecommit.sync_folders(repo_name, "commit_msg",
                    repo_branch, [], session)

            cc.create_commit.call_count == 1
            assert commit_id == "commit2"

def test_sync_folders_shrink_batch_ends_at_one():
    import boto3
    exceptions = boto3.client("codecommit").exceptions
    cc = mock.Mock()
    cc.exceptions = exceptions
    cc.create_commit.side_effect = \
            exceptions.MaximumFileEntriesExceededException({}, "")
    session = mock.Mock()
    session.client.return_value = cc

    with mock.patch("codecommit.sync_xtns") as sync_xtns:
        sync_xtns.return_value = ("commit_sync_id", [{}, {}, {}])
        with pytest.raises(exceptions.MaximumFileEntriesExceededException):
            codecommit.sync_folders(repo_name, "commit_msg",
                    repo_branch, [], session)

            cc.create_commit.call_count == 2

