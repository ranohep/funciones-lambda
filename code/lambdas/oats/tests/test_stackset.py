import pytest
from unittest import mock
import stackset

import boto3
from botocore.stub import Stubber

def test_stackset_not_exist():
    """Stackset doesn't exist"""

    stackset_name = 'does-not-exist'

    cfn = boto3.client('cloudformation')
    stubber = Stubber(cfn)
    stubber.add_client_error('describe_stack_instance', 'StackSetNotFoundException')
    stubber.activate()

    session = mock.Mock()
    session.client.return_value = cfn

    with pytest.raises(stackset.StackSetNotFoundError):
        status = stackset.stackset_instance_status(
            session,
            stackset_name,
            '123456789012',
            'us-east-1',
            '123456789012',
            'us-east-1'
        )


def test_stackset_instance_not_exist():
    """Stackset Instance doesn't exist"""

    stackset_name = 'SampleStackSet2'

    cfn = boto3.client('cloudformation')
    stubber = Stubber(cfn)
    stubber.add_client_error('describe_stack_instance', 'StackInstanceNotFoundException')
    stubber.activate()

    session = mock.Mock()
    session.client.return_value = cfn

    with pytest.raises(stackset.StackInstanceNotFoundError):
        status = stackset.stackset_instance_status(
            session,
            stackset_name,
            '123456789012',
            'us-east-1',
            '123456789012',
            'us-east-1'
        )

def test_stackset_inoperable():
    """Stackset Instance in inoperable state"""

    stackset_name = 'SampleStackSet2'
    account = '123456789012'
    region = 'us-east-1'

    cfn = boto3.client('cloudformation')
    stubber = Stubber(cfn)
    response = {
        'StackInstance': {
            'Status': 'INOPERABLE'
        }
    }
    expected_params = dict(
            StackSetName=stackset_name,
            StackInstanceAccount=account,
            StackInstanceRegion=region)
    stubber.add_response('describe_stack_instance', response, expected_params)
    stubber.activate()

    session = mock.Mock()
    session.client.return_value = cfn

    with pytest.raises(stackset.StackInstanceInoperableError):
        status = stackset.stackset_instance_status(
            session,
            stackset_name,
            '123456789012',
            'us-east-1',
            '123456789012',
            'us-east-1'
        )

def test_stackset_generic_error():
    """Stackset Instance in inoperable state"""

    stackset_name = 'SampleStackSet2'
    account = '123456789012'
    region = 'us-east-1'

    cfn = boto3.client('cloudformation')
    stubber = Stubber(cfn)
    stubber.add_client_error('describe_stack_instance')
    stubber.activate()

    with mock.patch('stackset.boto3.client') as client:
        client.return_value = cfn
        with pytest.raises(Exception):
            status = stackset.stackset_instance_status(
                stackset_name,
                '123456789012',
                'us-east-1'
            )

