#!/usr/bin/env python3

import logging
import json

from iam import assume_session
from ssm import get_target_lifecycle

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


class DomainNotFound(Exception):
    pass


class DomainRegistrationInProcess(Exception):
    pass


class TransferLockInProgress(Exception):
    pass


class TransferLockError(Exception):
    pass


class EnableTransferLock(Exception):
    "Transfer Lock Needs Enabled"
    pass


class WaitForDomain(Exception):
    "Domain is not currently avail- try again later"
    pass


def target_route53domains(account, admin_role, exec_role):
    session = assume_session('Onboarding', admin_role, (account, exec_role))
    r53d = session.client('route53domains')

    return r53d


def register_domain(account, domain_name, contact, admin_role, exec_role):
    r53d = target_route53domains(account, admin_role, exec_role)

    params = dict(
        DomainName=domain_name,
        DurationInYears=3,
        AutoRenew=True,
        AdminContact=contact,
        RegistrantContact=contact,
        TechContact=contact,
        PrivacyProtectAdminContact=True,
        PrivacyProtectRegistrantContact=True,
        PrivacyProtectTechContact=True,
    )

    log.debug('Registing domain with: %s', json.dumps(params))
    response = r53d.register_domain(**params)

    log.debug('Register Domain returned: %s', json.dumps(response))


def register_domain_handler(event, context):
    account = event['Account']
    region = event['Region']
    lifecycle = get_target_lifecycle(account, region)
    domain_name = '-'.join([lifecycle, 'mort.net'])

    admin_role = event.get('AdminRole', 'asap-stack-sets')
    exec_role = event.get('ExecRole', 'asap-cicd')

    contact = dict(
        FirstName='Cloud',
        LastName='CoE',
        ContactType='PERSON',
        AddressLine1='11600 American dream way',
        City='Reston',
        State='VA',
        CountryCode='US',
        ZipCode='20190',
        PhoneNumber='+1.7038331000',
        Email='aws_cloud_coe@fanniemae.com',
    )

    register_domain(
        account=account,
        domain_name=domain_name,
        contact=contact,
        admin_role=admin_role,
        exec_role=exec_role,
    )


def transfer_lock(account, domain_name, admin_role, exec_role):
    log.info('transfer lock on %s', domain_name)
    r53d = target_route53domains(account, admin_role, exec_role)

    try:
        domain_details = r53d.get_domain_detail(DomainName=domain_name)
        log.debug('domain details = %s',
                  json.dumps(domain_details, default=str))
    except Exception:
        raise WaitForDomain(domain_name)

    operation = 'DOMAIN_LOCK'
    op = check_domain_operation(account, operation, domain_name, admin_role,
                                exec_role)
    log.debug('DOMAIN LOCK operation: %s', op)
    if op is None:
        log.info('Enabling Domain Transfer Lock for %s', domain_name)
        response = r53d.enable_domain_transfer_lock(DomainName=domain_name)
        log.debug('response for transfer lock: %s', response)
    else:
        status = op['Status']
        if status not in ['IN_PROGRESS', 'SUBMITTED', 'SUCCESSFUL']:
            raise TransferLockError(op)


def transfer_lock_domain_handler(event, context):
    account = event['Account']
    region = event['Region']
    lifecycle = get_target_lifecycle(account, region)
    domain_name = '-'.join([lifecycle, 'mort.net'])

    admin_role = event.get('AdminRole', 'asap-stack-sets')
    exec_role = event.get('ExecRole', 'asap-cicd')

    transfer_lock(account, domain_name, admin_role, exec_role)


def domain_status(account, domain_name, admin_role, exec_role):
    r53d = target_route53domains(account, admin_role, exec_role)

    try:
        response = r53d.get_domain_detail(DomainName=domain_name)
        log.debug('domain status = %s', json.dumps(response, default=str))

        if 'clientTransferProhibited' not in response['StatusList']:
            raise EnableTransferLock
    except r53d.exceptions.InvalidInput:
        raise DomainNotFound


def domain_status_handler(event, context):
    log.debug('Event: %s', json.dumps(event))
    account = event['Account']
    region = event['Region']
    lifecycle = get_target_lifecycle(account, region)
    domain_name = '-'.join([lifecycle, 'mort.net'])

    admin_role = event.get('AdminRole', 'asap-stack-sets')
    exec_role = event.get('ExecRole', 'asap-cicd')

    log.info('Registering %s on %s/%s', domain_name, account, region)
    domain_status(account, domain_name, admin_role, exec_role)


def check_domain_operation(
    account,
    operation,
    domain_name,
    admin_role,
    exec_role
):
    r53d = target_route53domains(account, admin_role, exec_role)

    params = dict()
    while True:
        response = r53d.list_operations(**params)

        for operation in response['Operations']:
            if operation['Type'] == operation:
                try:
                    op_response = r53d.get_operation_detail(operation['OperationId'])
                    if op_response['DomainName'] == domain_name:
                        return op_response
                except:
                    pass

        if 'NextPageMarker' in response:
            params['Marker'] = response['NextPageMarker']
        else:
            break

    return None


def check_domain_operation_handler(event, context):
    log.debug('Event: %s', json.dumps(event))
    account = event['Account']
    region = event['Region']
    operation = event['Operation']

    lifecycle = get_target_lifecycle(account, region)
    domain_name = '-'.join([lifecycle, 'mort.net'])

    admin_role = event.get('AdminRole', 'asap-stack-sets')
    exec_role = event.get('ExecRole', 'asap-cicd')

    log.info('Checking %s on %s/%s', domain_name, account, region)

    op = check_domain_operation(account, operation, domain_name, admin_role, exec_role)

    return op

def tag_public_zone(account, domain_name, admin_role, exec_role, tags):
    log.info('tag %s: %s', domain_name, tags)
    r53d = target_route53domains(account, admin_role, exec_role)

    response = r53d.update_tags_for_domain(
            DomainName=domain_name,
            TagsToUpdate=tags
    )

    log.debug("Response tagging %s: %s", domain_name, response)


def tag_public_zone_handler(event, context):
    account = event['Account']
    region = event['Region']
    lifecycle = get_target_lifecycle(account, region)
    domain_name = '-'.join([lifecycle, 'mort.net'])
    tags = event["Tags"]

    admin_role = event.get('AdminRole', 'asap-stack-sets')
    exec_role = event.get('ExecRole', 'asap-cicd')

    tag_public_zone(account, domain_name, admin_role, exec_role, tags)


def hosted_zone_id(hosted_zone_name, session):
    r53 = session.client('route53')
    log.debug('Retrieving hosted_zone %s', hosted_zone_name)
    response = r53.list_hosted_zones_by_name(
        DNSName=hosted_zone_name,
        MaxItems='1'
    )
    log.debug('Hosted Zone %s details: %s', hosted_zone_name, response)

    hosted_zones = response.get('HostedZones', [])
    if len(hosted_zones) == 0:
        log.warning('Hosted Zone %s Not Found', hosted_zone_name)
        return None

    if hosted_zones[0]['Name'].rstrip(".") == hosted_zone_name:
        zone_id = hosted_zones[0]['Id']
        log.info('HostedZone %s Zone Id is %s', hosted_zone_name, zone_id)

        return zone_id

    else:
        return None


def current_ns_records(rrset_name, zone_id, session):
    r53 = session.client('route53')
    log.debug('Retrieving RRSET %s NS in ZoneId %s', rrset_name, zone_id)
    response = r53.list_resource_record_sets(
        HostedZoneId=zone_id,
        StartRecordName=rrset_name,
        StartRecordType='NS',
        MaxItems='1'
    )

    rrsets = response.get('ResourceRecordSets', [])
    log.debug('RRSETs NS %s response: %s', rrset_name, response)
    if len(rrsets) == 0:
        log.warning('No NS RRSETs for %s in %s', rrset_name, zone_id)
        return []
    rrset = rrsets[0]

    if rrset['Name'].rstrip('.') != rrset_name or rrset['Type'] != 'NS':
        log.warning('No NS RRSETs for %s in %s', rrset_name, zone_id)
        return []
    records = [r['Value'] for r in rrset['ResourceRecords']]
    log.info('NS RRSets for %s: %s', rrset_name, records)

    return records


def apply_changes(zone_id, changes, session):
    r53 = session.client('route53')
    log.debug('Applying changes to ZoneId %s: %s', zone_id, changes)

    response = r53.change_resource_record_sets(
        HostedZoneId=zone_id,
        ChangeBatch={
            'Changes': changes
        }
    )

    return response['ChangeInfo']['Id']


def rrset_changes_required(
    rrset_name,
    current_nameservers,
    final_nameservers
):
    current_nameservers = set(k.rstrip('.') + '.' for k in current_nameservers)
    final_nameservers = set(k.rstrip('.') + '.' for k in final_nameservers)

    if current_nameservers == final_nameservers:
        return None

    records = [{'Value': n} for n in final_nameservers]
    changes = [
        {
            'Action': 'UPSERT',
            'ResourceRecordSet': {
                'Name': rrset_name,
                'Type': 'NS',
                'TTL': 300,
                'ResourceRecords': records
            }
        }
    ]

    return changes


def update_delegate_account_records(
    account_domain,
    account_nameservers,
    session
):

    intg_domain = '.'.join(account_domain.split('.')[1:])
    zone_id = hosted_zone_id(intg_domain, session)
    current_nameservers = current_ns_records(
        account_domain,
        zone_id,
        session
    )

    changes = rrset_changes_required(
        account_domain,
        current_nameservers,
        account_nameservers
    )

    if changes:
        change_id = apply_changes(
            zone_id,
            changes,
            session
        )
        return change_id
    else:
        log.info('Current RRSet does not need to be updated')
        return None


def update_delegate_account_records_handler(event, context):
    log.debug('Event: %s', json.dumps(event))
    account_intgdomain = event['IntgHostedZoneName']
    account_nameservers = event['IntgHostedZoneNameServers']
    if isinstance(account_nameservers, str):
        account_nameservers = account_nameservers.split(',')
        account_nameservers =[k.strip() for k in account_nameservers]
    delegation_account = event['IntgAccountId']
    delegation_region = event['IntgRegion']

    session = assume_session('Onboarding', 'asap-stack-sets',
                             (delegation_account, 'asap-cicd'),
                             region_name=delegation_region)

    return update_delegate_account_records(
        account_intgdomain,
        account_nameservers,
        session
    )




if __name__ == '__main__':
    import unittest.mock as mock

    sdbx_ceng = "522168356138"
    context = mock.MagicMock()

    event = {
        "Account": "262257643404",
        "AWS_STEP_FUNCTIONS_STARTED_BY_EXECUTION_ID": "arn:aws:states:us-east-1:300368054325:execution:OnboardAccount:6a11517f-f766-067f-cd79-3b6ff9f2cf13",
        "Region": "us-east-1",
    }

    # domain_status_handler(event, context)


    event = {
        "LifecycleType": "devl",
        "IntgHostedZoneName": "ceng.devl.intgfanniemae.com",
        "IntgHostedZoneNameServers": "ns-1552.awsdns-02.co.uk,ns-601.awsdns-11.net,ns-225.awsdns-28.com,ns-1281.awsdns-32.org",
        "IntgAccountId": "189693026861",
        "IntgRegion": "us-east-1"
    }
    event = {
        "LifecycleType": "devl",
        "IntgHostedZoneName": "shdz.devl.intgfanniemae.com",
        "IntgHostedZoneNameServers": "ns-397.awsdns-49.com,ns-1269.awsdns-30.org,ns-1619.awsdns-10.co.uk,ns-1016.awsdns-63.net",
        "IntgAccountId": "189693026861",
        "IntgRegion": "us-east-1"
    }
    event = {
            "LifecycleType": "sdbx",
            "IntgHostedZoneName": "ess.sdbx.intgfanniemae.com",
            "IntgHostedZoneNameServers": "ns-1035.awsdns-01.org,ns-1890.awsdns-44.co.uk,ns-991.awsdns-59.net,ns-417.awsdns-52.com",
            "IntgAccountId": "255242629704",
            "IntgRegion": "us-east-1"
    }

    #print(update_delegate_account_records_handler(event, context))

    event = {
            "Account": "522168356138",
            "Region": "us-east-1",
            "Tags": [
                {
                    "Key": "AppCode",
                    "Value": "FKK"
                },
                {
                    "Key": "ApplicationName",
                    "Value": "AWS"
                },
                {
                    "Key": "ApplicationShortName",
                    "Value": "SharedServices"
                },
                {
                    "Key": "AssetID",
                    "Value": "MSR00036"
                },
                {
                    "Key": "CostCenter",
                    "Value": "487"
                },
                {
                    "Key": "ProjectCode",
                    "Value": "B08BASEADM"
                },
            ]
    }

    #tag_public_zone_handler(event, context)
