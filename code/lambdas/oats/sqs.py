#!/usr/bin/env python3

import json
import logging
from cloudformation import get_worker_queue_urls
from iam import assume_session

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


def send_worker_message(queue_url, message, session):
    log.info('Sending %s: %s', queue_url, json.dumps(message))
    sqs = session.client('sqs')

    response = sqs.send_message(
        QueueUrl=queue_url, MessageBody=json.dumps(message, indent=4)
    )
    log.debug('send message response = %s', json.dumps(response, default=str))

    return response['MessageId']


def send_worker_message_handler(event, context):
    account = event['Account']
    region = event.get('Region')
    stack_name = event.get('WorkerStackName', 'asap-aarp')
    queue_key = event['QueueKey']
    message = event['Message']

    session = assume_session(
        'Onboarding', 'asap-stack-sets', (account, 'asap-cicd'), region_name=region
    )

    queue_details = get_worker_queue_urls(stack_name, session)
    log.debug('queue_details = %s', json.dumps(queue_details))
    queue_url = queue_details[queue_key]

    return send_worker_message(queue_url, message, session)


if __name__ == '__main__':
    import unittest.mock as mock

    context = mock.MagicMock()

    event = {'Account': '285488728505', 'Region': 'us-east-1'}

    print(send_worker_message_handler(event, context))
