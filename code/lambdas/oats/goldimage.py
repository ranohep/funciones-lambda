#!/usr/bin/env python3
import os
import json
import logging
import datetime
import boto3

from iam import assume_session

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger()
log.setLevel(logging.DEBUG)
logging.getLogger('botocore').setLevel(logging.INFO)


class AutomationExecutionIdNotFound(Exception):
    pass


class AutomationExecutionFailed(Exception):
    pass


class GoldImageNotFound(Exception):
    pass


def get_stack_resource(cfn, stack_name, logical_name):
    log.debug('get_stack_resource %s %s', stack_name, logical_name)
    params = dict(StackName=stack_name)
    while True:
        response = cfn.list_stack_resources(**params)
        log.debug("Response = %s", json.dumps(response, default=str))
        for resource in response["StackResourceSummaries"]:
            if resource["LogicalResourceId"] == logical_name:
                log.debug(
                    'Found Resource in %s %s: %s',
                    stack_name,
                    logical_name,
                    json.dumps(resource, default=str),
                )
                return resource
        if "NextToken" in response:
            params["NextToken"] = response["NextToken"]
        else:
            return None


def ami_handler(event, context):
    log.debug('ami handler: %s', json.dumps(event))
    image_platform = event["ImagePlatform"]

    sts = boto3.client("sts")

    admin_role = event.get("AdminRole", "asap-stack-sets")
    assume_role = event.get("AssumeRole", "asap-cicd")
    account = event["Account"]
    region = event.get("Region", "us-east-1")

    session = assume_session('Onboard', admin_role, (account, assume_role))

    ec2 = session.client("ec2", region_name=region)
    params = dict(
        Filters=[
            dict(Name="state", Values=["available"]),
            dict(Name="tag:GoldImage", Values=["true"]),
            dict(Name="tag:Platform", Values=[image_platform]),
        ],
        Owners=[account],
    )
    log.debug('Searching EC2 AMI w/ params: %s', json.dumps(params))

    response = ec2.describe_images(**params)
    log.debug('Response from search: %s', json.dumps(response))
    images = response.get("Images", [])
    if len(images) == 0:
        log.info('Gold image for %s not found', image_platform)
        raise GoldImageNotFound

    image_id = images[0]["ImageId"]
    log.debug('Returning %s', image_id)
    return image_id


def status_handler(event, context):
    log.debug('status_handler: %s', json.dumps(event, default=str))
    account = event["Account"]
    region = event["Region"]
    exec_id = event["BuildAttempt"]["AutomationExecutionId"]

    session = assume_session('Onboard',
                             "asap-stack-sets", (account, "asap-cicd"),
                             region_name=region)

    ssm = session.client("ssm")

    response = ssm.get_automation_execution(AutomationExecutionId=exec_id)
    logging.debug('Response for %s: %s', exec_id, json.dumps(response, default=str))
    details = response.get("AutomationExecution")
    if details is None:
        raise AutomationExecutionIdNotFound

    status = details.get("AutomationExecutionStatus")

    if status in ["Pending", "InProgress", "Waiting", "Success"]:
        return status
    else:
        raise AutomationExecutionFailed(status)


def handler(event, context):
    log.debug('handler: %s', json.dumps(event, default=str))
    image_platform = event["ImagePlatform"]

    account = event["Account"]
    region = event["Region"]
    try:
        count = int(event['Build']['Count'])
    except:
        count = 0

    count += 1

    session = assume_session('Onboard',
                             "asap-stack-sets", (account, "asap-cicd"),
                             region_name=region)

    cfn = session.client("cloudformation")
    ssm = session.client("ssm")

    create_doc_stack = get_stack_resource(
        cfn, "asap-yapr-gissm-gi", "CreateSSMDocuments"
    )
    log.debug('create doc stack: %s', json.dumps(create_doc_stack, default=str))
    doc_resource = get_stack_resource(
        cfn, create_doc_stack["PhysicalResourceId"], "BuildGoldImage"
    )
    log.debug('doc resource: %s', json.dumps(doc_resource, default=str))

    ident = str(int(datetime.datetime.utcnow().timestamp()))
    response = ssm.start_automation_execution(
        DocumentName=doc_resource["PhysicalResourceId"],
        Parameters={
            "imagePlatform": [image_platform],
            "keyName": [f"provisioning-goldimage-{image_platform}-{ident}"],
            "instanceName": [f"gi01-{image_platform}-{ident}"],
        },
    )
    log.debug('response = %s', json.dumps(response, default=str))
    return {
        "AutomationExecutionId":  response["AutomationExecutionId"],
        "Count": count,
    }


if __name__ == "__main__":
    event = {
        "Account": "172587047172",
        "ImagePlatform": "Docker running on 64bit Amazon Linux",
        "AutomationExecutionId": "c4fe2894-1b78-417f-bc58-ae7c1b0e9d48",
    }

    # handler(event, {})
    status_handler(event, {})
