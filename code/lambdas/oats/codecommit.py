#!/usr/bin/env python3

from collections import OrderedDict, defaultdict
from pathlib import Path
import math
import logging
import json
import re
import botocore
import boto3

from iam import assume_session
from ssm import (
    get_target_lifecycle,
    get_target_lifecycle_simple,
    get_target_lambda_subnets,
    get_target_environment,
    get_is_target_account_contingency,
)

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
logging.getLogger("botocore").setLevel(logging.INFO)

UPPER_ENVIRONMENTS = {
    "Acceptance",
    "Acceptance-Contingency",
    "CLVE",
    "Production",
    "Contingency"
}


class HeadOutdatedError(Exception):
    """When try to commit, but the parent commit isn't HEAD"""
    pass


class FileDoesNotExistException(Exception):
    """CodeCommit file doesn't exist"""
    pass

class FolderDoesNotExistException(Exception):
    """CodeCommit folder doesn't exist"""
    pass

class CommitNotRequired(Exception):
    pass


def sid_name(lifecycle: str):
    """
    Standardize the lifecycle to use in a SID.

    Remove '-' and camel case
    """
    words = [s.capitalize() for s in lifecycle.split("-")]
    return "".join(words)


def standardize_sids(policy_document):
    for stmt in policy_document["Statement"]:
        if "Sid" in stmt:
            words = re.split(r"[-\s]+", stmt["Sid"])
            if len(words) > 1:
                words = [s.capitalize() for s in words]
                stmt["Sid"] = "".join(words)


def get_codecommit_folder(repo, folder_path, branch, session):
    log.debug("get_codecommit_folder %s, %s, %s", repo, folder_path, branch)
    codecommit = session.client("codecommit")
    try:
        response = codecommit.get_folder(
            repositoryName=repo, commitSpecifier=branch, folderPath=folder_path
        )
    except codecommit.exceptions.FolderDoesNotExistException:
        raise FolderDoesNotExistException

    commit_id = response["commitId"]
    files = response["files"]

    return commit_id, files

def get_codecommit_file(repo, file_path, branch, session):
    log.debug("get_codecommit_file %s, %s, %s", repo, file_path, branch)
    codecommit = session.client("codecommit")
    try:
        response = codecommit.get_file(
            repositoryName=repo, commitSpecifier=branch, filePath=file_path
        )
    except codecommit.exceptions.FileDoesNotExistException:
        raise FileDoesNotExistException

    commit_id = response["commitId"]
    content = response["fileContent"].decode("utf-8")
    timestamp = get_commit_timestamp(repo, commit_id, session)

    return commit_id, timestamp, content


def get_repository_default_branch(repo, session=None):
    if session is None:
        session = boto3.session.Session()
    codecommit = session.client("codecommit")
    response = codecommit.get_repository(repositoryName=repo)
    return response["repositoryMetadata"]["defaultBranch"]


def get_commit_timestamp(repo, commit_id, session=None):
    if session is None:
        session = boto3.session.Session()
    codecommit = session.client("codecommit")

    response = codecommit.get_commit(repositoryName=repo, commitId=commit_id)
    date = response["commit"]["committer"]["date"]
    timestamp, timezone = [int(s) for s in date.split()]
    # diff_hours = abs(timezone) // 100 + (abs(timezone) % 100) / 60
    # UTC Adjustments
    #    if timezone > 0:
    #        diff_hours *= -1
    #    timestamp += diff_hours * 3600

    return timestamp


def commit_files(repo, commit_id, commit_msg, branch, files, session):
    logging.info("Committing %d files to %s on %s", len(files), repo, branch)
    codecommit = session.client("codecommit")

    files = [dict(filePath=t[0], fileContent=t[1].encode()) for t in files]
    start = 0
    batch_size = len(files)

    while start < len(files):
        try:
            end = min(len(files), start + batch_size)
            logging.info("Attempting to put batch [%d:%d]", start, end)
            response = codecommit.create_commit(
                repositoryName=repo,
                branchName=branch,
                parentCommitId=commit_id,
                authorName=__name__,
                commitMessage=commit_msg,
                putFiles=files[start:end]
            )

            commit_id = response["commitId"]
            start = end

        except codecommit.exceptions.ParentCommitIdOutdatedException:
            logging.error("Head Outdated Error")
            raise HeadOutdatedError
        except codecommit.exceptions.MaximumFileEntriesExceededException:
            batch_size = math.ceil(batch_size / 2)
            logging.info("Resizing batch to %d", batch_size)
        except codecommit.exceptions.NoChangeException:
            start = end
            logging.info("No Change with commit. Continuing")

    timestamp = get_commit_timestamp(repo, commit_id, session)
    return commit_id, timestamp

def sync_xtns(repo, branch, files, session):
    xtns = []
    commit_id = None

    folders = defaultdict(set)
    for t in files:
        path = Path(t[0])
        folders[path.parent].add(t[0])
        xtns.append(dict(filePath=t[0], fileContent=t[1].encode()))

    for f in folders:
        try:
            commit_id, folder_files = get_codecommit_folder(repo, str(f),
                    branch, session)
            files_in_folder = {t["absolutePath"] for t in folder_files}
            for t in files_in_folder - folders[f]:
                xtns.append(dict(filePath=t))
        except FolderDoesNotExistException:
            commit_id = get_branch_commit(repo, branch, session)

    return commit_id, xtns

def sync_folders(repo, commit_msg, branch, files, session):
    logging.info("Committing %d files to %s on %s", len(files), repo, branch)
    codecommit = session.client("codecommit")

    commit_id, xtns = sync_xtns(repo, branch, files, session)

    history = []
    start = 0
    batch_size = len(xtns)

    while start < len(xtns):
        try:
            end = min(len(xtns), start + batch_size)
            logging.info("Attempting to put batch [%d:%d]", start, end)
            put_files = []
            delete_files = []
            for idx in range(start, end):
                if "fileContent" in xtns[idx]:
                    put_files.append(xtns[idx])
                else:
                    delete_files.append(xtns[idx])

            response = codecommit.create_commit(
                repositoryName=repo,
                branchName=branch,
                parentCommitId=commit_id,
                authorName=__name__,
                commitMessage=commit_msg,
                putFiles=put_files,
                deleteFiles=delete_files
            )

            commit_id = response["commitId"]
            history.append(commit_id)
            start = end

        except codecommit.exceptions.ParentCommitIdOutdatedException:
            logging.error("Head Outdated Error")
            commit_id = get_branch_commit(repo, branch, session)
        except codecommit.exceptions.MaximumFileEntriesExceededException:
            if batch_size == 1:
                raise
            batch_size = math.ceil(batch_size / 2)
            logging.info("Resizing batch to %d", batch_size)
        except codecommit.exceptions.NoChangeException:
            start = end
            logging.info("No Change with commit. Continuing")

    timestamp = get_commit_timestamp(repo, commit_id, session)
    return commit_id, timestamp, history

def merge_branches(repo, source, target, session):
    codecommit = session.client("codecommit")

    response = codecommit.merge_branches_by_fast_forward(
        repositoryName=repo,
        sourceCommitSpecifier=source,
        destinationCommitSpecifier=target,
    )
    log.debug(response)

    commit_id = response["commitId"]
    timestamp = get_commit_timestamp(repo, commit_id, session)

    return commit_id, timestamp


def get_branch_commit(repo, branch, session):
    codecommit = session.client("codecommit")
    response = codecommit.get_branch(repositoryName=repo, branchName=branch,)

    return response["branch"]["commitId"]




def update_promote_config(repo, account_file, environment, lifecycle, session):
    log.debug("update_promote_config %s, %s, %s", repo, account_file, environment)
    branch = "master"
    commit_id, timestamp, content = \
        get_codecommit_file(repo, account_file, branch, session)

    accounts_env = json.loads(content, object_pairs_hook=OrderedDict)

    if lifecycle in accounts_env["Lifecycles"]:
        log.info("%s does not need to update promote.", lifecycle)
        return {
            "Repo": repo, "CommitId": commit_id, "Timestamp": timestamp
        }

    log.debug("SamTemplate started pre-modifications: %s", content)
    accounts_env["Lifecycles"].append(lifecycle)

    if environment == "CLVE":
        log.debug("Updating the CLVE config")
        accounts_env["ClveLifecycles"].append(lifecycle)
    elif environment in ["Production", "Contingency"]:
        log.debug("Updating the Higher-Environment config")
        accounts_env["HeLifecycles"].append(lifecycle)
    else:
        log.debug("Updating the Lower-Environment config")
        accounts_env["LeLifecycles"].append(lifecycle)

    new_content = json.dumps(accounts_env, indent=2)
    log.debug("SamTemplate new contents: %s", content)

    commit_msg = f"Onboarding {lifecycle}"
    log.debug("Committing file %s on %s", account_file, commit_id)
    commit_id, timestamp = commit_files(
        repo=repo,
        commit_id=commit_id,
        commit_msg=commit_msg,
        files=[(account_file, new_content)],
        branch=branch,
        session=session,
    )
    return {"Repo": repo, "CommitId": commit_id, "Timestamp": timestamp}



def update_promote_config_handler(event, context):
    log.debug("update_promote_config_handler called with: %s",
              json.dumps(event))

    account = event["Account"]
    region = event["Region"]
    repo = event["Repository"]
    repo_account = event["RepositoryAccount"]
    repo_region = event["RepositoryRegion"]
    account_file = event.get("AccountFile",
                             "code/lambdas/asap_promote_releases/environ.json")

    environment = get_target_environment(account, region)
    lifecycle = get_target_lifecycle(account, region)

    session = assume_session(
        "AutomateOnboard",
        "asap-stack-sets",
        (repo_account, "asap-cicd"),
        region_name=repo_region,
    )

    return update_promote_config(repo, account_file, environment, lifecycle,
                                 session)


def add_asap_infra_config(repo, template_file, account, region, session):
    log.debug("add_asap_infra_config")
    config_path = template_file[0:template_file.rindex("/")]
    file_path = "{config_path}/{account}-{region}.json".format(**locals())

    try:
        log.debug("Determining with config already set for %s-%s",
                  account, region)
        commit_id, timestamp, _ = get_codecommit_file(
            repo=repo, file_path=file_path, branch="dev", session=session
        )
        log.info("Config already created for %s-%s", account, region)
        return {"Repo": repo, "CommitId": commit_id, "Timestamp": timestamp}
    except FileDoesNotExistException:
        log.info("Creating config for %s-%s", account, region)
        branch = "master"
        lifecycle = get_target_lifecycle(account, region)
        environment = get_target_environment(account, region)
        commit_msg = f"Added {lifecycle} {environment}"

        commit_id = get_branch_commit(repo, "dev", session)
        log.info("Adding new config to parent commit %s", commit_id)
        log.debug("Copy config from %s", template_file)

        _, timestamp, template_body = get_codecommit_file(
            repo=repo, file_path=template_file, branch="dev", session=session
        )

        if environment in UPPER_ENVIRONMENTS:
            devops_role_type = "v3-app-devops-upper-core"
        else:
            devops_role_type = "v3-app-devops-core"

        template = json.loads(template_body, object_pairs_hook=OrderedDict)
        template["account"]["/fnma/account/type"] = "standard"
        template["asap-ais"]["/asap/ais/core/roleTypes"] = ",".join(
            [
                devops_role_type,
                "v3-app-comp-core",
                "v3-app-release-mgmt-core",
                "v3-app-integration-core",
                "v3-app-machine-core",
            ]
        )
        template["asap-ais"]["/asap/ais/cicd/roleTypes"] = ",".join(
            [
                "v3-app-deploy-cicd",
                "v3-app-execute-cicd",
                devops_role_type,
                "v3-app-comp-core",
                "v3-app-release-mgmt-core",
                "v3-app-integration-core",
                "v3-app-machine-core",
            ]
        )
        template["asap-ais"]["/asap/ais/cloudadm/roleTypes"] = ",".join(
            []
        )
        template["asap-ais"]["/asap/ais/sourceKey"] = "v3source.zip"
        template["asap-ais"]["/asap/ais/buildspec"] = "build/v3target_buildspec.yml"

        lambda_subnets = get_target_lambda_subnets(account, region)
        template["asap"]["/asap/subnets/lambda"] = lambda_subnets
        template["asap"]["/asap/pam/masterbucket"] = "deprecated"
        template["asap"][
            "/asap/pam/masterkeyarn"
        ] = "arn:aws:kms:us-east-1:{}:key/deprecated".format(account)

        template["cloudadm"]["/cloudadm/common/AppCode"] = "FKK"
        template["cloudadm"]["/cloudadm/common/AssetID"] = "MSR00036"
        template["cloudadm"]["/cloudadm/common/CostCenter"] = "488"
        template["cloudadm"]["/cloudadm/common/ApplicationName"] = "AWS"
        template["cloudadm"]["/cloudadm/common/ProjectCode"] = "B08BASEADM"


        log.debug("Commiting file to %s", file_path)
        commit_id, timestamp = commit_files(
            repo=repo,
            commit_id=commit_id,
            commit_msg=commit_msg,
            files=[(file_path, json.dumps(template, indent=4))],
            branch="dev",
            session=session,
        )

        commit_id, timestamp = merge_branches(repo, "dev", "master", session)
        log.info("Copied %s to %s", template_file, file_path)
        return {"Repo": repo, "CommitId": commit_id, "Timestamp": timestamp}


def add_asap_infra_config_handler(event, context):
    account = event["Account"]
    region = event.get("Region", "us-east-1")
    repo = event.get("Repository", "asap-account-management")
    repo_account = event["RepositoryAccount"]
    repo_region = event["RepositoryRegion"]
    template = event["TemplateFile"]

    session = assume_session(
        "AutomateOnboard",
        "asap-stack-sets",
        (repo_account, "asap-cicd"),
        region_name=repo_region,
    )

    return add_asap_infra_config(repo, template, account, region, session)


def add_yapr_config(repo, branch, template_file, account, region, session):
    config_path = template_file[0:template_file.rindex("/")]
    file_path = "{config_path}/{account}-{region}.json".format(**locals())
    try:
        log.debug("Determining with config already set for %s-%s",
                  account, region)
        commit_id, timestamp, _ = get_codecommit_file(
            repo=repo, file_path=file_path, branch=branch, session=session
        )
        log.info("Config already created for %s-%s", account, region)
        return {"Repo": repo, "CommitId": commit_id, "Timestamp": timestamp}
    except FileDoesNotExistException:
        log.info("Creating config for %s-%s", account, region)
        branch = "master"
        lifecycle = get_target_lifecycle(account, region)
        commit_msg = f"Added {lifecycle}"

        commit_id = get_branch_commit(repo, branch, session)
        log.info("Adding new config to parent commit %s", commit_id)
        log.debug("Copy config from %s", template_file)

        _, _, template = get_codecommit_file(
            repo=repo, file_path=template_file, branch=branch, session=session
        )

        log.debug("Commiting file to %s", file_path)
        commit_id, timestamp = commit_files(
            repo=repo,
            commit_id=commit_id,
            commit_msg=commit_msg,
            files=[(file_path, template)],
            branch=branch,
            session=session,
        )
        log.info("Copied %s to %s", template_file, file_path)
        return {"Repo": repo, "CommitId": commit_id, "Timestamp": timestamp}


def add_yapr_config_handler(event, context):
    log.debug("add yapr config handler: %s", json.dumps(event))
    account = event["Account"]
    region = event["Region"]
    repo = event["Repository"]
    repo_account = event["RepositoryAccount"]
    repo_region = event["RepositoryRegion"]
    template_file = event["TemplateFile"]

    branch = "master"

    session = assume_session(
        "Onboarding",
        "asap-stack-sets", (repo_account, "asap-cicd"),
        region_name=repo_region,
    )

    return add_yapr_config(repo, branch, template_file, account, region,
                           session)


def update_fnma_services_config(
    lifecycle, region, repo, build_sh, configs, stack_config, session
):
    files = []
    branch = "master"
    log.debug("getting %s from %s on %s", build_sh, repo, branch)
    commit_id, timestamp, build_file = get_codecommit_file(
        repo, build_sh, branch, session=session
    )

    p = re.compile(r"^\s*arrEnvironments=\(([^\)]*)", re.DOTALL | re.MULTILINE)
    match = p.search(build_file)
    if match and f"'{lifecycle}@{region}'" not in match.group(1).split():
        files.append(
            (
                build_sh,
                "".join(
                    [
                        build_file[: match.start(0)],
                        match.group(0),
                        f" '{lifecycle}@{region}'",
                        build_file[match.end(0):],
                    ]
                ),
            )
        )

        log.debug(files)

    for config in configs:
        config_file = f"{config}/{lifecycle}.json"
        try:
            _, ts, config_file = get_codecommit_file(
                repo, config_file, commit_id, session=session
            )
            timestamp = max(ts, timestamp)
        except FileDoesNotExistException:
            files.append((config_file, json.dumps(stack_config, indent=4),))

    log.debug("Commiting %s", json.dumps(files))
    commit_msg = f"Onboarding {lifecycle}"
    if files:
        commit_id, timestamp = commit_files(
            repo, commit_id, commit_msg, branch, files, session=session
        )

    status = {"Repo": repo, "CommitId": commit_id, "Timestamp": timestamp}
    return status


def update_fnma_services_config_handler(event, context):
    log.debug(
        "update_fnma_services_utilities_handler called with: %s",
        json.dumps(event)
    )

    account = event["Account"]
    region = event["Region"]
    repo = event["Repository"]
    repo_account = event["RepositoryAccount"]
    repo_region = event["RepositoryRegion"]

    build_sh = event["BuildSh"]
    configs = event["Configs"]
    if isinstance(configs, str):
        configs = [configs]
    stack_config = event["StackConfig"]

    lifecycle = get_target_lifecycle(account, region)

    is_contg = get_is_target_account_contingency(account, region)
    parameters = stack_config['Parameters']
    for k, v in parameters.items():
        if is_contg:
            parameters[k] = v.replace('{CONT}', '-CONT')
        else:
            parameters[k] = v.replace('{CONT}', '')

    session = assume_session(
        "AutomateOnboard",
        "asap-stack-sets",
        (repo_account, "asap-cicd"),
        region_name=repo_region,
    )

    return update_fnma_services_config(
        lifecycle=lifecycle,
        region=region,
        repo=repo,
        build_sh=build_sh,
        configs=configs,
        stack_config=stack_config,
        session=session
    )


def update_sudo_deploy(account, region, repo, deploy_sh, session):
    log.debug("getting %s from %s", deploy_sh, repo)

    lifecycle = get_target_lifecycle(account, region)
    branch = get_repository_default_branch(repo, session=session)
    commit_id, timestamp, deploy_file = get_codecommit_file(
        repo, deploy_sh, branch, session=session
    )

    log.debug("contents of %s pre-modification: %s", deploy_sh, deploy_file)
    account_re = re.compile(
        r"""
            ^\s*accounts=\(\s*
            ^(?P<accounts>.*?)
            (?=\))
    """,
        re.VERBOSE | re.DOTALL | re.MULTILINE,
    )

    m = account_re.search(deploy_file)
    if m:
        new_account = f"{lifecycle}@{account}@{region}"
        if new_account in m.group("accounts"):
            log.info("%s already in %s. Skipping", lifecycle, deploy_file)
            return {
                "Repo": repo, "CommitId": commit_id, "Timestamp": timestamp
            }

        indent_level = re.search(r"^\s*", m.group("accounts"))
        end_whitespace = re.search(r"\s*$", m.group("accounts"))
        new_deploy = (
            deploy_file[m.start():m.end()]
            + indent_level.group()
            + f"'{new_account}'"
            + end_whitespace.group()
            + deploy_file[m.end():]
        )
        log.debug("contents of %s post-modification: %s",
                  deploy_sh, new_deploy)
        commit_msg = f"Onboarding {lifecycle}"
        commit_id, timestamp = commit_files(
            repo,
            commit_id,
            commit_msg,
            branch,
            [(deploy_sh, new_deploy)],
            session=session,
        )
        return {"Repo": repo, "CommitId": commit_id, "Timestamp": timestamp}

    else:
        log.warning("Contents of file in unexpected state: %s", deploy_file)
        print(deploy_file)
        raise ValueError


def update_sudo_deploy_handler(event, context):
    log.debug("update_sudo_deploy_handler called with: %s", json.dumps(event))

    account = event["Account"]
    region = event["Region"]
    repo = event["Repository"]
    repo_account = event["RepositoryAccount"]
    repo_region = event["RepositoryRegion"]
    deploy_sh = event["DeployScript"]

    session = assume_session(
        "AutomateOnboard",
        "asap-stack-sets",
        (repo_account, "asap-cicd"),
        region_name=repo_region,
    )

    return update_sudo_deploy(account, region, repo, deploy_sh, session)


def all_keys_added(template_json, repo, file_path, comm_key, decomm_key):
    if comm_key in template_json["Resources"]:
        log.info("%s already found in %s:%s", comm_key, repo, file_path)
        if decomm_key in template_json["Resources"]:
            log.info("%s already found in %s:%s", decomm_key, repo, file_path)
            return True
        else:
            log.error("Unexpectedly found %s but not %s. ",
                    comm_key, decomm_key)
            raise UnexpectedResources
    elif decomm_key in template_json["Resources"]:
        log.error("Unexpectedly found %s but not %s. ",
                decomm_key,comm_key)
        raise UnexpectedResources

    return False

def update_poller_config(
    account, region, repo, session, template_dir, template,
    catalog_id, parameter_prefix, com_url, decom_url
):

    lifecycle = get_target_lifecycle(account, region)
    lifecycle_simple = get_target_lifecycle_simple(account, region)
    comm_key = f"{lifecycle_simple}ProvQueueUrl20"
    decomm_key = f"{lifecycle_simple}DeProvQueueUrl20"
    name_prefix = f"{parameter_prefix}/{catalog_id}"

    branch = "master"
    commit_id, files = get_codecommit_folder(
        repo, template_dir, branch, session=session
    )
    current_file = [f for f in files if f["relativePath"] == template][0]
    alternate_files = [f for f in files if f["relativePath"] != template]

    # Check Alternative Files
    for file in alternate_files:
        file_path = file["absolutePath"]
        _, timestamp, content = get_codecommit_file(repo, file_path, commit_id,
                session)
        log.debug("contents of %s pre-modification: %s", file_path, content)
        template_json = json.loads(content, object_pairs_hook=OrderedDict)

        if all_keys_added(template_json, repo, file_path, comm_key, decomm_key):
            return {
                "Repo": repo,
                "CommitId": commit_id,
                "Timestamp": timestamp
            }

    # Check/Add to current file
    file_path = current_file["absolutePath"]
    _, timestamp, content = get_codecommit_file(repo, file_path, commit_id,
            session)
    log.debug("contents of %s pre-modification: %s", file_path, content)
    template_json = json.loads(content, object_pairs_hook=OrderedDict)
    if all_keys_added(template_json, repo, file_path, comm_key, decomm_key):
        return {
            "Repo": repo,
            "CommitId": commit_id,
            "Timestamp": timestamp
        }

    log.info("Adding %s to %s:%s", comm_key, repo, file_path)
    template_json["Resources"][comm_key] = {
        "Type": "AWS::SSM::Parameter",
        "Properties": {
            "Type": "String",
            "Value": com_url,
            "Name": f"{name_prefix}/provisioningQueueUrl/{lifecycle}",
        },
    }

    log.info("Adding %s to %s:%s", decomm_key, repo, file_path)
    template_json["Resources"][decomm_key] = {
        "Type": "AWS::SSM::Parameter",
        "Properties": {
            "Type": "String",
            "Value": decom_url,
            "Name": f"{name_prefix}/deprovisioningQueueUrl/{lifecycle}",
        },
    }

    update = json.dumps(template_json, indent=4)
    log.debug("contents of %s post-modification: %s", file_path, update)
    log.info("Committing %s:%s", repo, file_path)
    commit_id, timestamp = commit_files(
        repo=repo,
        commit_id=commit_id,
        commit_msg=f"Onboarding {lifecycle}",
        branch=branch,
        files=[(file_path, update)],
        session=session,
    )

    return {"Repo": repo, "CommitId": commit_id, "Timestamp": timestamp}


def update_poller_config_handler(event, context):
    log.debug("update_poller_config_handler with event: %s", json.dumps(event))

    account = event["Account"]
    region = event["Region"]
    repo = event["Repository"]
    repo_account = event["RepositoryAccount"]
    repo_region = event["RepositoryRegion"]
    template_dir = event["TemplateDirectory"]
    template = event["TemplateFile"]

    catalog_id = event["CatalogId"]
    parameter_prefix = event["ParameterPrefix"]

    commissioning_url = event["CommissioningQueueURL"]
    decommissioning_url = event["DecommissioningQueueURL"]

    session = assume_session(
        "AutomateOnboard",
        "asap-stack-sets",
        (repo_account, "asap-cicd"),
        region_name=repo_region,
    )

    return update_poller_config(
        account=account,
        region=region,
        repo=repo,
        session=session,
        template_dir=template_dir,
        template=template,
        catalog_id=catalog_id,
        parameter_prefix=parameter_prefix,
        com_url=commissioning_url,
        decom_url=decommissioning_url,
    )


if __name__ == "__main__":
    # pylint: disable=invalid-name,unused-argument,line-too-long
    import unittest.mock as mock
    context = mock.MagicMock()

    ceng_account = "172587047172"
    ceng_region = "us-east-1"
    repo = "s5ugys-oats-test"

    sdbx_account = "522168356138"
    sdbx_region = "us-east-1"

    event = {
        "Account": sdbx_account,
        "Region": sdbx_region,
        "Repository": repo,
        "RepositoryAccount": ceng_account,
        "RepositoryRegion": ceng_region,
        "PolicyFile": "deploy/account-bucket-access-policy.template",
    }

    event = {
        "Repository": "eng-asap-account-management",
        "RepositoryAccount": "172587047172",
        "RepositoryRegion": "us-east-1",
        "PolicyFile": "deploy/account-bucket-access-policy.template",
        "Account": "739363106608",
        "AWS_STEP_FUNCTIONS_STARTED_BY_EXECUTION_ID": "arn:aws:states:us-east-1:172587047172:execution:OnboardAccount-dev:b4e98574-5c6f-f7ce-0068-96df657ccae3",
        "Region": "us-east-2"
    }

    # print(add_bucket_policy_handler(event, context))

    event = {
        "Account": sdbx_account,
        "Region": sdbx_region,
        "Repository": repo,
        "RepositoryAccount": ceng_account,
        "RepositoryRegion": ceng_region,
        "TemplateFile": "code/lambdas/sam_template.yaml"
    }

    event = {
        "Account": ceng_account, # "739363106608",
        "Region": "us-east-1",
        "Repository": "eng-asap-cicd-mgmt",
        "RepositoryAccount": ceng_account,
        "RepositoryRegion": ceng_region,
        "TemplateFile": "code/lambdas/sam_template.yaml"
    }

    # print(update_promote_config_handler(event, context))

    event = {
        "Account": sdbx_account,
        "Region": sdbx_region,
        "Repository": repo,
        "RepositoryAccount": ceng_account,
        "RepositoryRegion": ceng_region,
        "TemplateFile": "eng-asap-infra/configs/template.json",
    }

    # print(add_asap_infra_config_handler(event, context))

    event = {
        "Account": sdbx_account,
        "Region": sdbx_region,
        "Repository": repo,
        "RepositoryAccount": ceng_account,
        "RepositoryRegion": ceng_region,
        "TemplateFile": "eng-asap-yapr-infra-configs/configs/template.json",
    }

    # print(add_yapr_config_handler(event, context))

    event = {
        "Account": sdbx_account,
        "Region": sdbx_region,
        "Repository": repo,
        "RepositoryAccount": ceng_account,
        "RepositoryRegion": ceng_region,
        "BuildSh": "fnma-service-utilities-repo/build.sh",
        "Configs": "fnma-service-utilities-repo/configs",
        "StackConfig": {
            "Parameters": {
            },
            "StackPolicy": {}
        },
    }

    # print(update_fnma_services_config_handler(event, context))

    # "Account": sdbx_account,
    # "Region": sdbx_region,
    prod_csbu = "519136688323"
    prod_mfbu = "753073002341"
    event = {
        #"Account": "068270649629",
        "Account": prod_mfbu,
        "Region": "us-east-1",
        "Repository": repo,
        "RepositoryAccount": ceng_account,
        "RepositoryRegion": ceng_region,
        "BuildSh": "fnma-monitoring-repo/build.sh",
        # "Configs": "fnma-monitoring-repo/stack/configs",
        "Configs": "fnma-monitoring-repo/configs",
        "StackConfig": {
            "Parameters": {
                "EMMRoleName": "MONITORING-LAMBDA{CONT}-EMM-ALERT"
            },
            "StackPolicy": {}
        },
    }

    #print(update_fnma_services_config_handler(event, context))

    event = {
        "Account": sdbx_account,
        "Region": sdbx_region,
        "Repository": repo,
        "RepositoryAccount": ceng_account,
        "RepositoryRegion": ceng_region,
        "DeployScript": "fnma-sudo-repo/scripts/deploy.sh",
    }

    # print(update_sudo_deploy_handler(event, context))

    event = {
        #"Account": sdbx_account,
        #"Region": sdbx_region,
        "Account": "472611518043",
        "Region": "us-east-1",
        "Repository": repo,
        "RepositoryAccount": ceng_account,
        "RepositoryRegion": ceng_region,
        "TemplateDirectory": "fnma-provisioning-services-repo/configs/services/v2/devl/poller20",
        "TemplateFile": "parameters.template",
        "CatalogId": "ecad248ddbaf00d00bdeaca2ca9619ae",
        "ParameterPrefix": "/provisioning/services/poller",
        "CommissioningQueueURL": "https://sqs.us-east-1.amazonaws.com/285488728505/asap-aarp-CommissioningQueue-NBTD7Q6IT38I",
        "DecommissioningQueueURL": "https://sqs.us-east-1.amazonaws.com/285488728505/asap-aarp-DecommissioningQueue-19RMVQWY2LCRP",
    }

    #print(update_poller_config_handler(event, context))

    event = {
        "Account": "648156244979",
        "Region": "us-east-1",
        "Repository": "s5ugys-test",
        "RepositoryAccount": "172587047172",
        "RepositoryRegion": "us-east-1",
        "RepositoryBranch": "master",
        "RepositoryPath": "accounts",
        "StackDetails": {
            "StackId": "arn:aws:cloudformation:us-east-1:648156244979:stack/asap-account-deployment/bbde15d0-f697-11ea-ba2c-1266e578c117",
            "StackStatus": "UPDATE_COMPLETE",
            "Timestamp": "2020-11-23 18:40:26.043000+00:00"
        },
        "AccountDetails": {
            "/provisioning/core/vpc/DefaultVPCType": "default",
            "/provisioning/core/vpc/default/TGWNetworkZone": "devl",
            "/provisioning/core/vpc/default/VpcId": "vpc-0376493ec2cdabc6e",
            "/provisioning/core/vpc/default/VpcName": "vpc-de1052",
            "/provisioning/core/vpc/default/az/AZCount": "4",
            "/global/AccountId": "648156244979",
            "/global/AccountName": "AWS2 Development-CCOE Mock Testing",
            "/global/AccountPurpose": "InfoSec Engineering",
            "/global/AccountShortName": "devl-ccoe",
            "/global/Environment": "Development",
            "/global/Lifecycle": "devl-ccoe",
            "/global/LifecycleSimple": "devlccoe",
            "/global/LifecycleType": "devl",
            "/global/LifecycleUpper": "DEVL-CCOE"
        }
    }
    #print(aggregate_account_info_handler(event, context))

