#!/usr/bin/env python3

import sys
import time
import logging
import json
import boto3


class StackInstanceStateError(Exception):
    pass


logging.basicConfig()
logging.getLogger("botocore").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.getLogger().setLevel(logging.DEBUG)
sts = boto3.client("sts")
cfn = boto3.client("cloudformation")


def instance_state(name, account, region):
    try:
        response = cfn.describe_stack_instance(
            StackSetName=name, StackInstanceAccount=account, StackInstanceRegion=region
        )

        return response.get("StackInstance", {}).get("Status")

    except cfn.exceptions.StackInstanceNotFoundException:
        return None


def add_stackset_to_instance(account, region, *names):

    attempt = 8
    while attempt > 0:
        errors = dict()
        states = [(n, instance_state(n, account, region)) for n in names]
        names = [s[0] for s in states if s[1] != "CURRENT"]

        if not names:
            break

        for name in names:
            try:
                logging.debug(
                    "Attempt Countdown #%d: add %s to %s:%s",
                    attempt,
                    name,
                    account,
                    region,
                )
                current_state = instance_state(name, account, region)
                if current_state == "CURRENT":
                    break
                elif current_state is None:
                    response = cfn.create_stack_instances(
                        StackSetName=name,
                        Accounts=[account],
                        Regions=[region],
                        OperationPreferences=dict(
                            FailureToleranceCount=1, MaxConcurrentCount=1
                        ),
                    )
            except Exception as err:
                errors[(account, region, name)] = err

        logging.info("Sleeping...")
        attempt -= 1
        time.sleep(15)

    else:
        raise StackInstanceStateError(errors)


def pipeline_state(account, region, name):
    local_account = sts.get_caller_identity()["Account"]
    arn = f"arn:aws:iam::{local_account}:role/asap-stack-sets"
    response = sts.assume_role(RoleArn=arn, RoleSessionName="stackset-admin")

    cred = response["Credentials"]
    params = dict(
        aws_access_key_id=cred["AccessKeyId"],
        aws_secret_access_key=cred["SecretAccessKey"],
        aws_session_token=cred["SessionToken"],
    )
    admin_sts = boto3.client("sts", **params)

    arn = f"arn:aws:iam::{account}:role/asap-cicd"
    response = admin_sts.assume_role(RoleArn=arn, RoleSessionName="stackset-admin")

    cred = response["Credentials"]
    params = dict(
        aws_access_key_id=cred["AccessKeyId"],
        aws_secret_access_key=cred["SecretAccessKey"],
        aws_session_token=cred["SessionToken"],
    )

    pipeline = boto3.client("codepipeline", region_name=region, **params)
    response = pipeline.get_pipeline_state(name=name)

    from pprint import pprint

    first, last = response["stageStates"][0], response["stageStates"][-1]

    triggered = dict(
        status_change=first["actionStates"][0]["latestExecution"]["lastStatusChange"],
        execution_id=first["latestExecution"]["pipelineExecutionId"],
        status=first["latestExecution"]["status"],
    )

    end_pipeline = dict(
        status_change=last["actionStates"][0]["latestExecution"]["lastStatusChange"],
        execution_id=last["latestExecution"]["pipelineExecutionId"],
        status=last["latestExecution"]["status"],
    )

    """
    {'actionStates': [{'actionName': 'Source',
                   'currentRevision': {'revisionId': 'wkkjYV9wrfw8ufG6yv_bh21UIiXQTfxm'},
                   'entityUrl': 'https://console.aws.amazon.com/s3/home?#',
                   'latestExecution': {'externalExecutionId': 'wkkjYV9wrfw8ufG6yv_bh21UIiXQTfxm',
                                       'lastStatusChange': datetime.datetime(2020, 2, 24, 15, 14, 44, 591000, tzinfo=tzlocal()),
                                       'status': 'Succeeded',
                                       'summary': 'Amazon S3 version id: '
                                                  'wkkjYV9wrfw8ufG6yv_bh21UIiXQTfxm'}}],
 'inboundTransitionState': {'enabled': True},
 'latestExecution': {'pipelineExecutionId': '94c912dd-bf33-446a-96e0-a5dc2acd4b8b',
                     'status': 'Succeeded'},
    """

    pprint(triggered)
    pprint(end_pipeline)


def add_stacksets(account, region):
    logging.info("Deploying asap-cicd StackSet to %s:%s", account, region)
    add_stackset_to_instance(account, region, "asap-cicd")
    pipeline_state(account, region, "asap-infra")
    _ = input("\nVerify asap-infra pipeline was successful in target account. ")

    logging.info("Deploying asap-dns StackSet to %s:%s", account, region)
    add_stackset_to_instance(account, region, "asap-dns-v3-account-cicd")
    msg = "\nVerify asap-dns pipeline was successful in target account(10min). "
    _ = input(msg)

    stacksets = ["asap-yapr-cicd", "asap-aarp-cicd", "asap-scam-cicd"]
    logging.info("Deployinng %s StackSet to %s: %s", stacksets, account, region)
    add_stackset_to_instance(account, region, *stacksets)

    pipelines = ["asap-yapr-infra", "asap-aarp"]


if __name__ == "__main__":
    if len(sys.argv) == 3:
        _, account, region = sys.argv

        add_stacksets(account, region)
    else:
        raise SystemExit("usage: %s <account#> <region>" % sys.argv[0])

    print(sys.argv)
    print(len(sys.argv))
