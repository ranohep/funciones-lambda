#!/usr/bin/env python3

import logging
import json
from collections import defaultdict

from iam import assume_session
from codecommit import commit_files, sync_folders, get_branch_commit
from stackset import stack_instances
import boto3
from botocore.exceptions import ClientError

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


class TargetAccountAssumeSessionError(Exception):
    pass

def get_account_parameters(parameters, session):
    log.debug("Retrieving parameters: %s", parameters)
    ssm = session.client("ssm")
    ssm_params = {}
    for path in parameters:
        if path.endswith("/"):
            params = dict(Path=path, Recursive=True)
            while True:
                response = ssm.get_parameters_by_path(**params)
                ssm_params.update(
                    {i["Name"]: i["Value"] for i in response["Parameters"]})

                if "NextToken" in response:
                    params["NextToken"] = response["NextToken"]
                else:
                    break
        else:
            try:
                response = ssm.get_parameter(Name=path)
                ssm_params[path] = response["Parameter"]["Value"]
            except:
                pass

    return ssm_params

def get_account_stack_info(session):
    """
    CCoE's initial stack is asap-account-deployment, find info on stack
    and other stack that is inmportant
    """
    stack_info = {}

    cfn = session.client("cloudformation")

    try:
        response = cfn.describe_stacks(StackName="asap-account-deployment")
        if len(response["Stacks"]) == 1:
            stack = response["Stacks"][0]
            stack_info["asap-account-deployment"] = {
                "Create": str(stack["CreationTime"]),
                "Status": stack["StackStatus"],
                "Reason": stack.get("StackStatusReason")
            }
    except ClientError as exc:
        if exc.response["Error"]["Code"] == "ValidationError":
            log.exception("Validation Error: %s", exc)
        else:
            raise


    return stack_info

def get_account_service_roles(session):
    """
    Get AWS Service Roles
    """
    roles = {}

    iam = session.client("iam")
    params = {"PathPrefix": "/aws-service-role/"}
    while True:
        response = iam.list_roles(**params)
        for role in response["Roles"]:
            create = role.get("CreateDate")
            roles[role["RoleName"]] = {
                "Name": role["RoleName"],
                "Arn": role["Arn"],
                "Create": str(create) if create else None
            }

        if response.get("IsTruncated"):
            params["Marker"] = response["Marker"]
        else:
            break

    return roles

def get_account_details_handler(event, context):
    log.debug('%s event: %s', context.function_name,
            json.dumps(event, default=str))
    account = event['Account']
    region = event['Region']
    account_generation = event.get("AccountGeneration")
    bucket_name = event["BucketName"]
    execution_id = event.get("ExecutionId")
    prefix = event.get("Prefix")
    parameter_list = event["SsmParameters"]

    try:

        session = assume_session(
            'Onboarding', 'asap-stack-sets', (account, 'asap-cicd'),
            region_name = region
        )
    except:
        raise TargetAccountAssumeSessionError

    account_details = {
        "Account": account,
        "Region": region,
        "SsmParameters": get_account_parameters(parameter_list, session),
        "StackInfo": get_account_stack_info(session),
        "ServiceRoles": get_account_service_roles(session),
    }
    if account_generation:
        account_details["AccountGeneration"] = account_generation

    key = f"{account}-{region}.json"
    if execution_id:
        key = f'{execution_id}/{key}'
    if prefix:
        key = f'{prefix}/{key}'
    body = json.dumps(account_details, sort_keys=True)

    s3 = boto3.client("s3")
    response = s3.put_object(
            Bucket=bucket_name,
            Key=key,
            Body=body.encode())
    return {
            "Bucket": bucket_name,
            "Key": key,
            "VersionId": response["VersionId"]}

def gather_s3_objects(s3_objects):
    log.debug("gather s3 objects: %s", s3_objects)
    if isinstance(s3_objects, dict):
        s3_objects = [s3_objects]

    s3 = boto3.client("s3")
    account_details = []
    deletes = defaultdict(list)

    for s3_object in s3_objects:
        bucket = s3_object.get("Bucket")
        key = s3_object.get("Key")
        if bucket is None or key is None:
            log.warning("Missing Required Properties in: %s", s3_object)
            next
        version_id = s3_object.get("VersionId")
        delete_obj = {"Key": key}
        if version_id:
            delete_obj["VersionId"] = version_id

        try:
            response = s3.get_object(Bucket=bucket, Key=key)
            account_details.append(json.load(response["Body"]))
            deletes[s3_object["Bucket"]].append(delete_obj)
        except:
            pass

    for bucket in deletes:
        response = s3.delete_objects(
                Bucket=bucket,
                Delete={
                    "Objects": deletes[bucket],
                    "Quiet": True
                })
        if "Errors" in response:
            errors.extend(response["Errors"])

    return account_details

def persist_account_info_to_codecommit(repo_account, repo_region, repo, branch,
        path, account_details):
    log.debug("persist_account_info_to_codecommit: %s", account_details)

    if len(account_details) == 0:
        return {}

    session = assume_session(
        "AutomateOnboard",
        "asap-stack-sets",
        (repo_account, "asap-cicd"),
        region_name=repo_region,
    )

    errors = []
    files = []
    lifecycle = None
    for account_detail in account_details:
        try:
            lifecycle = account_detail["SsmParameters"]["/global/Lifecycle"]
            files.append((
                f"{path}/{lifecycle}.json",
                json.dumps(account_detail, indent=4)
            ))
        except Exception as exc:
            errors.append(f"Exception {repr(exc)} for {account_detail}")

    log.debug("Looking to commit: %s", files)
    if len(files) == 1:
        commit_msg = "Onboard " + lifecycle
        commit_id = get_branch_commit(repo, branch, session)
        commit_id, ts = commit_files(repo, commit_id, commit_msg,
                branch, files, session)
        history = [commit_id]
    else:
        commit_msg = "Update Account Information"
        commit_id, ts, history = sync_folders(repo, commit_msg, branch, files,
                session)

    return {
        "CommitId": commit_id,
        "Timestamp": str(ts),
        "History": history,
        "Errors": errors
    }

def persist_account_info_handler(event, context):
    log.debug("%s event: %s", context.function_name, json.dumps(event))
    repo_account = event["SourceAccountId"]
    repo_region = event["SourceAccountRegion"]
    repo = event["Repository"]
    repo_branch = event["RepositoryBranch"]
    repo_path = event["RepositoryPath"]

    s3_objects = []
    errors = []
    event_account_details = event["AccountDetails"]
    if isinstance(event_account_details, dict):
        event_account_details = [event_account_details]

    for rec in event_account_details:
        if "ErrorInfo" in rec:
            errors.append(rec)
        else:
            s3_objects.append(rec)
    account_details = gather_s3_objects(s3_objects)

    rtn = persist_account_info_to_codecommit(repo_account, repo_region,
            repo, repo_branch, repo_path, account_details)
    rtn["Errors"].extend(errors)

    return rtn

def accounts_from_stackset_instances_handler(event, context):
    """
    Get list off all accounts from provisioned stackset instances
    Determine AccountGeneration from existance in V2 specific stackset
    """

    logging.info("Processing stack_set_instances event: %s",
            json.dumps(event))

    all_stackset_name = event["AllAccountStackSetName"]
    v2_stackset_name = event["Aws2AccountStackSetName"]

    stackset_account = event["StackSetAccountId"]
    stackset_region = event["StackSetAccountRegion"]

    session = assume_session(
        "Onboarding",
        "asap-stack-sets", (stackset_account, "asap-cicd"),
        region_name=stackset_region)

    all_accounts = stack_instances(session, all_stackset_name)
    logging.debug("All Accounts found: %s", json.dumps(all_accounts))
    v2_accounts = stack_instances(session, v2_stackset_name)
    logging.debug("V2 Accounts found: %s", json.dumps(v2_accounts))

    logging.info("Adding devl-ceng specifically. It is not an instance.")
    devl_ceng = {'Account': '172587047172', 'Region': 'us-east-1'}
    all_accounts.append(devl_ceng)
    v2_accounts.append(devl_ceng)

    for account in all_accounts:
        if account in v2_accounts:
            account["AccountGeneration"] = "v2"
        else:
            account["AccountGeneration"] = "v1"

    return all_accounts

def test_one_account():
    import unittest.mock as mock
    context = mock.MagicMock()

    devl_ceng = "172587047172"
    devl_ceng_region = "us-east-1"
    devl_ccoe = "648156244979"
    devl_ccoe_region = "us-east-1"
    bucket_name = "fnma-asap-devl-ceng"
    prefix = "asap-oats/maas-data"
    execution_id = "test"

    repo = "s5ugys-test"
    branch = "master"
    path = "accounts"


    event = {
        "Account": devl_ccoe,
        "Region": devl_ccoe_region,
        "BucketName": bucket_name,
        "Prefix": prefix,
        "ExectionId": execution_id,
        "AccountGeneration": "v2",
        "SsmParameters": [
            "/global/Lifecycle"
        ]
    }

    response = get_account_details_handler(event, context)
    print(response)
    breakpoint()

    event = {
        "SourceAccountId": devl_ceng,
        "SourceAccountRegion": devl_ceng_region,
        "Repository": repo,
        "RepositoryBranch": branch,
        "RepositoryPath": path,
        "AccountDetails": response
    }
    print(persist_account_info_handler(event, context))


def test_accounts_from_stacksets():
    import unittest.mock as mock
    context = mock.MagicMock()

    devl_ceng = "172587047172"
    devl_ceng_region = "us-east-1"

    event = {
        'AllAccountStackSetName': 'asap-cicd',
        'Aws2AccountStackSetName': 'asap-yapr-cicd',
        'StackSetAccountId': devl_ceng,
        'StackSetAccountRegion': devl_ceng_region,
        'Account':  '739363106608',
        'Region': 'us-east-2'
    }

    print(
        accounts_from_stackset_instances_handler(event, context)
    )

def test_all_accounts_update():
    from multiprocessing import Pool
    import unittest.mock as mock
    context = mock.MagicMock()

    devl_ceng = "172587047172"
    devl_ceng_region = "us-east-1"
    devl_ccoe = "648156244979"
    devl_ccoe_region = "us-east-1"
    bucket_name = "fnma-asap-devl-ceng"
    prefix = "asap-oats/maas-data"
    execution_id = "test"

    repo = "s5ugys-test"
    branch = "master"
    path = "accounts"

    event = {
        'AllAccountStackSetName': 'asap-cicd',
        'Aws2AccountStackSetName': 'asap-yapr-cicd',
        'StackSetAccountId': devl_ceng,
        'StackSetAccountRegion': devl_ceng_region,
        'Account':  '739363106608',
        'Region': 'us-east-2'
    }

    outputs = []

    response = accounts_from_stackset_instances_handler(event, context)
    starmap = []
    for account in response:
        if "AccountGeneration" not in account:
            account["AccountGeneration"] = "v2"
        account.update({
            "BucketName": bucket_name,
            "Prefix": prefix,
            "ExectionId": execution_id,
            "SsmParameters": [
                "/global/Lifecycle"
            ]})
        starmap.append((account, None))

    with Pool(100) as p:
        outputs = p.starmap(get_account_details_handler, starmap)


    event = {
        "SourceAccountId": devl_ceng,
        "SourceAccountRegion": devl_ceng_region,
        "Repository": repo,
        "RepositoryBranch": branch,
        "RepositoryPath": path,
        "AccountDetails": outputs
    }
    print(persist_account_info_handler(event, context))



if __name__ == '__main__':
    import unittest.mock as mock
    context = mock.MagicMock()

    test_all_accounts_update()

#    test_one_account()
    output = {
        "Bucket": "fnma-asap-devl-ceng",
        "Key": "asap-oats/maas-data/arn:aws:states:us-east-1:172587047172:execution:CollectAccountDetails-6UWjYBR13DvE:a8bfb5b7-d198-41b5-a9fc-4e2a5446b916/991444220964-us-east-2.json",
        "VersionId": "h.OIbEt6elZuZxf75IFE0D5407V16cGw"
    }
    event = {
        "SourceAccountId": "172587047172",
        "SourceAccountRegion": "us-east-1",
        "Repository": "eng-asap-maas-config",
        "RepositoryBranch": "dev",
        "RepositoryPath": "accounts",
        "AccountDetails": [output]
    }
    breakpoint()
    persist_account_info_handler(event, context)

