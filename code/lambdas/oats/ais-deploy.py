#!/usr/bin/env python3

from collections import OrderedDict
import logging
import json
import re
import boto3
import os
import zipfile
import botocore

from iam import assume_session
from ssm import (
    get_target_parameter,
    get_target_lifecycle,
    get_target_lifecycle_simple,
    get_target_lambda_subnets,
    get_target_environment,
    get_is_target_account_contingency,
    get_target_default_vpcid
)
from codecommit import (
    get_codecommit_file,
    commit_files,
    merge_branches
)

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
logging.getLogger("botocore").setLevel(logging.INFO)


def deploy_netptune_handler(event, context):
    log.debug("deploy_netptune_handler called with: %s",
              json.dumps(event))

    account = event["Account"]
    region = event["Region"]
    repo_account = event.get("RepositoryAccount","172587047172")
    repo_region = event.get("RepositoryRegion","us-east-1")
    repo = event.get("Repository","eng-asap-data-infra")
    template = event.get("NeptuneTemplateFile","asap-data-graphs.yml")
    branch = event.get("RepoBranch","dev")
    dry_run = event.get("DryRun",True)

    session = assume_session(
        "AISDeploy",
        "asap-stack-sets",
        (repo_account, "asap-cicd"),
        region_name=repo_region
    )

    

    commit_id, timestamp, template_body = get_codecommit_file(
        repo, template, branch, session=session
    )

    log.debug(template_body)

    remote_session = assume_session(
        "AISDeploy",
        "asap-stack-sets",
        (account, "asap-cicd"),
        region_name=region
    )

    log.debug("Successfully connected to: %s", remote_session.client('sts').get_caller_identity().get('Account'))

    vpc_id = get_target_default_vpcid(account,region)    
    ec2 = remote_session.resource('ec2')
    vpc = ec2.Vpc(vpc_id)
    log.debug(vpc.id)
    
    subnet_a, subnet_b = None, None
    for subnet in vpc.subnets.all():
        for tag in subnet.tags:
            if tag["Key"] == "Name":
                if "data-a" in tag["Value"]:
                    subnet_a = subnet.id
                elif "data-b" in tag["Value"]:
                    subnet_b = subnet.id

    if subnet_a == None or subnet_b == None:
        log.error("Data subnets not found:")
        log.error("data-a subnet %s:", subnet_a)
        log.error("data-a subnet %s:", subnet_b)
        raise Exception("Data subnets not found")
   
    sg_id = None
    for sg in vpc.security_groups.all():
        if "asap-lambda" in sg.group_name:
            sg_id = sg.id
    
    if sg_id == None:
        log.error("asap-lambda security group not found")
        raise Exception("asap-lambda security group not found")

    kms = remote_session.client("kms")
    response = kms.list_aliases()

    key_arn = f"arn:aws:kms:{region}:{account}:alias/fnma/asap"

    parameters=[
        {
            'ParameterKey': 'VpcId',
            'ParameterValue': vpc_id
        },
        {
            'ParameterKey': 'Subnet1',
            'ParameterValue': subnet_a
        },
        {
            'ParameterKey': 'Subnet2',
            'ParameterValue': subnet_b
        },
        {
            'ParameterKey': 'SecurityGroup',
            'ParameterValue': sg_id
        },
        {
            'ParameterKey': 'KmsKeyArn',
            'ParameterValue': key_arn
        }
    ]

    log.info(parameters)

    tags = [
        {
            "Key": "ApplicationShortName",
            "Value": "asap"
        },
        {
            "Key": "AssetID",
            "Value": "MSR02125"
        },
        {
            "Key": "CostCenter",
            "Value": "487"
        }
    ]

    log.info(tags)

    cfn = remote_session.client("cloudformation")
    response = None
    if not dry_run:
        response = cfn.create_stack(
            StackName='asap-data-graphs',
            TemplateBody=template_body,
            Parameters=parameters,
            Tags=tags
        )

    log.info(response)
    if response != None:
        event["StackId"] = response["StackId"]
        event["TemplateCommitId"] = commit_id
    elif not dry_run:
        raise Exception("No Response from cloudformation!")

    return event

def push_policies_handler(event, context):
    log.debug("push_policies_handler called with: %s",
              json.dumps(event))
    
    account = event["Account"]
    region = event["Region"]
    project_name = event.get("ProjectName", "asap-ais-policies-v3release")
    
    release_account = event.get("ReleaseAccount","172587047172")
    release_region = event.get("ReleaseRegion","us-east-1")
    
    lifecycle = get_target_lifecycle(account, region)
    
    localzip1='/tmp/source.zip'
    localzip2='/tmp/v3source.zip'
    source_path = '/tmp/source/'
    fnma_changes_path = os.path.join(source_path,'changes/fnma_changes.json')
    changed_paths_path = os.path.join(source_path,'events/fnma_changed_paths.json')
    
    #find latest build and download zip to local
    session = assume_session(
        "AISDeploy",
        "asap-stack-sets",
        (release_account, "asap-cicd"),
        region_name=release_region
    )
    
    build_client = session.client('codebuild')
    response = build_client.list_builds_for_project(
        projectName=project_name,
        # sortOrder='ASCENDING'|'DESCENDING'
    )
    
    if response != None and response['ResponseMetadata']['HTTPStatusCode'] // 100 == 2:
        build_id = response['ids'][0]
        log.info(f"Getting artfiact from {build_id}")
    else:
        log.error("Bad response from CodeBuild! 1")
        raise Exception(response)
        
    response = build_client.batch_get_builds( ids = [build_id])
    
    if response == None or response['ResponseMetadata']['HTTPStatusCode'] // 100 != 2:
        log.error("Bad response from CodeBuild! 2")
        raise Exception(response)
    elif response['builds'][0]['buildStatus'] != 'SUCCEEDED':
        log.error("CodeBuild is not in SUCCEDED status:")   
        raise Exception(response['builds'][0]['buildStatus'])
        
    response['builds'][0]['startTime'] = None
    response['builds'][0]['endTime'] = None
    response['builds'][0]['phases'] = None
    
    downloadZipLoc = response['builds'][0]['artifacts']['location']
    downloadBucket = downloadZipLoc.split(':')[-1].split('/')[0]
    downloadKey = downloadZipLoc.split(':')[-1].replace(downloadBucket+'/','')
    
    s3 = session.resource('s3')
    if not os.path.isdir(source_path):
        os.mkdir(source_path,0o777)
    
    log.info(f"Getting zip file {downloadKey} from {downloadBucket}...")
    downloadZip = s3.Object(downloadBucket, downloadKey)
    downloadZip.download_file(localzip1) 

    session = assume_session(
        "AISDeploy",
        "asap-stack-sets",
        (account, "asap-cicd"),
        region_name=region
    )
    
    s3bucket = get_target_parameter(account, region, "/asap/monitoredbucketname", session)
    s3sourceKey = get_target_parameter(account, region, "/asap/ais/sourceKey", session)
    s3key = f"releases/asap-ais-policies/{s3sourceKey}"
    s3 = session.resource('s3')
    uploadZip = s3.Object(s3bucket, s3key)

    log.debug(os.listdir(path='/tmp/'))
    
    log.info(f"Extracting zip file {localzip1} into /tmp/...")
    with zipfile.ZipFile(localzip1, 'r') as source_zip:
        source_zip.extractall(path='/tmp/')
    log.info(f"Extracting zip file {localzip2} into {source_path}...")
    with zipfile.ZipFile(localzip2, 'r') as source_zip:
        source_zip.extractall(path=source_path)
        
    log.debug(os.listdir(path=source_path))
        
    log.info(f"Generating new {fnma_changes_path}...")

    body = {
        "reread_tokens": False,
        "changes": {
            "change_ticket": "N/A",
            "description": f"Zipfile created by Automated Policy Engine Deployment for {lifecycle}"
        }
    }
    
    log.debug(body)
        
    with open(fnma_changes_path, 'w') as fnma_changes_file:
        json.dump(body, fnma_changes_file)
        
    log.info("Generating list of config.json files....")
    
    config_file_list = list()
    for (dirpath, dirnames, filenames) in os.walk(os.path.join(source_path,"role")):
        for file in filenames:
            if file == 'config.json':
                config_file_list.append(os.path.join(dirpath, file).replace(source_path,""))
                
    log.debug(config_file_list)
    
    for (dirpath, dirnames, filenames) in os.walk(os.path.join(source_path,"policy")):
        for file in filenames:
            if file == 'config.json':
                config_file_list.append(os.path.join(dirpath, file).replace(source_path,""))
                
    log.debug(config_file_list)
    
    log.info(f"Generating new {changed_paths_path}...")

    body = {
        "commit_message": {
            "commitId": "",
            "treeId": "",
            "parents": [],
            "message": f"Zipfile created by Automated Policy Engine Deployment for {lifecycle}",
            "author": {
                "name": "",
                "email": "",
                "date": ""
            },
            "committer": {
                "name": "",
                "email": "",
                "date": ""
            },
            "additionalData": ""
        },
        "final_path_list": config_file_list
    }

    log.debug(body)
    
    with open(changed_paths_path, 'w') as changed_paths_file: 
        json.dump(body, changed_paths_file)

    log.info("Generating list of all files...")    
    filePaths = []
    for root, directories, files in os.walk(source_path):
        for filename in files:
            filePath = os.path.join(root, filename)
            filePaths.append(filePath)
        
    log.info("Zipping file...") 
    with zipfile.ZipFile(localzip2, 'w') as source_zip:
            for file in filePaths:
                zipped_filepath =  file.replace(source_path,"")
                log.debug(f"Putting {file} into zip: {zipped_filepath}")
                source_zip.write(file,zipped_filepath)

    log.info(f"Uploading {localzip2} to {s3bucket}/{s3key}...")
    uploadZip.upload_file(localzip2)
    
    return event

def create_parameters_handler(event, context):
    log.debug("create_parameters_handler called with: %s",
            json.dumps(event))

    account = event["Account"]
    region = event["Region"]
    roleTypes = event["RoleTypes"]

    session = assume_session(
        "AISDeploy",
        "asap-stack-sets",
        (account, "asap-cicd"),
        region_name=region
    )

    ssm = session.client('ssm', region_name=region)

    response = ssm.put_parameter(Name='/provisioning/core/Account/CAR/ContRoleArnForTFDeploy',
        Value='arn:aws:iam::991444220964:role/terraform-cont-etss-comp', Type='String', Overwrite=True)
    response = ssm.put_parameter(Name='/provisioning/core/Account/CAR/RoleArnForTFDeploy',
        Value='arn:aws:iam::991444220964:role/terraform-prod-etss-comp', Type='String', Overwrite=True)


    environment = get_target_environment(account, region)

    if environment == 'Sandbox':
        
        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-test-data-management-nonprod"
        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-cloudwatch-metrics"
        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-event-transporter-nonprod"

        response = ssm.put_parameter(Name='/provisioning/core/Account/CAR/RoleArnForTwistlckEcrScan',
            Value='arn:aws:iam::175626457831:role/sdbx-sf-twstlk-instance', Type='String', Overwrite=True)

    elif environment == 'Development':

        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-codecommit-read" 
        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-test-data-management-nonprod"
        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-cloudwatch-metrics"
        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-event-transporter-nonprod"

        response = ssm.put_parameter(Name='/provisioning/core/Account/CAR/RoleArnForTwistlckEcrScan',
            Value='arn:aws:iam::189693026861:role/devl-twstlk-instance', Type='String', Overwrite=True)
            
    elif environment == 'Test':

        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-test-data-management-nonprod"
        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-cloudwatch-metrics"
        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-event-transporter-nonprod"
            
        response = ssm.put_parameter(Name='/provisioning/core/Account/CAR/RoleArnForTwistlckEcrScan',
            Value='arn:aws:iam::342797660180:role/test-twstlk-instance', Type='String', Overwrite=True)

    elif environment == 'Acceptance':

        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-test-data-management-nonprod"
        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-cloudwatch-metrics"
        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-event-transporter-nonprod"

        response = ssm.put_parameter(Name='/provisioning/core/Account/CAR/RoleArnForTwistlckEcrScan',
            Value='arn:aws:iam::254915557507:role/acpt-twstlk-instance', Type='String', Overwrite=True)

    elif environment == 'Acceptance-Contingency':

        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-test-data-management-nonprod"
        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-cloudwatch-metrics"
        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-event-transporter-nonprod"
        
        response = ssm.put_parameter(Name='/provisioning/core/Account/CAR/RoleArnForTwistlckEcrScan',
            Value='arn:aws:iam::254915557507:role/acpt-cont-twstlk-instance', Type='String', Overwrite=True)

    elif environment == 'Production' or environment == 'CLVE':

        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-test-data-management"
        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-event-transporter-prod"
            
        response = ssm.put_parameter(Name='/provisioning/core/Account/CAR/RoleArnForTwistlckEcrScan',
            Value='arn:aws:iam::106139703084:role/prod-twstlk-instance', Type='String', Overwrite=True)

    elif environment == 'Contingency':

        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-test-data-management"
        temp = roleTypes["caradmin"]
        roleTypes["caradmin"] = f"{temp},v3-car-event-transporter-prod"
            
        response = ssm.put_parameter(Name='/provisioning/core/Account/CAR/RoleArnForTwistlckEcrScan',
            Value='arn:aws:iam::106139703084:role/cont-twstlk-instance', Type='String', Overwrite=True)

    else:
        log.error(f"Unknown environement: {environment}")
        raise Exception("Unknown environement: " + environment)

    #Role Types, add templates to roleTypes value string if not already existing
    # for temp_name in templates:
    #     if temp_name not in roleTypes:
    #         roleTypes = f"{roleTypes},{temp_name}"


    final_output = []     
    for roleTypeName in roleTypes:

        #common params
        response = ssm.put_parameter(Name=f'/{roleTypeName}/common/AppCode',
                Value='FKK', Type='String', Overwrite=True)
        response = ssm.put_parameter(Name=f'/{roleTypeName}/common/AssetID',
                Value='MSR00036', Type='String', Overwrite=True)
        response = ssm.put_parameter(Name=f'/{roleTypeName}/common/CostCenter',
                Value='488', Type='String', Overwrite=True)
        response = ssm.put_parameter(Name=f'/{roleTypeName}/common/ApplicationName',
                Value='AWS', Type='String', Overwrite=True)
        response = ssm.put_parameter(Name=f'/{roleTypeName}/common/ProjectCode',
                Value='B08BASEADM', Type='String', Overwrite=True)

        #Role Types final put   
        response = ssm.put_parameter(Name=f'/asap/ais/{roleTypeName}/roleTypes',
                Value=roleTypes[roleTypeName], Type='String', Overwrite=True)

        final_output.append(
            {
                "Account": account,
                "Region": region,
                "RoleType": roleTypeName,
                "RoleTypeValue": roleTypes[roleTypeName]
            }
        )
    
    return final_output

def create_horizontal_handler(event, context):
    log.debug("create_horizontal_handler called with: %s",
            json.dumps(event))
    
    account = event["Account"]
    region = event["Region"]
    roleType = event["RoleType"]
    templates = event["RoleTypeValue"].split(",")

    session = assume_session(
        "AISDeploy",
        "asap-stack-sets",
        (account, "asap-cicd"),
        region_name=region
    )

    config = botocore.config.Config(read_timeout=900, connect_timeout=900, retries={'max_attempts': 0})
    l = session.client('lambda',config=config)
    response = l.invoke(
        FunctionName="asap-ais-rebuild-role", 
        Payload=json.dumps(
            {
                "provisioning_type": roleType,
                "app_short_name": roleType,
                "role_type": templates,
                "services": [],
                "rebuild_role": "True",
                "reread_tokens": "True"
            }
        ).encode(),
        InvocationType="RequestResponse"
    )

    if response.get("FunctionError", None):
        log.error("core-resources returned an error:")
        raise Exception(response['Payload'].read())
    elif response['StatusCode'] // 100 != 2:
        log.error("bad response:")
        response['Payload'] = None
        raise Exception(response)
    else:
        return response['Payload'].read()


