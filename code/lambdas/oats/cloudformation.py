#!/usr/bin/env python3

import json
import logging
from iam import assume_session

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


class StackNotFound(Exception):
    pass


def get_worker_queue_urls(stack_name, session):
    cfn = session.resource('cloudformation')
    stack = cfn.Stack(stack_name)

    queue_keys = ['CommissioningQueueURL', 'DecommissioningQueueURL']
    outputs = {o['OutputKey']: o['OutputValue'] for o in stack.outputs}
    outputs = {k: v for k, v in outputs.items() if k in queue_keys}

    return outputs


def get_worker_queue_urls_handler(event, context):
    account = event['Account']
    region = event.get('Region', 'us-east-1')
    stack_name = event.get('WorkerStackName', 'asap-aarp')

    session = assume_session(
        'Onboarding', 'asap-stack-sets', (account, 'asap-cicd'), region_name=region
    )

    return get_worker_queue_urls(stack_name, session)


def get_stack_status(stack_name, session):
    cfn = session.client('cloudformation')
    try:
        response = cfn.describe_stacks(StackName=stack_name)
        stack = response['Stacks'][0]

        return {
            'StackName': stack_name,
            'Status': stack['StackStatus'],
            'Reason': stack.get('StackStatusReason'),
        }

    except cfn.exceptions.ClientError as exception:
        if 'does not exist' in exception.args[0]:
            raise StackNotFound
        else:
            raise


def get_stack_status_handler(event, context):
    account = event['Account']
    region = event.get('Region', 'us-east-1')
    stack_name = event['StackName']

    session = assume_session(
        'Onboarding', 'asap-stack-sets', (account, 'asap-cicd'), region_name=region
    )

    return get_stack_status(stack_name, session)


def disable_termination_protection_handler(event, context):
    log.debug('disable_termination_protection_handler: %s', json.dumps(event))
    account = event['Account']
    region = event.get('Region', 'us-east-1')
    stack_name = event['StackName']

    session = assume_session(
        'Onboarding', 'asap-stack-sets', (account, 'asap-cicd'), region_name=region
    )

    cfn = session.client('cloudformation')
    try:
        response = cfn.update_termination_protection(
            StackName=stack_name, EnableTerminationProtection=False,
        )
    except cfn.exceptions.ClientError as exception:
        if 'being in state [DELETE_IN_PROGRESS]' not in exception.args[0]:
            raise


def get_stack_resource(stack_name, logical_name, session):
    "Get the stacks logicial id's phyicsal id"
    cfn = session.resource('cloudformation')
    resource = cfn.StackResource(stack_name, logical_name)
    return resource.physical_resource_id


def get_dns_test_function(session):
    cfn = session.resource('cloudformation')

    stack_names = ['asap-dns-sam', 'asap-dns-test-sam']
    for stack_name in stack_names:
        try:
            response = get_stack_status(stack_name, session)
            resource = cfn.StackResource(stack_name, 'DnsTestFunction')
            return resource.physical_resource_id

            return response
        except StackNotFound:
            pass

    return None


def invoke_dns_test_lambda_handler(event, context):
    "DNS has 2 different stack names, test and std - find name on this account"
    account = event['Account']
    region = event.get('Region', 'us-east-1')

    session = assume_session(
        'Onboarding', 'asap-stack-sets', (account, 'asap-cicd'), region_name=region
    )
    dns_test_function_name = get_dns_test_function(session)

    l = session.client('lambda')
    response = l.invoke(
        FunctionName=dns_test_function_name, Payload=json.dumps({}).encode(),
    )

    payload = response['Payload'].read()

    if response['StatusCode'] // 100 != 2:
        raise Exception(response, payload)

    return payload


if __name__ == '__main__':
    import unittest.mock as mock

    context = mock.MagicMock()

    event = {'Account': '285488728505', 'Region': 'us-east-1'}

    # print(get_worker_queue_urls_handler(event, context))

    # event['StackName'] = 'fkk-application-stack'
    # print(get_stack_status_handler(event, context))

    print(invoke_dns_test_lambda_handler(event, context))
