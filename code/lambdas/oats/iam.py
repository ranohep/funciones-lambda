#!/usr/bin/env python3
"""
Lambda for the status of Pipelines in other accounts
"""

import json
import logging
import datetime
from enum import Enum
import boto3

logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


def assume_session(session_name, *account_role_tuples, **kwargs):
    sts = boto3.client('sts')
    cred_params = {}
    for account_role in account_role_tuples:
        if isinstance(account_role, str) or len(account_role) == 1:
            account = sts.get_caller_identity()['Account']
            role = account_role
        else:
            account, role = account_role

        cred_params = dict(
            RoleArn=f'arn:aws:iam::{account}:role/{role}', RoleSessionName=session_name
        )

        response = sts.assume_role(**cred_params)
        cred = response["Credentials"]
        cred_params = dict(
            aws_access_key_id=cred["AccessKeyId"],
            aws_secret_access_key=cred["SecretAccessKey"],
            aws_session_token=cred["SessionToken"],
        )
        sts = boto3.client('sts', **cred_params)

    session = boto3.session.Session(**kwargs, **cred_params)
    return session


def assume_role(account, role, session_name, session=None):
    """Assume Role and Return Credentials"""
    log.debug("assuming role %s/%s", account, role)
    if session is None:
        session = boto3.session.Session()

    sts = session.client("sts")

    sts_params = dict(
        RoleArn=f"arn:aws:iam::{account}:role/{role}", RoleSessionName=session_name,
    )
    response = sts.assume_role(**sts_params)

    cred = response["Credentials"]
    params = dict(
        aws_access_key_id=cred["AccessKeyId"],
        aws_secret_access_key=cred["SecretAccessKey"],
        aws_session_token=cred["SessionToken"],
    )

    return params


def pipeline_status(client, name):
    """Get Pipeline Status using client"""
    log.debug("Getting pipeline status of %s", name)
    response = client.get_pipeline_state(name=name)
    logging.debug("Response = %s", response)
    first, last = (response["stageStates"][0], response["stageStates"][-1])

    start_status = first.get("latestExecution", {}).get("status")
    if start_status:
        start_time = first["actionStates"][0]["latestExecution"][
            "lastStatusChange"
        ].isoformat()
    else:
        start_time = None

    end_status = last.get("latestExecution", {}).get("status")
    if end_status == "Succeeded":
        end_time = last["actionStates"][-1]["latestExecution"][
            "lastStatusChange"
        ].isoformat()
    else:
        end_time = None

    if end_time and end_time > start_time:
        current_state = "COMPLETE"
    else:
        current_state = "INCOMPLETE"

    status = (name, current_state, start_time, end_time)


def pipeline_release(client, name):
    """Trigger Release on Pipeline"""
    log.debug("Trigger Pipeline %s", name)
    client.start_pipeline_execution(name=name)


def stackset_status(account, region, name):
    cfn = boto3.client("cloudformation")
    response = cfn.describe_stack_instance(
        StackSetName=name, StackInstanceAccount=account, StackInstanceRegion=region
    )

    return response["StackInstance"]["Status"]


def handler(event, context):
    logging.debug("event = %s", json.dumps(event))

    account = event["Account"]
    region = event.get("Region", "us-east-1")
    pipeline = event["PipelineName"]

    role = event.get("AssumeRole", "asap-cicd")

    sts = boto3.client("sts")
    creds = assume_role(account, role, "PipelineStatus")

    client = boto3.client("codepipeline", **creds)
    status = pipeline_status(client, pipeline)
    logging.debug(status)
    return json.dumps(status)


if __name__ == "__main__":
    from unittest import mock

    sts = boto3.client("sts")
    source_account = sts.get_caller_identity()["Account"]
    target_account = "172587047172"

    creds = assume_role(source_account, "asap-stack-sets", "PipelineStatus")

    sts = boto3.client("sts", **creds)
    creds = assume_role("172587047172", "asap-cicd", "PipelineStatus", sts=sts)

    source = boto3.client("codepipeline")
    target = boto3.client("codepipeline", **creds)

    pipelines = (
        (source_account, "eng-asap-account-management"),
        (source_account, "eng-asap-cicd-mgmt"),
        (source_account, "eng-asap-infra-dev"),
        (source_account, "eng-asap-infra-release"),
        (target_account, "asap-infra"),
        (target_account, "asap-dns"),
    )

    status = []

    for pipeline in pipelines:
        try:
            if pipeline[0] == source_account:
                status.append(pipeline_status(source, pipeline[1]))
            elif pipeline[0] == target_account:
                status.append(pipeline_status(target, pipeline[1]))
        except:
            logging.exception("Exception Caught")

    logging.debug("Status = %s", status)
    print(json.dumps(status, indent=4))
