#!/usr/bin/env python3
"""
Lambda for the status of Pipelines in other accounts
"""

import json
import logging
import datetime
from enum import Enum
from iam import assume_session
import boto3

logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


class StackSetNotFoundError(Exception):
    pass


class StackInstanceNotFoundError(Exception):
    pass


class StackInstanceInoperableError(Exception):
    pass


def stackset_instance_status(session, name, stackset_account, stackset_region,
        account, region):
    log.debug("getting stack instance status %s %s %s", name, account, region)
    cfn = session.client("cloudformation")
    try:
        response = cfn.describe_stack_instance(
            StackSetName=name, StackInstanceAccount=account, StackInstanceRegion=region
        )
        log.debug('response for describe stack instance %s, %s, %s: %s',
                  name, account, region, response)
        if response["StackInstance"]["Status"] == "INOPERABLE":
            raise StackInstanceInoperableError

    except cfn.exceptions.StackSetNotFoundException:
        raise StackSetNotFoundError

    except cfn.exceptions.StackInstanceNotFoundException:
        raise StackInstanceNotFoundError

    return response["StackInstance"]["Status"]


def instance_status_handler(event, context):
    logging.debug("event = %s", json.dumps(event))

    stackset = event["StackSetName"]
    stackset_account = event["StackSetAccountId"]
    stackset_region = event["StackSetAccountRegion"]
    account = event["Account"]
    region = event.get("Region", "us-east-1")

    session = assume_session(
            "Onboarding",
            "asap-stack-sets", (stackset_account, "asap-cicd"),
            region_name=stackset_region)

    status = stackset_instance_status(session, stackset,
            stackset_account, stackset_region, account, region)
    logging.debug(
        "StackSet %s on %s/%s Instance %s in Region %s Status: %s",
        stackset,
        stackset_account,
        stackset_region,
        account,
        region,
        status,
    )

    return_status = {
        "StackSetName": stackset,
        "Account": account,
        "Region": region,
        "Status": status,
    }
    return return_status


def add_stack_instance(session, stackset, account, region):
    logging.debug("Adding %s/%s to %s", account, region, stackset)
    cfn = session.client("cloudformation")

    _ = cfn.create_stack_instances(
        StackSetName=stackset,
        Accounts=[account],
        Regions=[region],
        OperationPreferences=dict(FailureToleranceCount=1, MaxConcurrentCount=1),
    )


def add_instance_handler(event, context):
    logging.debug("event = %s", json.dumps(event))

    stackset = event["StackSetName"]
    stackset_account = event["StackSetAccountId"]
    stackset_region = event["StackSetAccountRegion"]
    account = event["Account"]
    region = event.get("Region", "us-east-1")

    session = assume_session(
            "Onboarding",
            "asap-stack-sets", (stackset_account, "asap-cicd"),
            region_name=stackset_region)

    status = add_stack_instance(session, stackset, account, region)
    logging.debug(
        "StackSet %s on %s/%s Instance %s in Region %s Status: %s",
        stackset,
        stackset_account,
        stackset_region,
        account,
        region,
        status,
    )


def stack_instances(session, stackset_name):
    cfn = session.client("cloudformation")

    accounts = []
    params = {"StackSetName": stackset_name}
    while True:
        response = cfn.list_stack_instances(**params)
        for instance in response["Summaries"]:
            accounts.append({
                "Account": instance["Account"],
                "Region": instance["Region"]
            })

        if "NextToken" in response:
            params["NextToken"] = response["NextToken"]
        else:
            break

    return accounts


