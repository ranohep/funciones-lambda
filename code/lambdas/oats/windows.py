#!/usr/bin/env python3

import logging
import json
from iam import assume_session
from ssm import (
        get_target_lifecycle,
        get_target_environment,
        get_target_parameter,
        set_target_parameter
)

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

class UnexpectedState(Exception): pass

class ActiveDirectoryEnvironment:
    PRODUCTION_ENVIRONMENTS = {"Production", "Contingency", "CLVE"}
    def __init__(self, info, account, region):
        local_info = [
            e for e in info.values() if e["AccountId"] == account
        ]

        if local_info:
            self.name = local_info[0]["Name"]
            self.account_id = local_info[0]["AccountId"]
        else:
            environment = get_target_environment(account, region)
            if environment in self.PRODUCTION_ENVIRONMENTS:
                idx = "Production"
            else:
                idx = "NonProduction"

            self.name       = info[idx]["Name"]
            self.account_id = info[idx]["AccountId"]

        self.region     = region
        self._owner_id  = None
        self._session   = None

    @property
    def session(self):
        if not self._session:
            self._session = assume_session('Onboarding',
                    'asap-stack-sets',
                     (self.account_id, 'asap-cicd'),
                     region_name=self.region)

        return self._session

    @property
    def owner_id(self):
        if not self._owner_id:
            ds = self.session.client("ds")
            params = {}
            while True:
                response = ds.describe_directories(**params)


                for directory in response["DirectoryDescriptions"]:
                    if directory["Type"] != "MicrosoftAD":
                        continue

                    if directory["Name"] == self.name:
                        self._owner_id = directory["DirectoryId"]
                        break

                if "NextToken" in response:
                    params["NextToken"] = response["NextToken"]
                else:
                    break


        return self._owner_id

    def share_with(self, account_id):
        ds = self.session.client("ds")
        try:
            response = ds.share_directory(
                    DirectoryId=self.owner_id,
                    ShareTarget={"Id": account_id, "Type": "ACCOUNT"},
                    ShareMethod="HANDSHAKE")

            return response["SharedDirectoryId"]
        except ds.exceptions.DirectoryAlreadySharedException:
            params = {
                "OwnerDirectoryId": self.owner_id
            }
            while True:
                response = ds.describe_shared_directories(**params)
                for directory in response["SharedDirectories"]:
                    if directory["OwnerAccountId"] != self.account_id:
                        continue
                    if directory["OwnerDirectoryId"] != self.owner_id:
                        continue
                    if directory["ShareMethod"] != "HANDSHAKE":
                        continue

                    if directory["SharedAccountId"] == account_id:
                        return directory["SharedDirectoryId"]

                if "NextToken" in response:
                    params["NextToken"] = response["NextToken"]
                else:
                    break

            raise UnexpectedState


def share_ad_directory_handler(event, context):
    log.debug("share_ad_directory_handler: %s", event)
    account_id = event['Account']
    region = event['Region']
    info = event["ActiveDirectoryInfo"]
    consumer_directory_path = event["ConsumerDirectoryPath"]

    ad = ActiveDirectoryEnvironment(info, account_id, region)
    log.info("Found for {}/{} share {}/{}".format(
            account_id, region, ad.account_id, ad.name))

    if ad.account_id == account_id:
        consumer_id = ad.owner_id
        log.info("Using Local Directory {}".format(ad.name))
    else:
        consumer_id = ad.share_with(account_id)
        log.info("Shared Directory {} shared with {} as directory id {}" \
                .format(ad.name, account_id, consumer_id))

        session = assume_session('Onboarding',
                        'asap-stack-sets',
                         (account_id, 'asap-cicd'),
                         region_name=region)

        ds = session.client("ds")
        retry = 0
        while retry < 7:
            try:
                _ = ds.accept_shared_directory(SharedDirectoryId=consumer_id)
                log.info("Accepted share on {}/{}".format(account_id, region))
                break
            except ds.exceptions.DirectoryAlreadySharedException:
                log.info("Already Accepted share on {}/{}".format(
                    account_id, region))
                break
            except ds.exceptions.ThrottlingException:
                retry += 1
                log.warning(
                        "Throttle Retry %s for accept shared directory id %s",
                        retry, consumer_id)
                time.sleep(10 * retry)

    try:
        current_consumer_id = get_target_parameter(account_id, region,
                consumer_directory_path)

        if current_consumer_id == consumer_id:
            log.info("SSM Parameter {} already set correctly to {}.".format(
                consumer_directory_path, current_consumer_id))
            return {
                "ConsumerDirectoryId": current_consumer_id
            }
    except:
        pass


    _ = set_target_parameter(account_id, region,
            consumer_directory_path, consumer_id,
            Type="String", Overwrite=True)
    log.info("Set SSM Parameter {} to {} on {}:{}".format(
        consumer_directory_path, consumer_id,
        account_id, region))

    return {
        "ConsumerDirectoryId": consumer_id
    }

def share_windows_ami_handler(event, content):
    account_id = event["Account"]
    region = event["Region"]

    ami_account_id = event["WindowsImageInfo"]["AccountId"]
    ssm_paths = event["WindowsImageInfo"]["GoldImageSsmPaths"]

    session = assume_session('Onboarding',
                    'asap-stack-sets',
                     (ami_account_id, 'asap-cicd'),
                     region_name=region)
    ec2 = session.client("ec2")

    image_ids = {}
    excs = []
    for ssm_path in ssm_paths:
        try:
            ami_id = get_target_parameter(ami_account_id, region, ssm_path, session)
            ec2.modify_image_attribute(
                ImageId=ami_id,
                LaunchPermission={
                    "Add": [
                        {"UserId": account_id}
                    ]
                }
            )
            image_ids[ssm_path] = ami_id
        except Exception as exc:
            log.exception("Attempting to share {} to {} failed: {}".format(
                    ssm_path, account_id, exc))


    return {
        "ImageIds": image_ids
    }


if __name__ == '__main__':
    import unittest.mock as mock

    context = mock.MagicMock()

    sdbx_ceng = "522168356138"
    prod_essp = "255242629704"
    prod_essx = "068270649629"
    clve_ess  = "480289415865"
    acnt_ess  = "250177672777"
    devl_bstn = "535490593863"
    event = {
        "Account": devl_bstn,
        "Region": "us-east-1",
        "ConsumerDirectoryPath": "/wineng/core/ad/id",
        "ActiveDirectoryInfo": {
            "Production": {
                "Name": "fmadprod.prod-essp.aws.fanniemae.com",
                "AccountId": "255242629704"
            },
            "NonProduction": {
                "Name": "fmadnonprod.prod-essx.aws.fanniemae.com",
                "AccountId": "068270649629"
            }
        },
        "WindowsImageInfo": {
            "AccountId": "172587047172",
            "GoldImageSsmPaths": [
                "/wineng/core/image/GoldenWindows2016",
                "/wineng/core/image/GoldenWindows2016-1",
                "/wineng/core/image/GoldenWindows2016-2"
            ]
        }
    }

    breakpoint()
    #print(share_ad_directory_handler(event, context))
    print(share_windows_ami_handler(event, context))


