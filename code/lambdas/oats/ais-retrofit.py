#!/usr/bin/env python3

from collections import OrderedDict
import logging
import json
import re
import boto3
import os
import botocore

from iam import assume_session
from ssm import (
    get_target_parameter,
    get_target_lifecycle,
    get_target_lifecycle_simple,
    get_target_lambda_subnets,
    get_target_environment,
    get_is_target_account_contingency,
    get_target_default_vpcid
)
from codecommit import (
    get_codecommit_file,
    commit_files,
    merge_branches
)

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
logging.getLogger("botocore").setLevel(logging.INFO)

def load_accounts_handler(event, context):
    account_name_list = event.get("Lifecycles",[])
    lifecycle = event.get("Environment",None)
    account_gen = event.get("AccountGeneration","v2")
    role_type = event["RoleType"]

    account = event.get("Account",None)
    region = event.get("Region","us-east-1")


    local_account = context.invoked_function_arn.split(":")[4]
    local_region  = context.invoked_function_arn.split(":")[3]
    
    log.debug("Account|Region for AppConfig: {}|{}".format(local_account,local_region))
    local_session = assume_session(
        "AISRetrofit",
        "asap-stack-sets",
        (local_account, "asap-cicd"),
        region_name=local_region
    )
    
    client = local_session.client('appconfig')
    
    response = client.get_configuration(
        Application='asap-maas',
        Environment='asap-maas-production',
        Configuration='asap-maas-config-profile-codepipeline',
        ClientId=context.function_name
    )
    
    config = json.loads(response["Content"].read())
    
    output = []
    
    for lifecycle_from_config in config:
        if lifecycle is None or lifecycle_from_config == lifecycle:
            for account_from_lifecycle in config[lifecycle_from_config]:
                if len(account_name_list) == 0 or account_from_lifecycle in account_name_list:
                    account_config = config[lifecycle_from_config][account_from_lifecycle]
                    if account_config["AccountGeneration"] == account_gen:
                        
                        #account override                        
                        if account is not None:
                            account_config["Account"] = account
                            account_config["Region"] = region
                            account_config["SsmParameters"]["/global/Lifecycle"] = get_target_lifecycle(account, region)
                        
                        log.debug("{}: {}:{}".format(account_config["SsmParameters"]["/global/Lifecycle"].upper(), account_config["Account"], account_config["Region"]))
                        
                        #####Get app shortnames for vertical role types
                        if role_type in ["core","cicd"]:
                            
                            remote_session = assume_session(
                                "AISRetrofit",
                                "asap-stack-sets",
                                (account_config["Account"], "asap-cicd"),
                                region_name=account_config["Region"]
                            )
                                                    
                            client = remote_session.client('servicecatalog')
                            paginator = client.get_paginator('list_portfolios')
                            page_iterator = paginator.paginate()
                            
                            app_list = ["cloudadm","caradmin"]
                            new_app_list = []
                            
                            for page in page_iterator:
                            
                                for portfolio in page["PortfolioDetails"]:
                                    
                                    if portfolio["DisplayName"] not in app_list:
                                        
                                        log.debug("{}: {}".format(account_config["SsmParameters"]["/global/Lifecycle"].upper(), portfolio["DisplayName"]))
                                        new_app_list.append(portfolio["DisplayName"])
                                        
                                        
                            output.append( {
                                "Account": account_config["Account"],
                                "Region": account_config["Region"],
                                "LifecycleName": account_config["SsmParameters"]["/global/Lifecycle"],
                                "AppShortNames": new_app_list
                              })
                            
                            log.debug("{}: Total apps: {}".format(account_config["SsmParameters"]["/global/Lifecycle"].upper(), len(new_app_list)))
                            
                            if account is not None:
                                return output
                            
                        else:
                            output.append( {
                                "Account": account_config["Account"],
                                "Region": account_config["Region"],
                                "LifecycleName": account_config["SsmParameters"]["/global/Lifecycle"]
                            })
    
                            if account is not None:
                                return output
    return output

def retrofit_account_handler(event, context):
    templates = event["Templates"]
    roleType = event["RoleType"]
    account = event["AccountDetails"]["Account"]
    region = event["AccountDetails"]["Region"]
    name_upper = event["AccountDetails"]["LifecycleName"].upper()
    name_lower = event["AccountDetails"]["LifecycleName"].lower()
    app_short_name = event.get("AppShortName", roleType) #if shortname is not populated than we are doing horizontal role where shortname == roleType
    push_param = event.get("UpdateParameter", True)
    delete_role_list = event.get("RolesToClean", [])

    session = assume_session(
        "AISRetrofit",
        "asap-stack-sets",
        (account, "asap-cicd"),
        region_name=region
    )

    #delete IAM role/policies indicated
    iam = session.resource('iam')
    for delete_role in delete_role_list:
        role_name = delete_role.format(lifecycle = name_lower)
        role = iam.Role(role_name)

        policy_iterator = role.attached_policies.all()
        for policy in policy_iterator:
            response = role.detach_policy(PolicyArn = policy.arn)

            policy_version_iterator = policy.versions.all()
            for policy_version in policy_version_iterator:
                if not policy_version.is_default_version:
                    policy_version.delete()

            policy.delete()
            
        role_policy_iterator = role.policies.all()
        for role_policy in role_policy_iterator:
            role_policy.delete()

    if push_param:
        ssm = session.client('ssm', region_name=region)

        param_name = f"/asap/ais/{roleType}/roleTypes"
        param_value =  ssm.get_parameter(Name=param_name)["Parameter"]["Value"]

        for temp_name in templates:
            if temp_name not in param_value:
                param_value = f"{param_value},{temp_name}"
        
        log.info("Putting parameter: Key='%s', Value='%s'", param_name, param_value)
        response = ssm.put_parameter(Name=param_name, Value=param_value, Type='String', Overwrite=True)

    config = botocore.config.Config(read_timeout=900, connect_timeout=900, retries={'max_attempts': 0})
    l = session.client('lambda',config=config)
    log.info("Invoking lambda for %s (%s:%s) ...",name_upper,account,region)
    response = l.invoke(
            FunctionName="asap-ais-rebuild-role", 
            Payload=json.dumps(
                {
                    "provisioning_type": roleType,
                    "app_short_name": app_short_name,
                    "role_type": templates,
                    "services": [],
                    "rebuild_role": "True",
                    "reread_tokens": "True"
                }
            ).encode(),
            InvocationType="RequestResponse"
        )

    if response['StatusCode'] // 100 != 2:
        response['Payload'] = None
        error_str = "{}|{}|{}|BAD RESPONSE: {}".format(name_upper, account, region, response)
        log.error(error_str)
        raise Exception(error_str)
    elif response.get("FunctionError", None):
        error_str = "{}|{}|{}|FUNCTION RETURNED ERROR: {}".format(name_upper, account, region, response['Payload'].read())
        log.error(error_str)
        raise Exception(error_str)
    else:
        return response['Payload'].read()

def update_oats_handler(event,context):

    templates = event["Templates"]
    roleType = event["RoleType"]
    repo_account = event.get("RepositoryAccount","172587047172")
    repo_region = event.get("RepositoryRegion","us-east-1")
    repo = event.get("Repository","eng-asap-oats")
    json_path = event.get("FilePath","code/statemachines/ais_deploy.json")
    branch = event.get("RepoBranch","dev")
    
    session = assume_session(
        "AISRetrofit",
        "asap-stack-sets",
        (repo_account, "asap-cicd"),
        region_name=repo_region
    )

    commit_id, timestamp, json_body = get_codecommit_file(
        repo, json_path, branch, session=session
    )

    decoded_json = json.loads(json_body)

    param_value = decoded_json["States"]["Create Parameters"]["Parameters"]["RoleTypes"][roleType]
    print_value = f"<AISRetrofit Automation> Template(s) added to {roleType} roleType:"

    for temp_name in templates:
        if temp_name not in param_value:
            param_value = f"{param_value},{temp_name}"
            print_value = f"{print_value} {temp_name}"

    decoded_json["States"]["Create Parameters"]["Parameters"]["RoleTypes"][roleType] = param_value

    return commit_files(
        repo=repo,
        commit_id=commit_id,
        commit_msg=print_value,
        files=[(json_path, json.dumps(decoded_json, indent=2))],
        branch=branch,
        session=session,
    )

