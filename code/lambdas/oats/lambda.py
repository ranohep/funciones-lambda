#!/usr/bin/env python3

import json
import logging
from iam import assume_session
from cloudformation import get_dns_test_function, get_stack_resource
import smtplib
import boto3
import email.utils
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
# Variables for Lifecycle Update
s3 = boto3.resource('s3')
lifecycle_ssm_param = '/asap/provisioning/lifecycle'
ssm_client = boto3.client('ssm')
response = ssm_client.get_parameter(Name=lifecycle_ssm_param)
bucket_params = response['Parameter']['Value'].split(',')
bucket = bucket_params[0]
key = bucket_params[1]
def invoke_commissioning_worker_handler(event, context):
    log.info('invoke_commissioning_worker_handler')
    account = event['Account']
    region = event.get('Region', 'us-east-1')
    stack_name = event.get('StackName', 'asap-aarp')
    session = assume_session(
        'Onboarding', 'asap-stack-sets', (account, 'asap-cicd'), region_name=region
    )

    function_name = get_stack_resource(stack_name, 'Commissioning', session)

    l = session.client('lambda')
    log.info('Invoking %s', function_name)
    l.invoke(
        FunctionName=function_name, Payload=json.dumps({}).encode(),
    )


def invoke_decommissioning_worker_handler(event, context):
    log.info('invoke_decommissioning_worker_handler')
    account = event['Account']
    region = event.get('Region', 'us-east-1')
    stack_name = event.get('StackName', 'asap-aarp')
    session = assume_session(
        'Onboarding', 'asap-stack-sets', (account, 'asap-cicd'), region_name=region
    )

    function_name = get_stack_resource(stack_name, 'Decommissioning', session)

    l = session.client('lambda')
    log.info('Invoking %s', function_name)
    l.invoke(
        FunctionName=function_name, Payload=json.dumps({}).encode(),
    )


def invoke_dns_test_lambda_handler(event, context):
    "DNS has 2 different stack names, test and std - find name on this account"
    account = event['Account']
    region = event.get('Region', 'us-east-1')

    session = assume_session(
        'Onboarding', 'asap-stack-sets', (account, 'asap-cicd'), region_name=region
    )
    dns_test_function_name = get_dns_test_function(session)

    l = session.client('lambda')
    response = l.invoke(
        FunctionName=dns_test_function_name, Payload=json.dumps({}).encode(),
    )

    payload = response['Payload'].read()

    if response['StatusCode'] // 100 != 2:
        raise Exception(response, payload)

    return payload


def isROOTMFAEnabled(event, context):
    account = event['Account']
    region = event.get('Region', 'us-east-1')

    session = assume_session(
        'Onboarding', 'asap-stack-sets', (account, 'asap-cicd'), region_name=region
    )    
    iam = session.client('iam')
    response = iam.get_account_summary()
    summary = response['SummaryMap']
    if summary['AccountMFAEnabled']:
        # MFA is enabled
        print('ROOT MFA is Enabled')
        return True
    else:
        # MFA is not enabled 
        print('ROOT MFA is Not Enabled') 
        return False

def invoke_lifecycle_update_handler(event, context):
    account = event['Account']
    print(account)
    region = event['Region']
    print(region)
    session = assume_session("LifecycleUpdate","asap-stack-sets", (account, "asap-cicd"),region_name=region)
    ssm_client = session.client('ssm')
    lifecycle_ssm_param = '/fnma/account/lifecycle'
    response = ssm_client.get_parameter(Name=lifecycle_ssm_param)
    app_params = response['Parameter']['Value'].split('-')
    #Create the message
    app_short = app_params[1]
    life_cycle = app_params[0]

    if app_short != "" and life_cycle != "":
        obj = s3.Object(bucket,key)
        data = obj.get()['Body']
        json_data = json.load(data)
        print(json_data)
    return addLifecycle(json_data, app_short,life_cycle)

def addLifecycle(json_data, app_short,life_cycle):
    json_obj = json_data['lifecycle']
    life_cycle_dict = {'devl': 'development','acpt': 'acceptance','test': 'test','prod': 'production',
    'cont': 'contingency','clve' : 'clve','sdbx' : 'sandbox','acnt': 'acptcont'}
    actual_key = ""
    if life_cycle in life_cycle_dict.keys():
        actual_key = life_cycle_dict[life_cycle]
    print(actual_key)

    final_val = life_cycle + '-' + app_short.lower()
    app_short_upper = app_short.upper()
    if app_short not in json_obj[actual_key].keys():
        json_obj[actual_key].setdefault(app_short_upper,final_val)
        json_data['lifecycle'] = json_obj
        s3.Bucket(bucket).put_object(Key=key, Body=json.dumps(json_data))
        print ("App Short Name entry {} added in  {} " .format(app_short_upper,actual_key))
        return True
    else:
        print ("App Short Name entry {} is already in  {} " .format(app_short_upper,actual_key))
        return False

class StackAlreadyExistsException(Exception): "Stack Already Exists"
def invoke_deploy_v3_handler(event, context):
    log.debug("Processing event: %s", json.dumps(event, default=str))
    account = event["PipelineAccountId"]
    region = event.get("PipelineAccountRegion", "us-east-1")
    session = assume_session("InvokeDeployV3","asap-stack-sets",
            (account, "asap-cicd"), region_name=region)
    function_name = event["DeployV3FunctionName"]
    l = session.client("lambda")
    log.info('Invoking %s on %s/%s', function_name, account, region)
    response = l.invoke(
        FunctionName=function_name, Payload=json.dumps(event).encode(),
    )

    if "Payload" in response:
        response["Payload"] = response["Payload"].read().decode("utf-8")

    log.debug("Response from %s: %s", function_name,
            json.dumps(response, default=str))
    if "FunctionError" in response:
        error_detail = json.loads(response["Payload"])
        if error_detail["errorType"] == "AlreadyExistsException":
            raise StackAlreadyExistsException(response["Payload"])
        else:
            raise Exception(response["FunctionError"], error_detail)

    return json.loads(response["Payload"])

def send_myservices_email_handler(event, context):
    log.info('Receive the event - {}'.format(event,indent=2))
    if 'error_output' in event:
        return sendMailFailure(event)
    else:
        log.info('no errors All Went successfull ')
        if isROOTMFAEnabled(event, context):
            return sendMailSuccess(event)
        else:
            return sendMFAOnboardEmail(event)
 
def sendMailSuccess(event):
    sender_email = 'Cloud_CoE_-_FAST_-_Provisioning_Automation@fanniemae.com'
    reciever_email = ['shanmugam_x_karuppannan@fanniemae.com','sanjiv_karandikar@fanniemae.com','nitin_x_adsul@fanniemae.com']
    cc_email = ['angel_angalkurthy@fanniemae.com','shantanu_bagchi@fanniemae.com','vasudha_upadhyaya@fanniemae.com']
    SMTP_HOST = 'mailhub.fanniemae.com'
    account = event['Account']
    region = event['Region']
    print(account)
    print(region)
    session = assume_session("SendEmail","asap-stack-sets", (account, "asap-cicd"),region_name=region)
    ssm_client = session.client('ssm')
    account_name_ssm_param = '/global/AccountName'
    lifecycle_ssm_param = '/fnma/account/lifecycle'
    response = ssm_client.get_parameter(Name=lifecycle_ssm_param)
    app_params = response['Parameter']['Value'].split('-')
    # Create the message
    appshortname = app_params[1]
    lifecycle = app_params[0]
    response = ssm_client.get_parameter(Name=account_name_ssm_param)
    myservices =  response['Parameter']['Value']
    print(appshortname)
    print(lifecycle)
    print(myservices)
    msg = MIMEMultipart('alternative')
    html = """\
        <html>
        <head></head>
        <body>
            <p>We need a new Account Type called <b>""" + str(myservices) + """</b> <br><br>
    
            Account Type:  <b>""" + str(myservices) +"""</b><br><br>
            Account Type Code: <b>""" +str(appshortname.upper())+ """</b> (Note: when they select the account type above, this is the code we expect in the interface) Lifecycle Type: <b>""" +str(lifecycle.upper())+ """</b><br><br>
            Lifecycle: <b>""" +str(lifecycle)+ """-"""+str(appshortname)+ """</b> (NOTE: This is what we expect in the SG-AWS-<Lifecycle>-…. Group names) Account Number: <b>""" +str(account) +"""</b><br><br>
            </p>
        </body>
        </html>
    """
    msg.attach(MIMEText(html, 'html'))

    msg['To'] = ', '.join(reciever_email)
    msg['Cc'] = ', '.join(cc_email)
    msg['From'] = email.utils.formataddr(('Cloud Coe - FAST - Provisioning Automation', sender_email))
    msg['Subject'] = 'Please add the following account to the Test MyServices portal.'
    server = smtplib.SMTP(SMTP_HOST)
    server.set_debuglevel(True) # show communication with the server
    try:
        server.sendmail(sender_email, (reciever_email+ cc_email), msg.as_string())
    finally:
        server.quit()
   
def sendMailFailure(event):
    sender_email = 'Cloud_CoE_-_FAST_-_Provisioning_Automation@fanniemae.com'
    reciever_email = ['shantanu_bagchi@fanniemae.com','angel_angalkurthy@fanniemae.com']
    SMTP_HOST = 'mailhub.fanniemae.com'
    email_body = json.dumps(event,indent=4)
    msg = MIMEMultipart('alternative')
    text = """FAILED email on step function !\n """ +str(email_body)+ """ """
    html = """\
        <html>
        <head></head>
        <body>
            <p>FAILURE email<br>
    
            Account Type: """ +str(email_body)+ """<br>
        </body>
        </html>
    """
    msg.attach(MIMEText(text, 'plain'))
    msg.attach(MIMEText(html, 'html'))

    msg['To'] = ', '.join(reciever_email)
    msg['From'] = email.utils.formataddr(('Cloud Coe - FAST - Provisioning Automation', sender_email))
    msg['Subject'] = 'Failure to Onboard'
    
    server = smtplib.SMTP(SMTP_HOST)
    server.set_debuglevel(True) # show communication with the server
    try:
        server.sendmail(sender_email, (reciever_email) , msg.as_string())
    finally:
      server.quit()

def sendMFAOnboardEmail(event):
    sender_email = 'Cloud_CoE_-_FAST_-_Provisioning_Automation@fanniemae.com'
    reciever_email = ['shantanu_bagchi@fanniemae.com','angel_angalkurthy@fanniemae.com']
    SMTP_HOST = 'mailhub.fanniemae.com'
    email_body = json.dumps(event,indent=4)
    msg = MIMEMultipart('alternative')
    text = """FAILED email on step function !\n """ +str(email_body)+ """ """
    html = """\
        <html>
        <head></head>
        <body>
            <p>MFA is not enabled for the ROOT Account<br>   
            Account Type: """ +str(email_body)+ """ is not ready to be on-boarded to Myservices<br>
        </body>
        </html>
    """
    msg.attach(MIMEText(text, 'plain'))
    msg.attach(MIMEText(html, 'html'))

    msg['To'] = ', '.join(reciever_email)
    msg['From'] = email.utils.formataddr(('Cloud Coe - FAST - Provisioning Automation', sender_email))
    msg['Subject'] = 'MFA is not enabled for the ROOT Account'
    
    server = smtplib.SMTP(SMTP_HOST)
    server.set_debuglevel(True) # show communication with the server
    try:
        server.sendmail(sender_email, (reciever_email) , msg.as_string())
    finally:
      server.quit()

def createRITMForADGroup(event):
    api_proxy_url = {'https': 'http://zsproxy.fanniemae.com:9480',
        'http': 'http://zsproxy.fanniemae.com:9480'
    }
    # auth_header, host_name, url = get_secret()
    # url = url.replace('host_name', host_name)
    auth_header = 'YXdzbXlzcnZpbnQ6QHckTVMyMDE3'
    url = 'https://fanniemaedev.service-now.com/api/now/import/u_create_ritm_import'
    LOGGER.info('auth_header - %s', auth_header)
    LOGGER.info('host_name - %s', host_name)
    LOGGER.info('url - %s', url)
    payload = {
        "u_service_catalog_variables": "{'contact_name':'syuspc', 'requested_for':'syuspc', 'ad_group_action':'Add', 'ad_group_action_add_type':\"Active Directory Non-Standard Security Group\", 'ad_security_group_name_add':'SG-SC0012-28762', 'ad_group_owner':'syuspc', 'data_app_server':\"Provides isolation lab 2 security administrative access\", 'books_of_business':'No', 'asset_id':'AST000000000147104', 'risk_rating':'Medium', 'risk_rating_justification':'test', 'privilege_value':'Privilege', 'environment':'Test', 'financial_reporting':'Yes', 'hamp':'Yes', 'npi':'Yes', 'biz_just_choice':'test'}",
        "u_service_catalog_id": "3a9baf950fc6f1401c42e388b1050e48"
        }
    # payload = {
    #     'sysparm_exclude_reference_link': 'true',
    #     'sysparm_display_value': 'false',
    #     'sysparm_fields': 'u_sc_task,u_sc_req_item,u_parameters,u_app_name,u_app_name.u_asset_id,u_app_name.u_business_owner.email,u_app_name.u_technology_owner.email,u_app_name.supported_by.email,u_app_name.u_l2_support_group.email,u_requested_for.email',
    #     'sysparm_query': 'u_type=aws',
    #     'u_status': 'closed complete'
    # }
    requests_session = requests.session()
    requests_session.verify = False
    authorization = 'Basic ' + auth_header
    requests_session.headers.update(
        {
            'Authorization': authorization,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    )
    response = requests_session.get(url,proxies=api_proxy_url, params=payload,json=True, timeout=5)
    LOGGER.info('Status code - %s', response.status_code)
    LOGGER.info('Response - %s', response.json())
    return response.json()
if __name__ == '__main__':
    import unittest.mock as mock

    context = mock.MagicMock()

    event = {'Account': '285488728505', 'Region': 'us-east-1'}

    print(invoke_dns_test_lambda_handler(event, context))
