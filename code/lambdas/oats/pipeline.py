#!/usr/bin/env python3
"""
Lambda for the status of Pipelines in other accounts
"""

import os
import json
import logging
import datetime
import boto3

from iam import assume_session
import ssm

logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


class SourcePipelineStateNotFound(Exception):
    pass


class TargetPipelineStateNotFound(Exception):
    pass


class ForceSourceRelease(Exception):
    pass


class PipelineApprovalRequired(Exception):
    pass


class PipelineAutoApproved(Exception):
    pass


def validate_tokens_inprogress(stages):
    tokens = []
    for stage in stages:
        for state in stage['actionStates']:
            if 'token' not in state.get('latestExecution', {}):
                continue

            tokens.append(
                dict(
                    stageName=stage['stageName'],
                    actionName=state['actionName'],
                    token=state['latestExecution']['token'],
                )
            )

    return tokens


def pipeline_status(client, name, auto_approve=False):
    """Get Pipeline Status using client"""

    log.debug("Getting pipeline status of %s - autoapprove is %s",
              name, auto_approve)
    response = client.get_pipeline_state(name=name)
    log.debug("Response = %s", json.dumps(response, default=str))
    pipeline_updated = response.get("updated") or response["created"]
    log.debug("pipeline update = %s", pipeline_updated)

    stages = response["stageStates"]
    tokens = validate_tokens_inprogress(stages)

    if tokens:
        if auto_approve:
            for token in tokens:
                client.put_approval_result(
                    pipelineName=name,
                    result={'summary': 'Onboarding', 'status': 'Approved'},
                    **token,
                )
            raise PipelineAutoApproved(tokens)
        else:
            raise PipelineApprovalRequired(tokens)

    status = {
        "PipelineUpdated": pipeline_updated.timestamp(),
        "Status": 'Unknown',
        "StartTime": None,
        "StartTimestamp": None,
        "Time": None,
        "Timestamp": None,
    }

    start_execution = stages[0].get('latestExecution', {})
    end_execution = stages[-1].get('latestExecution')
    if end_execution:
        start_id = start_execution.get('pipelineExecutionId')
        end_id = end_execution.get('pipelineExecutionId')
        if start_id == end_id:
            status['Status'] = end_execution.get('status')

    start_updated = get_state_last_updated(stages[0].get("actionStates", []))
    if start_updated:
        status['StartTime'] = start_updated.isoformat()
        status['StartTimestamp'] = start_updated.timestamp()
    else:
        status['StartTimestamp'] = 0

    last_updated = get_state_last_updated(stages[-1].get("actionStates", []))
    if last_updated:
        status['EndTime'] = last_updated.isoformat()
        status['EndTimestamp'] = last_updated.timestamp()
    else:
        status['EndTimestamp'] = 0

    return status


def get_state_last_updated(states):
    log.debug("states = %s", json.dumps(states, default=str))

    last_updated = [
        s.get("latestExecution", {}).get("lastStatusChange") for s in states
    ]
    log.debug("last_updated = %s", last_updated)
    last_updated = list(filter(None, last_updated))
    if last_updated:
        last_updated = max(last_updated)
        last_updated = last_updated.replace(microsecond=0)
        last_updated = last_updated

    return last_updated


def status_handler(event, context):
    log.debug("pipeline status handler = %s", json.dumps(event))
    account = event["Account"]
    region = event["Region"]
    auto_approve = event.get('AutoApprove', False)
    pipeline = event["PipelineName"]

    session = assume_session(
        "Onboarding",
        "asap-stack-sets", (account, "asap-cicd"),
        region_name=region,
    )

    if '{lifecycle}' in pipeline:
        lifecycle = ssm.get_target_lifecycle(account, region)
        pipeline = pipeline.replace('{lifecycle}', lifecycle)

    try:
        count = int(event["PipelineState"]["Count"])
    except:
        count = 0

    client = session.client("codepipeline")
    status = pipeline_status(client, pipeline, auto_approve)
    status["Count"] = count + 1
    log.debug('Status to be returned: %s', json.dumps(status))

    if 'CommitStatus' in event:
        commit_timestamp = event['CommitStatus'].get('Timestamp', 0)

    return status


def pipeline_release(client, name):
    """Trigger Release on Pipeline"""
    log.debug("Trigger Pipeline %s", name)
    client.start_pipeline_execution(name=name)

    now = datetime.datetime.utcnow()
    return now.replace(microsecond=0)


def release_handler(event, context):
    log.debug("event = %s", json.dumps(event))

    admin_role = os.getenv("AdminRole", "asap-stack-sets")
    exec_role = event.get("AssumeRole", "asap-cicd")
    account = event.get("Account")
    region = event.get('Region', 'us-east-1')
    if not account:
        account = boto3.client('sts').get_caller_identity()['Account']

    session = assume_session('Onboaring', admin_role, (account, exec_role),
                             region_name=region)

    pipeline = event["PipelineName"]

    client = session.client("codepipeline")

    trigger_time = pipeline_release(client, pipeline)

    return True


def target_pipeline_status_handler(event, context):
    log.debug('target_pipeline_status_handler: %s', json.dumps(event))
    triggered = False
    source_state = event["SourcePipeline"].get("PipelineState")
    log.debug("source state = %s", json.dumps(source_state))
    if source_state is None:
        raise SourcePipelineStateNotFound

    source_ts = source_state.get("EndTimestamp")
    log.debug("source ts = %s", source_ts)
    if source_ts is None:
        raise SourcePipelineStateNotFound

    target_state = event.get("TargetPipeline", {}).get("PipelineState", {})
    target_count = target_state.get("Count", 0)
    target_state = status_handler(event["TargetPipeline"], context)
    if target_state is None:
        raise TargetPipelineStateNotFound
    target_state["Count"] = target_count + 1
    log.debug("target state = %s", target_state)

    target_ts = target_state.get("EndTimestamp")
    log.debug("target ts = %s", target_ts)
    if target_ts is None:
        raise TargetPipelineStateNotFound

    """
    target_pipeline_last_updated = target_state["PipelineUpdated"]
    if target_pipeline_last_updated > source_ts:
        # Target Pipeline Created After Last Source Trigger
        raise ForceSourceRelease

    """
    if target_state.get("Status") == "Unknown" and source_state.get("Count") == 1:
        raise ForceSourceRelease

    if (
        source_ts > target_ts
        and not event.get("SourcePipeline", {}).get('Triggered', False)
        and (target_state["Status"] is None or target_state["Status"] == "Failed")
    ):
        raise ForceSourceRelease

    log.debug("returning %s", target_state)
    return target_state


def source_pipeline_needs_triggered_handler(event, context):
    commit_timestamp = event.get('CommitResult', {}).get('Timestamp')
    pipeline_state = event.get('SourcePipeline', {}).get('PipelineState')
    log.debug('Compare %s and %s', commit_timestamp, pipeline_state)

    if commit_timestamp is None or pipeline_state is None:
        log.info('Commit or Pipeline States are empty')
        return

    if pipeline_state.get('Status') != 'Succeeded':
        log.info(
            'pipeline state %s is running. check later', pipeline_state.get('Status')
        )
        return

    pipeline_timestamp = pipeline_state.get('StartTimestamp')
    log.debug('%d < %d?', pipeline_timestamp, commit_timestamp)
    if pipeline_timestamp is None or pipeline_timestamp < commit_timestamp:
        raise ForceSourceRelease


if __name__ == "__main__":
    import unittest.mock as mock
    context = mock.MagicMock()

    event = {
        "Account": "172587047172",
        "Region": "us-east-1",
        "PipelineName": "eng-asap-account-management"
    }

    import pdb; pdb.set_trace()  # XXX BREAKPOINT
    status_handler(event, context)

    raise SystemExit
    event = {
        "SourceOnly": True,
        "SourcePipeline": {
            "PipelineName": "eng-asap-account-management",
            "PipelineState": {
                "PipelineUpdated": 1557857947.701,
                "Status": "Succeeded",
                "StartTime": "2020-03-11T15:51:27+00:00",
                "StartTimestamp": 1583941887,
                "Time": "2020-03-11T15:53:48+00:00",
                "Timestamp": 1583942028,
                "Count": 1,
            },
        },
        "CommitResult": {
            "Repo": "eng-asap-account-management",
            "CommitId": "ecc0b7e1427e4463440ce96ddea56e5c51e1d8e6",
            "Timestamp": 1584941841,
        },
    }

    # source_pipeline_needs_triggered_handler(event, context)

    event = {
        "PipelineName": "asap-yapr-infra",
        "Account": "262257643404",
        "Region": "us-east-1",
        "PipelineState": {
            "PipelineUpdated": 1584563836.484,
            "Status": "Failed",
            "StartTime": "2020-03-18T20:41:18+00:00",
            "StartTimestamp": 1584564078,
            "Time": "2020-03-18T20:43:29+00:00",
            "Timestamp": 1584564209,
            "Count": 27,
        },
    }
    # target_pipeline_status_handler(event, context)

    """
    event = {
        "Account": "189693026861",
        "PipelineName": "monitoring-deployment-devl-pipeline",
        "AutoApprove": True,
        "Region": "us-east-2"
    }
    """

