#!/usr/bin/env python3

import json
import logging

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger()
log.setLevel(logging.DEBUG)


def handler(event, context):
    log.debug(json.dumps(event, default=str))


if __name__ == '__main__':
    import unittest.mock as mock

    context = mock.MagicMock()
    handler({'Hello': 'World'}, context)
