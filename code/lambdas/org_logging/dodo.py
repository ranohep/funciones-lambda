import os
import sys
import glob
import re
import shutil
import subprocess
import datetime
import zipfile
from doit import create_after
from doit.tools import create_folder
from contextlib import contextmanager

sys.path.append(
    os.path.join(os.path.dirname(__file__), '..', '..', 'shared', 'asap')
)
import asap.doit

BASE_DIR = os.path.basename(os.path.dirname(__file__))
DIST_DIR = "dist"
REQ_FILE = f"{DIST_DIR}/requirements.txt"
ZIP_FILE = f"{DIST_DIR}/{BASE_DIR}.zip"

SOURCES = asap.doit.source_python_files()
zipper = asap.doit.ZipDependencies(epoch=True)


def task_zip():
    return {
        'task_dep': ['requirements'],
        'file_dep': SOURCES,
        'targets': [ZIP_FILE, 'samTemplate.yaml'],
        'actions': [zipper],
        'clean': True,
    }


def task_setup_pipenv():
    """
    Setup the pipenv venv with all of the development tools
    """
    return {
        "actions": ["pipenv install -d"],
        "file_dep": ["Pipfile", "Pipfile.lock"],
    }


def task_dist():
    return {
        "actions": ["mkdir -p %(targets)s"],
        "targets": ["dist"],
        "clean": True,
    }

def task_requirements():
    """Build requirements.txt file from pipenv's Pipfile"""

    def clean_dist(dir):
        if os.path.isdir(dir):
            shutil.rmtree(dir)

    @contextmanager
    def read_requirements():
        if os.name == "nt":
            f = open("requirements.txt", "rb")
            yield f
            f.close()
        else:
            proc = subprocess.Popen(["pipenv", "lock", "-r"],
                                    stdout=subprocess.PIPE)
            yield proc.stdout

    def install_requirements(req_file):
        with open(req_file, "wb") as req, read_requirements() as f:
            for line in f:
                if line.startswith(b"-e "):
                    req.write(line[3:])
                else:
                    req.write(line)
        subprocess.run(["pip", "install", "-r", req_file, "-t", DIST_DIR])

    return {
        "actions": [
            (install_requirements, [REQ_FILE]),
        ],
        "file_dep": ["Pipfile", "Pipfile.lock"],
        "task_dep": ["dist"],
        "targets": [REQ_FILE],
        "clean": True,
    }


