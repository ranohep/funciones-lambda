
from util import *
import time
import os

ops_bucket = 'fnma-provisioning-devl-ceng-us-east-1-172587047172-operations'

def invoke_edl_resource_policy_update_handler(event, context):
    global acc_list
    session = None
    region = 'us-east-1'
    evt = {                       
        "event_type": "dump_svcs"
    }
    lifecycle = event['lifecycle'].upper()
    datascience_acctid = ''
    try:
        datascience_acctid = event['ds_acnt_id']
    except:
        print('Lifecycle does not have data scienece account')
    # kms_key_list = event['kms_key_list']
    no_op = ''
    try:
        no_op = event['no_op']
    except:
        no_op = False
    
    print(no_op)
    action = ''
    try:
        action = event['Action'].upper()
    except:
        action = 'UPDATE'
    if lifecycle == 'CONT' or lifecycle == 'ACNT':
        region = 'us-east-2'
    account_list_lifecycle = acc_list.get(lifecycle)
    print(account_list_lifecycle)
    if account_list_lifecycle is not None:
        for acc_num in account_list_lifecycle:
            jit_acnt_list = account_list_lifecycle.copy()
            jit_acnt_list.remove(acc_num)
            if datascience_acctid != '':
                jit_acnt_list.append(datascience_acctid)
            principal_list = form_jit_principal_list(jit_acnt_list)
            
            session = assume_session("ResourcePolicyUpdate","asap-stack-sets", (acc_num, "asap-cicd"),region_name=region)
            lambda_client = session.client('lambda')
            invoke_response = lambda_client.invoke(FunctionName="asap-ais-graph-test-tool",
                                           InvocationType='Event',
                                           Payload=json.dumps(evt))
            time.sleep(5)
            app_short_list = get_list_of_appshortnames(session)
            s3_conditional_list = form_jit_conditonal_list(jit_acnt_list,'s3')
            edl_s3_policy = form_jit_str(principal_list,s3_conditional_list,'s3')
            bucket_edl_policies_dict = form_resource_policies_list(session,acc_num,edl_s3_policy,app_short_list,region,'S3',None,None)            
            update_bucket_policies(session,bucket_edl_policies_dict,no_op,action)
            # if kms_key_list:
            kms_conditional_list = form_jit_conditonal_list(jit_acnt_list,'kms')
            edl_kms_policy = form_jit_str(principal_list,kms_conditional_list,'kms')
            kms_edl_policies_dict = form_resource_policies_list(session,acc_num,edl_kms_policy,app_short_list,region,'KMS',None,None) 
            update_kms_policies(session,kms_edl_policies_dict,no_op,action)
            # edl_glue_policy = form_jit_str(principal_list,conditional_list,'glue')
            # update_glue_policies(session,edl_glue_policy,acc_num,region,no_op)
            principal_list = None
            jit_acnt_list = None
            edl_s3_policy = None
    else:
        print('NO ACCOUNTS AVALIABLE')
    return None

def invoke_alb_resource_policy_update_handler(event, context):    
    # session = None
    # account = event['Account']
    # print(account)
    # region = event['Region']
    # print(region)
    # action = ''
    # try:
    #     action = event['Action'].upper()
    # except:
    #     action = 'UPDATE'
    # accnum = ''
    # if (region == 'us-east-1'):
    #     accnum = '127311923021'
    # elif (region == 'us-east-2'):
    #     accnum = '033677994240'
    # else:
    #     raise 'Invalid Region In Input'
    # key_list = {}
    # key_list["<accnum>"] = accnum
    # no_op = ''
    # try:
    #     no_op = event['no_op']
    # except:
    #     no_op = False
    # session = assume_session("ALBResourcePolicyUpdate","asap-stack-sets", (account, "asap-cicd"),region_name=region)
    # # Create bucket
    # try:
    #     create_bucket(session,account,region)
    # except botocore.exceptions.ClientError as error:
    #     if error.response['Error']['Code'] == 'BucketAlreadyExists':
    #         print('Bucket Already Exists Continue with policy update')
    #     else:
    #         raise error
    # elb_s3_policy = get_resource_policy_template('s3')
    # bucket_elb_policies_dict = form_resource_policies_list(session,account,elb_s3_policy,None,region,'s3',None,key_list)    
    # update_bucket_policies(session,bucket_elb_policies_dict,no_op,action)
    print("The lambda function is deprecated.The bucket and respective policy would be deployed through cfn")
    return None

def invoke_s3_resource_policy_update_handler(event, context):    
    global ops_bucket
    session = None
    account = event['Account']
    print(account)
    region = event['Region']
    print(region)
    bucket_list = event['Buckets']
    print(bucket_list)
    action = event['Action'].upper()
    print(action)
    template_file_name = ''
    bucket_policies_dict = {}
    no_op = ''
    try:
        no_op = event['no_op']
    except:
        no_op = False    
    session = assume_session("S3ResourcePolicyUpdate","asap-stack-sets", (account, "asap-cicd"),region_name=region)
    if action == 'UPDATE':
        template_file_name = event['Template']
        print(template_file_name)
        key_list = []
        try:
            key_list = event['KEYS_TO_BE_REPLACED']
            print(key_list)
        except:
            print("Nothing to replace")
        s3_policy = get_s3_data(None,ops_bucket,template_file_name)
        bucket_policies_dict = form_resource_policies_list(session,account,s3_policy['Statement'],None,region,'s3',bucket_list,key_list)
    elif action == 'ROLLBACK':
        print('Rollback changes')
        bucket_policies_dict = form_rollback_resource_policies_list(session,account,bucket_list,'s3')
    else:
        raise "Invalid Action"
    update_bucket_policies(session,bucket_policies_dict,no_op,action)
    return None

def invoke_resource_policy_update_handler(event, context):    
    global ops_bucket
    session = None
    account = event['Account']
    print(account)
    region = event['Region']
    print(region)
    resources_list = event['Resources']
    print(resources_list)
    action = event['Action'].upper()
    print(action)
    resource_type = event['Type'].upper()
    print(resource_type)    
    template_file_name = ''
    resource_policies_dict = {}
    no_op = ''
    try:
        no_op = event['no_op']
    except:
        no_op = False    
    session = assume_session("ResourcePolicyUpdate","asap-stack-sets", (account, "asap-cicd"),region_name=region)
    if action == 'UPDATE':
        template_file_name = event['Template']
        print(template_file_name)
        key_list = []
        try:
            key_list = event['KEYS_TO_BE_REPLACED']
            print(key_list)
        except:
            print("Nothing to replace")
        policy = get_s3_data(None,ops_bucket,template_file_name)
        resource_policies_dict = form_resource_policies_list(session,account,policy['Statement'],None,region,resource_type,resources_list,key_list)
    elif action == 'ROLLBACK':
        print('Rollback changes')
        resource_policies_dict = form_rollback_resource_policies_list(session,account,resources_list,resource_type.lower())
    else:
        raise "Invalid Action"
    update_resource_policies(session,resource_type,resource_policies_dict,no_op,action)
    return None