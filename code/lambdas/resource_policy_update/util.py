import json
import botocore
import boto3
import os
import logging
import time


base_template = {"Version": "2012-10-17", "Id": "BasePolicy", "Statement": []}
exclude_list = ['caradmin','cloudadm','fkk','secmgr']
jit_principal = "arn:aws:iam::<accnum>:root"
condition_str = "arn:aws:iam::<accnum>:role/jit-*"
jit_condition_str = "arn:aws:sts::<accnum>:assumed-role/jit-*/*"
rollback_bucket = "fnma-asap-resource-policy-backup"

cond_arn_list = []
asap_tags = [
    {
        'Key': 'AppCode',
        'Value': 'FKK'
    },
    {
        'Key': 'ApplicationName',
        'Value': 'AWS'
    },                
    {
        'Key': 'ManagedBy',
        'Value': 'CCOE'
    },                    
    {
        'Key': 'CostCenter',
        'Value': '477'
    },
    {
        'Key': 'AssetID',
        'Value': 'MSR00036'
    },
    {
        'Key': 'ApplicationShortName',
        'Value': 'ASAP'
    }
]
def get_list_of_appshortnames(session):
    app_short_list = []
    ssm_client = session.client('ssm')
    bucket = ssm_client.get_parameter(Name='/asap/bucketname')['Parameter']['Value']
    print(bucket)
    key = 'graph_dump/app_services.json'
    app_services =  get_s3_data(session,bucket,key)
    app_short_list = []
    for x in app_services:
        if x not in exclude_list:
            app_short_list.append(x)
    print(app_short_list)
    return app_short_list

def form_jit_principal_list(jit_acnt_list):
    jit_principal_list = []
    for accnum in jit_acnt_list:
        arn_str = jit_principal
        arn_str = arn_str.replace('<accnum>',accnum)
        jit_principal_list.append(arn_str)
    return jit_principal_list
    
def form_jit_conditonal_list(jit_acnt_list,resource_type):
    cond_arn_list = []
    tmp_str = ''
    if resource_type == 'kms':
        tmp_str = condition_str
    else:
        tmp_str = jit_condition_str
    for accnum in jit_acnt_list:
        cond_arn_str = tmp_str
        cond_arn_str = cond_arn_str.replace('<accnum>',accnum)
        cond_arn_list.append(cond_arn_str)
    return cond_arn_list

def form_jit_str(jit_principal_list,cond_arn_list,resource_type):
    global edl_s3_policy
    global edl_kms_policy
    global edl_glue_policy
    global edl_s3_sid_list
    global edl_kms_sid_list
    global edl_glue_sid_list
    
    tmp_edl_policy = []
    actual_policy = []
    
    if resource_type == 's3':
        tmp_edl_policy = edl_s3_policy.copy()
        actual_policy = edl_s3_policy.copy()
    elif resource_type == 'kms':
        tmp_edl_policy = edl_kms_policy.copy()
        actual_policy =  edl_kms_policy.copy()
    elif resource_type == 'glue':
        tmp_edl_policy = edl_glue_policy.copy()
        actual_policy =  edl_glue_policy.copy()
        
    if jit_principal_list:
        for stmt in actual_policy:
            allow_principal_list = jit_principal_list.copy()
            deny_principal_list = jit_principal_list.copy()
            sid = stmt['Sid']
            if sid == 'AllowJIT':
                tmp_edl_policy.remove(stmt)
                stmt['Resource'] = principal_list
            elif sid == 'EDLDeny':
                tmp_edl_policy.remove(stmt)
                
                if resource_type == 'kms':
                    stmt['Condition']['StringNotLike']['aws:PrincipalArn'] = cond_arn_list
                else:
                    deny_principal_list.append("arn:aws:iam::<accountid>:root")
                print(f'deny_principal_list =    {deny_principal_list}')
                stmt['Principal']['AWS'] = deny_principal_list
            elif sid == 'EDLAllow':
                print(sid)
                print(f'allow_principal_list =    {allow_principal_list}')
                tmp_edl_policy.remove(stmt)
                stmt['Principal']['AWS'] = allow_principal_list
                if resource_type == 'kms':
                    stmt['Condition']['StringLike']['aws:PrincipalArn'] = cond_arn_list
            elif sid == 'DenyJIT':
                tmp_edl_policy.remove(stmt)
                stmt['Principal']['AWS'] = jit_principal_list
                stmt['Condition']['StringNotLike']['aws:arn'] = cond_arn_list
            if stmt not in tmp_edl_policy:
                tmp_edl_policy.append(stmt)
            allow_principal_list = None
            deny_principal_list = None
    else:
        print(actual_policy)
        for stmt in actual_policy:
            sid = stmt['Sid']
            print(sid)
            if sid in edl_s3_sid_list or sid in edl_kms_sid_list or sid in edl_glue_sid_list:
                print('remove empty statements for '+ sid)
                tmp_edl_policy.remove(stmt)

    return tmp_edl_policy

def form_resource_policies_list(session,accountid,policy,app_short_list,region,resource_type,resource_list,key_list):
    resource_type = resource_type.upper()
    policy_str = json.dumps(policy)
    print(f'Template Policy   {policy_str}')
    ssm_client = session.client('ssm')
    lifecycle = ssm_client.get_parameter(Name='/provisioning/default/common/Lifecycle')['Parameter']['Value']
    resource_policies_dict = {}
    bucket_name = None
    policy_str = policy_str.replace("<lifecycle>",lifecycle)
    policy_str = policy_str.replace("<lifecycleupper>",lifecycle.upper())
    policy_str = policy_str.replace("<region>",region)
    policy_str = policy_str.replace("<accountid>",accountid)
    if key_list:
        for key in key_list:
            key_val = key_list[key]
            policy_str = policy_str.replace(key,key_val)
            if (key == '<appshortname>'):
                policy_str = policy_str.replace("<appshortnameupper>",key_val.upper())
    if not resource_list:
        if resource_type == 'S3':
            s3_obj_name = os.environ['s3_resource_policy_object_name']
        elif resource_type == 'KMS':
            s3_obj_name = os.environ['kms_resource_policy_object_name']
        else:
            raise "Unspported Resource Type in input. Valid values are S3|KMS"
        if app_short_list and s3_obj_name.find("edl") != -1:
            key_aliases = get_kms_aliases(session)
            for x in app_short_list:
                if resource_type == 'S3':
                    bucket_name = x + '-' + lifecycle + '-' + region
                    if does_bucket_exist(session,bucket_name):
                        resource_policy = policy_str.replace("<appshortname>",x)
                        resource_policy = resource_policy.replace("<appshortnameupper>",x.upper())
                        kms_key_id = get_kms_key_id_for_appshortname(x,key_aliases)
                        resource_policy = resource_policy.replace("<kms_key_id>",kms_key_id)
                        resource_policy_str = json.loads(resource_policy)
                        print("here 1" + x)
                        print(resource_policy_str)
                        resource_policies_dict[bucket_name] = resource_policy_str
                        resource_policy = None
                if resource_type == 'KMS':
                    kms_key_id = get_kms_key_id_for_appshortname(x,key_aliases)
                    # resource_policy_str = json.loads(policy)
                    print("here 1" + x)
                    # print(resource_policy_str)
                    resource_policies_dict[kms_key_id] = json.loads(policy_str)
        elif not app_short_list and s3_obj_name.find("alb") != -1:
            bucket_name = 'fnma-'+lifecycle + '-elbaccesslog-'+ region + '-' + accountid
            s3_policy_str = json.loads(policy_str)
            resource_policies_dict[bucket_name] = s3_policy_str
            s3_policy_str = None
    else:
        for resource in resource_list:
            resource_policy_str = json.loads(policy_str)
            resource_policies_dict[resource] = resource_policy_str
    print(resource_policies_dict)
    return resource_policies_dict
    
def does_bucket_exist(session,bucket_name):
    s3 = session.resource('s3')
    try:
        s3.meta.client.head_bucket(Bucket=bucket_name)
        print("Bucket " + bucket_name + " Exists!")
        return True
    except botocore.exceptions.ClientError as e:
        # If a client error is thrown, then check that it was a 404 error.
        # If it was a 404 error, then the bucket does not exist.
        error_code = int(e.response['Error']['Code'])
        if error_code == 403:
            print("Private Bucket. Forbidden Access!")
            return True
        elif error_code == 404:
            print("Bucket " + bucket_name + " Does Not Exists!")
            return False

def create_bucket(session,account,region):
    s3_client = session.client('s3')
    ssm_client = session.client('ssm')
    lifecycle = ssm_client.get_parameter(Name='/provisioning/default/common/Lifecycle')['Parameter']['Value']
    bucket_name = 'fnma-'+lifecycle + '-elbaccesslog-'+ region + '-' + account
    print(bucket_name)
    # Create bucket
    try:
        if region != 'us-east-1':
            location = {'LocationConstraint': region}
            s3_client.create_bucket(Bucket=bucket_name,ACL='private',
                                    CreateBucketConfiguration=location)
        else:
            s3_client.create_bucket(Bucket=bucket_name,ACL='private')
    except botocore.exceptions.ClientError as error:
        if error.response['Error']['Code'] == 'BucketAlreadyExists':
            print('Bucket Already Exists Continue with policy update')
            return None
        else:
            raise error
    # Create the configuration
    put_bucket_encryption(s3_client,bucket_name)
    put_bucket_public_block(s3_client,bucket_name)
    put_bucket_ownership_controls(s3_client,bucket_name)
    put_bucket_life_cycle_rules(s3_client,bucket_name)
    put_bucket_tagging(session,bucket_name)

def put_bucket_tagging(session,bucket_name):
    s3 = session.resource('s3')
    bucket_tagging = s3.BucketTagging(bucket_name)
    response = bucket_tagging.put(
        Tagging={
            'TagSet': asap_tags
        }
    )

def put_bucket_encryption(s3_client,bucket_name):
    response = s3_client.put_bucket_encryption(
        Bucket=bucket_name,
        ServerSideEncryptionConfiguration={
            'Rules': [
                {
                    'ApplyServerSideEncryptionByDefault': {
                        'SSEAlgorithm': 'AES256'
                    }
                }
            ]
        }
    )
def put_bucket_ownership_controls(s3_client,bucket_name):
    response = s3_client.put_bucket_ownership_controls(
        Bucket=bucket_name,
        OwnershipControls={
            'Rules': [
                {
                    'ObjectOwnership': 'BucketOwnerPreferred'
                }
            ]
        }
    )    

def put_bucket_public_block(s3_client,bucket_name):
    response = s3_client.put_public_access_block(
        Bucket=bucket_name,
        PublicAccessBlockConfiguration={
            'BlockPublicAcls': True,
            'IgnorePublicAcls': True,
            'BlockPublicPolicy': True,
            'RestrictPublicBuckets': True
        }
    )
    
def put_bucket_life_cycle_rules(s3_client,bucket_name):
    response = s3_client.put_bucket_lifecycle_configuration(
        Bucket=bucket_name,
        LifecycleConfiguration={
            'Rules': [
                {
                    'Expiration': {
                        'Days': 7
                    },
                    'ID': 'retention7days',
                    'Status': 'Enabled',
                    'Prefix': '',
                    'NoncurrentVersionExpiration': {
                        'NoncurrentDays': 7
                    }
                }
            ]
        }
    )
    
def get_s3_data(session,bucket,key):
    if session != None:
        s3 = session.resource('s3')
    else:
        s3 = boto3.resource('s3')
    obj = s3.Object(bucket,key)
    data = obj.get()['Body']
    try:
        json_data = json.load(data)
        return json_data
    except:
        raise "Malformed JSON Template"
        
def put_s3_data(session,bucket,key,json_data):
    if session != None:
        s3 = session.resource('s3')
    else:
        s3 = boto3.resource('s3')
    print(json_data)
    print(bucket)
    s3.Bucket(bucket).put_object(Key=key, Body=json.dumps(json_data))
    print('Updated Obj')
    
def get_resource_data(object_name):
    try:
        bucket = os.environ['bucket_name']
        key = os.environ[object_name]
    except:
        return None
    return get_s3_data(None,bucket,key)
    
def get_acct_white_list():
    return get_resource_data('acnt_whitelist_object_name')

def get_resource_policy_template(resource_type):
    if resource_type == 's3':
        json_data = get_resource_data('s3_resource_policy_object_name')
    elif resource_type == 'kms':
        json_data = get_resource_data('kms_resource_policy_object_name')
    elif resource_type == 'glue':
        json_data = get_resource_data('glue_resource_policy_object_name')
    if json_data:
        return json_data['Statement']
    else:
        return None

def assume_session(session_name, *account_role_tuples, **kwargs):
    sts = boto3.client('sts')
    cred_params = {}
    for account_role in account_role_tuples:
        if isinstance(account_role, str) or len(account_role) == 1:
            account = sts.get_caller_identity()['Account']
            role = account_role
        else:
            account, role = account_role

        cred_params = dict(
            RoleArn=f'arn:aws:iam::{account}:role/{role}', RoleSessionName=session_name
        )

        response = sts.assume_role(**cred_params)
        cred = response["Credentials"]
        cred_params = dict(
            aws_access_key_id=cred["AccessKeyId"],
            aws_secret_access_key=cred["SecretAccessKey"],
            aws_session_token=cred["SessionToken"],
        )
        sts = boto3.client('sts', **cred_params)

    session = boto3.session.Session(**kwargs, **cred_params)
    return session

def get_each_bucket_policy(s3_client,b_name):
    try:
        return json.loads(s3_client.get_bucket_policy(Bucket=b_name)['Policy'])
    except botocore.exceptions.ClientError as error:
        if error.response['Error']['Code'] == 'NoSuchBucketPolicy':
            return base_template
        else:
            raise error

def does_kms_key_exist(session,key_id):
    kms = session.client('kms')
    try:
        kms.describe_key(KeyId=key_id)
        print("Key " + key_id + " Exists!")
        return True
    except botocore.exceptions.ClientError as error:
        if error.response['Error']['Code'] == 'NotFoundException':
            print("Key " + key_id + " Does Not Exist!")
            return False
        else:
            raise error

def get_each_kms_policy(kms_client,key_id):
    try:
        return json.loads(kms_client.get_key_policy(KeyId=key_id,PolicyName='default')['Policy'])
    except botocore.exceptions.ClientError as error:
        if error.response['Error']['Code'] == 'NotFoundException':
            return base_template
        else:
            raise error

def create_sid_list(policy):
    print(policy)
    sid_list = []
    for x in policy:
        print ("here  two :  ",x)
        sid = x["Sid"]
        print(sid)
        sid_list.append(sid)
    return sid_list
    
def updated_resource_policy_stmt(statement,sid_list,policy):
    new_statement = []
    for stmt in statement:
        try:
            sid = stmt['Sid']
            if sid not in sid_list:
                new_statement.append(stmt)
        except:
            break
    new_statement = new_statement + policy
    return new_statement

def update_bucket_policies(session,bucket_policies_dict,no_op,action):
    s3_client = session.client('s3')
    sts_client = session.client('sts')
    account = sts_client.get_caller_identity()['Account']
    global edl_s3_sid_list
    for bucket_name in bucket_policies_dict:
        print(f'  {bucket_name}')
        policy = {}
        if action == 'UPDATE' or action == 'OVERWRITE':
            try:
                print("Taking Backup for ", action)
                policy = get_each_bucket_policy(s3_client,bucket_name)
                key = 's3/'+account+'/'+bucket_name+'.json'
                put_s3_data(None,rollback_bucket,key,policy)
                
            except botocore.exceptions.ClientError as error:
                print(error)
                continue
            bucket_policy = bucket_policies_dict[bucket_name]
            try:
                if not edl_s3_sid_list:
                    edl_s3_sid_list = create_sid_list(bucket_policy)
            except NameError:
                edl_s3_sid_list = create_sid_list(bucket_policy)
            statement = policy['Statement']
            print(f'Existing Policy   {policy}')
            if action == 'OVERWRITE':
                # print(action)
                # print("In overwrite to delete")
                # response = s3_client.delete_bucket_policy(
                #     Bucket=bucket_name,
                #     ExpectedBucketOwner=account
                # )
                # print(response)
                # time.sleep(3)
                statement = []
            policy['Statement'] = updated_resource_policy_stmt(statement,edl_s3_sid_list,bucket_policies_dict[bucket_name])
            print(f'Updated Policy {policy}')
        else:
            print("In Rollback")
            policy = bucket_policies_dict[bucket_name]
        policy_str = json.dumps(policy)
        policy_str = policy_str.replace("\'","\"")
        print(policy_str)
        # Call put policy
        # if not no_op:
        #     try:
        #         # response = s3_client.put_bucket_policy(
        #         #     Bucket=bucket_name,
        #         #     Policy=policy_str
        #         # )
        #         # print(response)
        #     except botocore.exceptions.ClientError as error:
        #         print(f'Unable to update policy for {bucket_name} {error}')
        #         continue            
    
def get_list_of_kms_keys_from_appshortname(app_short_list,key_aliases):
    kms_key_id_list = []
    for alias in key_aliases:
        alias_str = alias['AliasName']
        for appshortname in app_short_list:
            app_short_alias = 'alias/fnma/app/'+appshortname
            if app_short_alias == alias_str:
                kms_key_id_list.append(alias['TargetKeyId'])
                print(f' The Key Alias is  {alias}')
                break
            else:
                continue
    return kms_key_id_list

def get_kms_key_id_for_appshortname(appshortname,key_aliases):
    app_short_alias = 'alias/fnma/app/'+appshortname
    for alias in key_aliases:
        alias_str = alias['AliasName']
        # print(" looking for: ", app_short_alias)
        # print(" Found: ", alias_str)
        if app_short_alias == alias_str:
            kms_key_id = alias['TargetKeyId']
            print(f' The Key Alias is  {alias} and the id ia {kms_key_id}')
            return kms_key_id
        else:
            continue
    
    
def get_kms_aliases(session):
    kms_client = session.client('kms')
    response = kms_client.list_aliases()
    key_aliases = response['Aliases']
    return key_aliases


def update_kms_policies(session,kms_policies_dict,no_op,action):
    kms_client = session.client('kms')
    sts_client = session.client('sts')
    account = sts_client.get_caller_identity()['Account']    
    # if not key_id_list:
    #     key_aliases = get_kms_aliases(session)
    #     key_id_list = get_list_of_kms_keys_from_appshortname(app_short_list,key_aliases)
    for key_id in kms_policies_dict:
        if not does_kms_key_exist(session,key_id):
            continue
        kms_policy = kms_policies_dict[key_id]
        # kms_policy = kms_policy.replace("\"","\'")
        if action == 'UPDATE' or action == 'OVERWRITE':
            try:
                if not edl_kms_sid_list:
                    edl_kms_sid_list = create_sid_list(kms_policy)
            except NameError:
                edl_kms_sid_list = create_sid_list(kms_policy)        
            print(f'  {key_id}')
            policy = get_each_kms_policy(kms_client,key_id)
            key = 'kms/'+account+'/'+key_id+'.json'
            print('here 2' + key)
            put_s3_data(None,rollback_bucket,key,policy)
            statement = policy['Statement']
            print(f'Existing Policy   {policy}')
            if action == 'OVERWRITE':
                # print(action)
                # print("In overwrite to delete")
                # response = s3_client.delete_bucket_policy(
                #     Bucket=bucket_name,
                #     ExpectedBucketOwner=account
                # )
                # print(response)
                # time.sleep(3)
                statement = []        
            policy['Statement'] = updated_resource_policy_stmt(statement,edl_kms_sid_list,kms_policy)
            print(f'Updated Policy {policy}')
            
            if not no_op:
                try:
                    print("HERE To Update: ",key_id)
                    # response = kms_client.put_key_policy(
                    #     KeyId=key_id,
                    #     PolicyName='default',
                    #     Policy=json.dumps(policy),
                    # )
                except:
                    print('Unable to update policy '+ key_id)
                    continue

def update_resource_policies(session,resource_type,resource_policies_dict,no_op,action):
    resource_type = resource_type.lower()
    client = session.client(resource_type)
    sts_client = session.client('sts')
    account = sts_client.get_caller_identity()['Account']
    sid_list = []
    for key_id in resource_policies_dict:
        print(f'  {key_id}')
        policy = {}
        if action == 'UPDATE':
            try:
                print('here 1')
                if resource_type == 's3':
                    policy = get_each_bucket_policy(client,key_id)
                elif resource_type == 'kms':
                    policy = get_each_kms_policy(client,key_id)
                key = resource_type+'/'+account+'/'+key_id+'.json'
                print('here 2' + key)
                put_s3_data(None,rollback_bucket,key,policy)
                
            except botocore.exceptions.ClientError as error:
                print(error)
                continue
            resource_policy = resource_policies_dict[key_id]
            print(policy)
            statement = policy['Statement']
            sid_list = create_sid_list(resource_policy)
            print(f'Existing Policy   {policy}')
            policy['Statement'] = updated_resource_policy_stmt(statement,sid_list,resource_policies_dict[key_id])
            print(f'Updated Policy {policy}')
        else:
            print("In Rollback")
            policy = resource_policies_dict[key_id]
        policy_str = json.dumps(policy)
        policy_str = policy_str.replace("\'","\"")
        print(policy_str)
        # Call put policy
        if not no_op:
            try:
                if resource_type == 's3':
                    response = client.put_bucket_policy(
                        Bucket=key_id,
                        Policy=policy_str
                    )
                    print(response)
                elif resource_type == 'kms':
                    response = client.put_key_policy(
                        KeyId=key_id,
                        PolicyName='default',
                        Policy=policy_str,
                    )
                    print(response)
            except botocore.exceptions.ClientError as error:
                print(f'Unable to update policy for {key_id} {error}')
                continue            





def update_glue_policies(session,edl_glue_policy,accountid,region,no_op):
    glue_client = session.client('glue')
    edl_glue_policy_str = json.dumps(edl_glue_policy)
    edl_glue_policy_str = edl_glue_policy_str.replace('<accountid>',accountid)
    edl_glue_policy_str = edl_glue_policy_str.replace('<region>',region)
    base_template_json = base_template.copy()
    base_template_json['Statement'] = json.loads(edl_glue_policy_str)
    edl_glue_policy_str = json.dumps(base_template_json)
    print("GLUE POLICY = " + edl_glue_policy_str)
    if not no_op:
        response = glue_client.put_resource_policy(
            PolicyInJson = edl_glue_policy_str,
            PolicyExistsCondition = 'NONE',
            EnableHybrid = 'FALSE'
        )    

def get_glue_policies(session):
    glue_client = session.client('glue')
    response = glue_client.get_resource_policies()
    print(response)

def form_rollback_resource_policies_list(session,account,resource_list,resource_type):
    resource_policies_dict = {}
    for resource in resource_list:
        template_file_name = resource_type+'/'+account+'/'+resource+'.json'
        print(template_file_name)
        policy = get_s3_data(None,rollback_bucket,template_file_name)
        resource_policies_dict[resource] = policy
    return resource_policies_dict

        
edl_s3_policy = get_resource_policy_template('s3')
edl_kms_policy = get_resource_policy_template('kms')
edl_glue_policy = get_resource_policy_template('glue')
if edl_s3_policy:
    edl_s3_sid_list = create_sid_list(edl_s3_policy)
if edl_kms_policy:
    edl_kms_sid_list = create_sid_list(edl_kms_policy)
if edl_glue_policy:
    edl_glue_sid_list = create_sid_list(edl_glue_policy)
acc_list = get_acct_white_list()