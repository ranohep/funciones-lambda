#!/usr/bin/env python3

import logging
import json

logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.DEBUG)


def clean_account(account):
    account = account.replace('-', '')
    account = account.strip()

    return account


def default_region(region, context):
    if not region:
        log.debug('Determing region from context: %s',
                  context.invoked_function_arn)
        region = context.invoked_function_arn.split(':')[3]

    return region


def handler(event, context):
    log.debug('handler called with: %s', json.dumps(event, default=str))

    event['Account'] = clean_account(event['Account'])
    event['Region'] = default_region(event.get('Region'), context)

    return event


if __name__ == '__main__':
    import unittest.mock as mock
    context = mock.MagicMock()
    context.invoked_function_arn = \
        'arn:aws:lambda:us-east-1:999999999999:function:fname'

    event = {
        "Account": "  123-456-7890    ",
    }

    print(handler(event, context))
